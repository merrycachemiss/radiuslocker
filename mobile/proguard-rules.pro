# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#firebase addons
# Add this global rule
-keepattributes Signature,SourceFile,LineNumberTable
-renamesourcefileattribute SourceFile
# This rule will properly ProGuard all the model classes in
# the package com.yourcompany.models. Modify to fit the structure
# of your app.
#-keepclassmembers class com.merrycachemiss.rl.aboutAppActivity {
#  *;
#}
#-keep class com.android.billingclient.api.*
#-keep class com.android.vending.billing.**

##crashlytics stuff
#-keepattributes *Annotation*
#-keepattributes SourceFile,LineNumberTable
#-keep public class * extends java.lang.Exception
##end crashlytics