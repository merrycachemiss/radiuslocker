/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import static android.Manifest.permission.BLUETOOTH_CONNECT;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.merrycachemiss.rl.adapters.btSafeDeviceLVAdapter;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.bioPinAuthentication;
import com.merrycachemiss.rl.util.permissionHelper;

import java.util.ArrayList;
import java.util.Set;

//import io.fabric.sdk.android.Fabric;


import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_HAS_BEEN_REFRESHED_BEFORE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_REFRESHER_QUEUED;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SerialProjection.SERIAL_PROJECTION;

import static com.merrycachemiss.rl.util.permissionHelper.BT_CONNECT_REQ_CODE;

public final class btSafeListPickerActivity extends AppCompatActivity{
    //BLUETOOTH LISTVIEW RELATED VARS
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothDevice> safeBTPairedStoredSerialsArrayList;
    private ListView safeBTDeviceListview;
    private TextView noSafeBTDevicesFoundText;
    public static boolean[] safeBTDeviceListCheckedState;
    public static boolean[] safeConnectedBTDeviceListCheckedState;

    private rldbInsertQueryDelete iqd;
    private static String TAG;
    private permissionHelper perm;
    private bioPinAuthentication auth;
    private boolean btIsRefreshing = false;
    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_bt_picker);
        Toolbar myToolbar = findViewById(R.id.safeBTToolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        TAG = "btSafeListPickerActivit";

        iqd = new rldbInsertQueryDelete(this);
        perm = new permissionHelper(this);

        safeBTDeviceListview = findViewById(R.id.safeBTDeviceListview);
        noSafeBTDevicesFoundText = findViewById(R.id.noSafeBTDevicesFoundText);

        // Register for broadcasts on BluetoothAdapter or Devicestate change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(btOnOffReceiver, filter);

        IntentFilter filter3 = new IntentFilter("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ContextCompat.registerReceiver(this, btConnDisconnRec, filter3, ContextCompat.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(btConnDisconnRec, filter3);
        }
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        safeBTDeviceListview.setOnItemClickListener((parent, view, position, id) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S, iqd, BT_REFRESHER_QUEUED, 1)) {
                    listInteraction(position, view);
                } else {
                    perm.toastMissingPermission(this.getApplicationContext().getResources().getString(R.string.missing_critical_perms));
                    String[] perms = new String[1];
                    perms[0] = BLUETOOTH_CONNECT;
                    perm.promptForPermission(this, perms, BT_CONNECT_REQ_CODE);
                }
            } else {
                listInteraction(position, view);
            }
        });
        auth = new bioPinAuthentication(this, TAG);
    }

    private void listInteraction(int position, View view){
        //get actual bt device info from second array (the indexes match) - just shows in toast for now
        //later you'll want to store the info in sql lite db or something when they check the item
        BluetoothDevice device = safeBTPairedStoredSerialsArrayList.get(position);
        final String deviceAddress = device.getAddress();
        @SuppressLint("MissingPermission") final String deviceName = device.getName();
        //toggle checkbox indirectly for row chosen
        CheckBox safeBTDeviceNameCheckBox = view.findViewById(R.id.safeBTDeviceNameCheckBox);
        safeBTDeviceNameCheckBox.toggle();
        //store checkbox states for scrolling
        if (safeBTDeviceNameCheckBox.isChecked()) {
            safeBTDeviceListCheckedState[position] = true;

            Runnable insertThread = () -> {
                iqd.monitoredSafeBTInsert(rldbContract.SafeBTQuery.CONTENT_URI, deviceAddress, deviceName);
                Log.d(TAG, "inserted " + deviceAddress + " " + deviceName);
            };
            new Thread(insertThread).start();

        } else{
            safeBTDeviceListCheckedState[position] = false;

            Runnable deleteThread = () -> {
                iqd.monitoredSafeBTDelete(rldbContract.SafeBTQuery.CONTENT_URI, deviceAddress);
                Log.d(TAG, "deleted " + deviceAddress + " " + deviceName);
            };
            new Thread(deleteThread).start();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //actionbar's "back" should not kill activities.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        auth.adjustTimeoutForPresence(true);
    }
    protected void onResume() {
        super.onResume();
        auth.adjustTimeoutForPresence(false);
        safeBTDeviceListview.setVisibility(View.GONE);
        noSafeBTDevicesFoundText.setVisibility(View.VISIBLE);

        try {
            if (bluetoothAdapter.isEnabled()) {
                generateSafeBtDeviceList();
            }
        } catch (Exception e) {
            
            Log.e(TAG, "Couldn't generate safe BT device list", e);
            Toast.makeText(btSafeListPickerActivity.this,
                    "Can't get BT device list.",
                    Toast.LENGTH_SHORT).show();
        }

    }


    @SuppressLint("MissingPermission")
    private void generateSafeBtDeviceList() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S, iqd, BT_REFRESHER_QUEUED, 1)) {
                generateList();
            } else {
                safeBTDeviceListview.setVisibility(View.GONE);
                noSafeBTDevicesFoundText.setVisibility(View.VISIBLE);
                String[] perms = new String[1];
                perms[0] = BLUETOOTH_CONNECT;
                perm.promptForPermission(this, perms, BT_CONNECT_REQ_CODE);
            }
        } else {
            generateList();
        }
    }

    @SuppressLint("MissingPermission")
    private void generateList(){
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (!pairedDevices.isEmpty()) {
            //FIRST ARRAY FOR DISPLAYING THE HUMAN-READABLE BT DEVICE INFO IN LISTVIEW
            ArrayList<String> safeBTPairedStoredDeviceArrayList = new ArrayList<>();
            //SECOND ARRAY FOR DEALING WITH THE BACKEND BT INFO (INVISIBLE TO USER)
            safeBTPairedStoredSerialsArrayList = new ArrayList<>();


            int pos = 0;
            //will need to figure out the size of the checked items list, and the size of the paired devices list
            //create/store checkmark states based on DB result. listview adapter sets checks.
            safeBTDeviceListCheckedState = new boolean[pairedDevices.size()];
            safeConnectedBTDeviceListCheckedState = new boolean[pairedDevices.size()];

            for (BluetoothDevice device : pairedDevices) {
                //need to first convert to hashmap way, then sorting, then a second sweep to define static boolean checkbox tracker's array size
                //otherwise the size will mismatch, since it will not be adding in some of these if they are in the other MONITORED table
                final String[] selectionArgs = {device.getAddress()};
                boolean isChosenMonitoredBTDevice = iqd.genericSingleItemExistsQuery(rldbContract.MonitoredBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);
                if (!isChosenMonitoredBTDevice) {//if already chosen as a monitored BT device, then don't display as a safe BT choice
                    //the device name gets passed to listview adapter:
                    safeBTPairedStoredDeviceArrayList.add(device.getName());// + " - " + device.getAddress());
                    //store device serials only, for storing to DB
                    safeBTPairedStoredSerialsArrayList.add(device);


                    //creates/stores checkmark states based on DB result. listview adapter sets checks.
                    boolean isChosenSafeBTDevice = iqd.genericSingleItemExistsQuery(rldbContract.SafeBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);
                    safeBTDeviceListCheckedState[pos] = isChosenSafeBTDevice;

                    //as above, but used to determine whether or not should be highlighted green if connected
                    final String[] connectedSelectionArgs = {device.getAddress(), "1"};
                    boolean isConnectedBTDevice = iqd.isConnectedBTDeviceQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);
                    safeConnectedBTDeviceListCheckedState[pos] = isConnectedBTDevice;
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "BT device connected state, according to db " + device.getName() + " " + device.getAddress() + " status: " + isConnectedBTDevice);
                    }
                    //boolean isConnectedBTDevice = iqd.genericSingleItemExistsQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);
                    pos++;
                }

            }


            if (!safeBTPairedStoredSerialsArrayList.isEmpty()) {
                //do a check here to see if the hashmap is empty. if not empty, then do the below.
                noSafeBTDevicesFoundText.setVisibility(View.GONE);
                safeBTDeviceListview.setVisibility(View.VISIBLE);
                ListAdapter safeBTDeviceAdapter = new btSafeDeviceLVAdapter(this, safeBTPairedStoredDeviceArrayList);
                safeBTDeviceListview.setAdapter(safeBTDeviceAdapter);
            } else {
                safeBTDeviceListview.setVisibility(View.GONE);
                noSafeBTDevicesFoundText.setVisibility(View.VISIBLE);
            }
        } else {
            safeBTDeviceListview.setVisibility(View.GONE);
            noSafeBTDevicesFoundText.setVisibility(View.VISIBLE);
            iqd.setSettingsTempLongVal(BT_HAS_BEEN_REFRESHED_BEFORE, 1);
        }
        safeBTDeviceListview.setEnabled(true);

    }

    private final BroadcastReceiver btOnOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // BT radio state detection
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {

                if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    Log.d(TAG,"btOnOffReceiver: BT turned off");
                    safeBTDeviceListview.setVisibility(View.GONE);
                    noSafeBTDevicesFoundText.setVisibility(View.VISIBLE);
                    //since BT has effectively refreshed, remove the refresh button
                    if (!btIsRefreshing) {
                        iqd.setSettingsTempLongVal(BT_HAS_BEEN_REFRESHED_BEFORE, 1);
                    }
                    btIsRefreshing = false;
                }
                else if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                    Log.d(TAG,"btOnOffReceiver: BT turned on");
                    
                    try {
                        generateSafeBtDeviceList();
                    } catch (Exception e) {

                        

                        Toast.makeText(btSafeListPickerActivity.this,
                                "Can't get BT device list.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };

    private final BroadcastReceiver btConnDisconnRec = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            boolean btEnabled = false;
            if (bluetoothAdapter != null){
                btEnabled = bluetoothAdapter.isEnabled();
            }
            if (btEnabled) {
                Log.d(TAG, "btConnDisconnRec: BT device connected/disconnected");
                
                safeBTDeviceListview.setEnabled(false);
                try {
                    generateSafeBtDeviceList();
                } catch (Exception e) {
                    safeBTDeviceListview.setEnabled(true);
                    
                    Toast.makeText(btSafeListPickerActivity.this,
                            "Can't get BT device list.",
                            Toast.LENGTH_SHORT).show();
                }
            }


        }
    };
    @Override
    protected void onDestroy() {
        // Unregister broadcast listeners

        try {
            unregisterReceiver(btOnOffReceiver);
        }catch (Exception e){
            Log.d(TAG,"Couldn't unregister btOnOffReceiver for unknown reasons");
        }

        try {
            unregisterReceiver(btConnDisconnRec);
        }catch (Exception e){
            Log.d(TAG,"Couldn't unregister btConnDisconnRec for unknown reasons");
        }

        super.onDestroy();
    }
}
