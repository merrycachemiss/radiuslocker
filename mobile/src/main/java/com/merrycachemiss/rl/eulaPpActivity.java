/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import android.content.Intent;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.dataStorage.shPrefsGetterSetter;
import com.merrycachemiss.rl.services.mainSystemEventReceiverService;
import com.merrycachemiss.rl.util.bioPinAuthentication;


import static com.merrycachemiss.rl.MainActivity.acceptedEula;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REAUTH_TRIGGER_TIME;
import static com.merrycachemiss.rl.util.shortcutAdder.getShortcutsAddNewOnes;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_NEW_EULA;
import static com.merrycachemiss.rl.util.NotificationHelper.clearNotification;

public final class eulaPpActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "eulaPpActivity";
    private shPrefsGetterSetter sPrefs;
    private bioPinAuthentication auth;
    private boolean wasFromFirstInstall = false;
    private boolean justAcceptedNewEULA = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eula_pp);
        final TextView declineTextLink = findViewById(R.id.declineTextLink);
        final TextView acceptTextLink = findViewById(R.id.acceptTextLink);
        final TextView ppFullText = findViewById(R.id.ppFullText);
        final TextView licenseFullTextA = findViewById(R.id.licenseFullTextA);
        final TextView licenseFullTextB = findViewById(R.id.licenseFullTextB);
        final TextView EULAPpContactInfo = findViewById(R.id.EULAPpContactInfo);
        sPrefs = new shPrefsGetterSetter(getApplicationContext());
        Toolbar myToolbar = findViewById(R.id.eulaToolbar);
        setSupportActionBar(myToolbar);

        if (sPrefs.getSharedPrefsInt("eula_accepted") == 1){//only show back button if user prev accepted eula
            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                ab.setDisplayHomeAsUpEnabled(true);
            }
            acceptedEula = 1;
            //disable accept/decline buttons if already accepted/declined
            declineTextLink.setEnabled(false);
            acceptTextLink.setEnabled(false);
            declineTextLink.setVisibility(View.GONE);
            acceptTextLink.setVisibility(View.GONE);
            auth = new bioPinAuthentication(this, TAG);

        } else{
            acceptedEula = 0;
            declineTextLink.setOnClickListener(this);
            acceptTextLink.setOnClickListener(this);
        }
        if (android.os.Build.VERSION.SDK_INT >= 26) {//can't remember why sdk26
            //Linkify.addLinks(licenseFullText, Linkify.WEB_URLS);
            Linkify.addLinks(ppFullText, Linkify.WEB_URLS);
        }
        Linkify.addLinks(EULAPpContactInfo, Linkify.EMAIL_ADDRESSES);
        licenseFullTextA.setText(Html.fromHtml(getString(R.string.eula_full_text_a)));
        licenseFullTextB.setText(Html.fromHtml(getString(R.string.eula_full_text_b)));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.declineTextLink) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user declined EULA + PP");
            }
            acceptedEula = 0;
            finishAffinity();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
        else if (v.getId() == R.id.acceptTextLink) {
            sPrefs.setSharedPrefsInt("eula_accepted", 1);
            acceptedEula = 1;

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user accepted EULA + PP, going back to previous activity. From old eula: " + (sPrefs.getSharedPrefsInt("not_new_install") == 2) );
            }

            wasFromFirstInstall = sPrefs.getSharedPrefsInt("not_new_install") != 2;

            if (sPrefs.getSharedPrefsInt("not_new_install") == 2) {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user accepted new eula, so clear the sticky notification about it");
                }
                clearNotification(this, TAG, NOTIF_ID_NEW_EULA);

                //ensure user is asked for PIN upon new EULA acceptance.
                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(this);
                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
                justAcceptedNewEULA = true;

            }

            //add dynamic homescreen shortcuts, if user above api 25
            if (Build.VERSION.SDK_INT >= 25) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "try to add dynamic shortcuts");
                }
                try {
                    getShortcutsAddNewOnes(this, TAG);
                } catch (Exception e) {

                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to add dynamic shortcuts", e);
                    }
                }
            }

            finish();
        }

    }

    private void startBtMonService (){
        Intent btMonStartServiceIntent = new Intent(eulaPpActivity.this, mainSystemEventReceiverService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(btMonStartServiceIntent);
        } else {
            startService(btMonStartServiceIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //actionbar's "back" should not kill activities.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (acceptedEula == 1) {//only perform ops if accepted eula
            if (auth == null){
                auth = new bioPinAuthentication(this, TAG);
            }
            auth.adjustTimeoutForPresence(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "activity onPause, starting btmon service if eula accepted for the first time.");
        }
        if (acceptedEula == 1) {//only perform ops if accepted eula
            if (auth == null){
                auth = new bioPinAuthentication(this, TAG);
            }

            if (!justAcceptedNewEULA) {//ensures that it doesn't bump the timeout when they accept the new EULA upon app update. otherwise it lets them in without auth.
                auth.adjustTimeoutForPresence(true);
            }
            if (wasFromFirstInstall) {//only start the service if it was from very first install, otherwise it will keep restarting when they leave this activity.
                startBtMonService();
            }
            justAcceptedNewEULA = false;//reset this, in case they come back to the EULA activity through other means during the same session.
        }
    }

}
