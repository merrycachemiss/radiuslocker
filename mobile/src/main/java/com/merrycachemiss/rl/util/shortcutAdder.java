/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.constantPINServiceLauncherActivity;
import com.merrycachemiss.rl.lockEmptyActivity;

import java.util.ArrayList;
import java.util.List;

import static com.merrycachemiss.rl.MainActivity.shortcutsMade;

public final class shortcutAdder {

    private static List<ShortcutInfo> existingShortcuts;
    private static String TAG = "";
    private static ShortcutManager shortcutManager;
    private static List<ShortcutInfo> shortcutInfoList;
    private shortcutAdder() {

    }

    public static void getShortcutsAddNewOnes(Context context, String srcTAG){
        TAG = srcTAG;
        if (Build.VERSION.SDK_INT >= 25) {
            shortcutManager = context.getSystemService(ShortcutManager.class);
            shortcutInfoList = new ArrayList<>();
            if (shortcutManager != null) {
                existingShortcuts = shortcutManager.getDynamicShortcuts();
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "got list of RL's existing dynamic shortcuts, there were: " + existingShortcuts.size());
                //202205 note -- I see that I'd made a part that checks if shortcuts exist. Now I can't remember if it was
                //just broken, but for whatever reason I didn't finish it(?). This then seems to re-add the shortcuts, brute-force style, replacing existing?
                Log.d(TAG, "adding all homescreen shortcuts, even if list wasn't empty");
            }
            addAllShortcutDefinitions(context);
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user is < api 25, can't add shortcuts");
            }
        }
    }


    private static void addShortcut1 (Context context){
        if (Build.VERSION.SDK_INT >= 25) {
            Intent intent = new Intent(context.getApplicationContext(), lockEmptyActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            //intent.putExtra("lockNow", 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            ShortcutInfo shortcut1 = new ShortcutInfo.Builder(context, "lock_now")
                    .setIntent(intent)
                    .setShortLabel(context.getResources().getString(R.string.lock_now))
                    .setLongLabel(context.getResources().getString(R.string.lock_phone_now))
                    .setIcon(Icon.createWithResource(context, R.drawable.ic_lock))
                    .setRank(1)//1st in the list
                    .build();

            shortcutInfoList.add(shortcut1);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "lock_now shortcut added");
            }
        }
    }

    private static void addShortcut2 (Context context){
        if (Build.VERSION.SDK_INT >= 25) {
            Intent intent = new Intent(context.getApplicationContext(), constantPINServiceLauncherActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            //intent.putExtra("launch", 1);
            //intent.putExtra("fromAppShortcut", 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            ShortcutInfo shortcut2 = new ShortcutInfo.Builder(context, "constant_pin_manual")
                    .setIntent(intent)
                    .setShortLabel(context.getResources().getString(R.string.constant_pin))
                    .setLongLabel(context.getResources().getString(R.string.start_constant_pin_mode))
                    .setIcon(Icon.createWithResource(context, R.drawable.ic_phone_and_lock))
                    .setRank(2)
                    .build();

            shortcutInfoList.add(shortcut2);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "constant_pin_manual shortcut added");
            }
        }
    }


    @TargetApi(25)
    private static void addAllShortcutDefinitions(Context context){

        addShortcut1(context);
        addShortcut2(context);


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "performing final shortcut add step: addAllShortcutDefinitions()");
        }
        if (!shortcutInfoList.isEmpty()){
            try {
                shortcutManager.setDynamicShortcuts(shortcutInfoList);
                shortcutsMade = true;
            } catch (Exception e){
                shortcutsMade = false;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to perform final shortcut step");
                }
            }

        }
    }

//    private static boolean shortcutExists (String newId) {
//        if (Build.VERSION.SDK_INT >= 25) {
//            int i;
//            String existingShortcutId;
//            for (i = 0; i <= existingShortcuts.size(); i++) {
//                existingShortcutId = existingShortcuts.get(i).getId();
//                if (BuildConfig.DEBUG) {
//                    Log.d(TAG, "existing shortcut id: " + existingShortcutId);
//                }
//                if (Objects.equals(newId, existingShortcutId)){
//                    if (BuildConfig.DEBUG) {
//                        Log.d(TAG, "shortcut id: " + newId + " already existed");
//                    }
//                    return true;
//                } else if (BuildConfig.DEBUG) {
//                    Log.d(TAG, "shortcut id: " + newId + " didn't exist");
//                    return false;
//                }
//            }
//        } else {
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "user is < api 25, can't add shortcuts");
//            }
//            return true;
//        }
//        return false;
//    }
}



