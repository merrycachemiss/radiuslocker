/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import static com.merrycachemiss.rl.dataStorage.dataConstants.LATEST_CONNECTED_WIFI_SSID;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;

import java.util.Objects;

public class getSetCurrentWIFISSID {
    private final Context context;
    private final String TAG = "getSetCurrentWIFISSID";
    private final String sourceClass;
    public getSetCurrentWIFISSID(Context context, String sourceClass) {
        this.context = context;
        this.sourceClass = sourceClass;
    }

    public String getCurrentWIFISSID(){
        String ssid = "";
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    WifiManager WIFIManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    WifiInfo curWIFIinfo = null;
                    if (WIFIManager != null) {
                        curWIFIinfo = WIFIManager.getConnectionInfo();
                    }
                    if (curWIFIinfo != null) {
                        ssid = curWIFIinfo.getSSID();
                        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                            ssid = ssid.substring(1, ssid.length() - 1);
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, sourceClass + ": currently connected to wifi network: " + ssid);
                    }
                    return ssid;
                }
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, sourceClass + ": wifi check fail", e);
            }
            return ssid;
        }
        return ssid;
    }

    public void setCurrentWIFISSID(rldbInsertQueryDelete iqd, String ssid){
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }
        iqd.setSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID, ssid);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, sourceClass + ": stored current wifi network for constant PIN upon disconnect option: " + ssid);
        }
    }



    //distinction between these two:
    //currentlyOnWIFISnoozeSSID() can be called on its own, such as checking for snooze state for locking after a wearable disconnect
    //justLeftAnyWIFISnoozeSSID() can be called in combination with the above to see that it's not on a snoozed wifi, but also that it transitioned away from previously being connected to one.



    public boolean currentlyOnWIFISnoozeSSID(rldbInsertQueryDelete iqd, String ssid){
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }
        final String[] selectionArgs = {ssid};
        if (BuildConfig.DEBUG) {
            Log.d(TAG, sourceClass + " currentlyOnWIFISnoozeSSID(): current SSID " + ssid + " is a WIFI Snooze SSID: " + iqd.genericSingleItemExistsQuery(rldbContract.SafeWIFIQuery.CONTENT_URI, null, selectionArgs));
        }

        return iqd.genericSingleItemExistsQuery(rldbContract.SafeWIFIQuery.CONTENT_URI, null, selectionArgs) && !Objects.equals(ssid, "");
    }

    public boolean justLeftAnyWIFISnoozeSSID(rldbInsertQueryDelete iqd){
        //usually called when current wifi is null/blank, thus disconnected
        final String[] selectionArgs = {iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID)};
        if (BuildConfig.DEBUG) {
            Log.d(TAG, sourceClass + " justLeftAnyWIFISnoozeSSID(): previously stored ssid " + iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID) + " just left a snooze ssid: " + iqd.genericSingleItemExistsQuery(rldbContract.SafeWIFIQuery.CONTENT_URI, null, selectionArgs));
        }
        return iqd.genericSingleItemExistsQuery(rldbContract.SafeWIFIQuery.CONTENT_URI, null, selectionArgs);
    }


}
