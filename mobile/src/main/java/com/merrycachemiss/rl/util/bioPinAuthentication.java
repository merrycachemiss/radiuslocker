/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import static android.content.Context.KEYGUARD_SERVICE;
import static com.merrycachemiss.rl.MainActivity.reauthTriggerTime;
import static com.merrycachemiss.rl.MainActivity.requireBiometricAuth;
import static com.merrycachemiss.rl.MainActivity.isAuthenticated;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REAUTH_TRIGGER_TIME;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REQUIRE_BIOMETRIC_AUTH_USE_APP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.ONLY_USE_PIN_PASS_FOR_RL_AUTH;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SwitchCompat;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class bioPinAuthentication {
    final Activity activity;
    final String TAG = "bioPinAuthentication";
    final String sourceClass;
    final rldbInsertQueryDelete iqd;
    final public static int PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY = 1337;
    final public static int PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_TASKER_TASK = 1338;
    final public static int PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_LOCK_RETRIES = 1339;
    final public static int PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_CONSTANT_PIN = 1340;
    final public static int PIN_ONLY_AUTH_REQUEST_FOR_PAUSE_CONSTANT_PIN = 1341;

    BiometricPrompt.PromptInfo rlBiometricPrompt;
    SwitchCompat requireBiometricAuthSwitch;
    AppCompatCheckBox requirePinPassAuthOnlyCheckbox;

    public bioPinAuthentication(Activity activity, String sourceClass){
        this.activity = activity;
        this.sourceClass = sourceClass;
        this.iqd = new rldbInsertQueryDelete(activity);
    }

    public void authenticate(boolean bumpAuthTimeout, boolean isOnboarding, int authFromNotificationType) {
        boolean canDoBiometrics = BiometricManager.from(activity.getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK) == BiometricManager.BIOMETRIC_SUCCESS || BiometricManager.from(activity.getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS;
        boolean pinPassOnly = iqd.getSettingsTempLongVal(ONLY_USE_PIN_PASS_FOR_RL_AUTH) == 1;
        if (bumpAuthTimeout){
            isAuthenticated = false;
        }
        if (SystemClock.elapsedRealtime() > iqd.getSettingsTempLongVal(REAUTH_TRIGGER_TIME) || isOnboarding || authFromNotificationType > 0) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, sourceClass + " launching auth, isOnboarding = " + isOnboarding);
            }

            if ((!canDoBiometrics || pinPassOnly) && Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                //so BiometricPrompt is worthless (read: buggy as hell) when you want to just use the more-secure PIN authentication only in your app.
                //When you use setAllowedAuthenticators(BiometricManager.Authenticators.DEVICE_CREDENTIAL) only, it crashes on certain devices.
                //So for PIN-only users, surface authentication using the old-fashioned way.

                //But on Android T/13 (on Pixel at least), you must use biometric prompt with the flag to use device credential only.
                //So, 13 devices don't reach this part below, it skips to the next condition. What a mess, but I'm not a saint here either.

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + " using legacy createConfirmDeviceCredentialIntent for auth, because pinPassOnly = " + pinPassOnly + ", canDoBiometrics = " + canDoBiometrics + ", Build.VERSION.SDK_INT = " + Build.VERSION.SDK_INT);
                }
                KeyguardManager keyguardManager = (KeyguardManager) activity.getApplicationContext().getSystemService(KEYGUARD_SERVICE);

                if (keyguardManager != null && keyguardManager.isKeyguardSecure()) {
                    Intent intent = keyguardManager.createConfirmDeviceCredentialIntent(activity.getApplicationContext().getResources().getString(R.string.please_authenticate), null);
                    if (authFromNotificationType != 0) {
                        activity.startActivityForResult(intent, authFromNotificationType);
                    } else {
                        activity.startActivityForResult(intent, PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY);
                    }
                } else {
                    //user doesn't have any locking info set, so we have to let them perform whatever they were doing.
                    //if the user's credentials were removed, all bets are off.
                    if (!isOnboarding) {
                        broadcastAuthResult(true, authFromNotificationType, false, canDoBiometrics, pinPassOnly);
                    } else {
                        //if user was onboarding and doesn't have PIN/etc set up. trigger failure flow related to that.
                        authOnboardingResult(false, true);
                    }
                }


            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + " using BiometricPrompt for auth");
                }
                Executor biometricExecutor = Executors.newSingleThreadExecutor();
                final String biometricTitle = activity.getApplicationContext().getResources().getString(R.string.please_authenticate);

                //have to do the pin only check, because the initial IF above only triggers the legacy credential intent < android 13.
                if (isOnboarding) {
                    final String setupAuth = activity.getApplicationContext().getResources().getString(R.string.set_up_biometric_auth);
                    if (!pinPassOnly && canDoBiometrics) {
                        rlBiometricPrompt = new BiometricPrompt.PromptInfo.Builder()
                                .setTitle(biometricTitle)
                                .setDescription(setupAuth)
                                .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG | BiometricManager.Authenticators.BIOMETRIC_WEAK | BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                                .build();
                    } else {
                        rlBiometricPrompt = new BiometricPrompt.PromptInfo.Builder()
                                .setTitle(biometricTitle)
                                .setDescription(setupAuth)
                                .setAllowedAuthenticators(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                                .build();
                    }
                } else {
                    if (!pinPassOnly && canDoBiometrics) {
                        rlBiometricPrompt = new BiometricPrompt.PromptInfo.Builder()
                                .setTitle(biometricTitle)
                                .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG | BiometricManager.Authenticators.BIOMETRIC_WEAK | BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                                .build();
                    } else {
                        rlBiometricPrompt = new BiometricPrompt.PromptInfo.Builder()
                                .setTitle(biometricTitle)
                                .setAllowedAuthenticators(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                                .build();
                    }
                }



                final BiometricPrompt myBiometricPrompt = new BiometricPrompt((FragmentActivity) activity, biometricExecutor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                    boolean hwSwFailure = false;
                    if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON || errorCode == BiometricPrompt.ERROR_USER_CANCELED) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, sourceClass + " user cancelled biometric prompt");
                        }
                        if (!isOnboarding) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, sourceClass + " exiting app");
                            }
                            activity.finishAffinity();
                        } else {
                            authOnboardingResult(false, false);
                        }
                    } else if (errorCode == BiometricPrompt.ERROR_NO_BIOMETRICS || errorCode == BiometricPrompt.ERROR_HW_NOT_PRESENT || errorCode == BiometricPrompt.ERROR_NO_DEVICE_CREDENTIAL) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, sourceClass + " no biometric hw present. error code: " + errorCode);
                        }
                        hwSwFailure = true;
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, sourceClass + " unrecoverable biometric prompt error - too many failures, or user left activity");
                        }
                    }
                    try {
                        activity.runOnUiThread(() -> Toast.makeText(activity,
                                activity.getApplicationContext().getResources().getString(R.string.auth_failure),
                                Toast.LENGTH_LONG).show());
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, sourceClass + " failed to toast", e);
                        }
                    }
                    if (isOnboarding) {
                        authOnboardingResult(false, hwSwFailure);
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, sourceClass + " exiting app");
                        }

                        //I think here you might need a counter to avoid immediate minimize of activity. give them a few tries before kickout.
                        //there's a second place, onAuthenticationFailed(), that has the same thing.
                        activity.finishAffinity();
                    }
                    broadcastAuthResult(false, authFromNotificationType, hwSwFailure, canDoBiometrics, pinPassOnly);
                    iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
                }


                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, sourceClass + " biometric auth success");
                    }
                    if (bumpAuthTimeout) {
                        //set auth timeout to 5 minutes from now, but only if it was via an activity
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "bumping auth timeout");
                        }
                        iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, SystemClock.elapsedRealtime() + 300000);
                    }
                    broadcastAuthResult(true, authFromNotificationType, false, canDoBiometrics, pinPassOnly);
                    if (isOnboarding) {
                        authOnboardingResult(true, false);
                    }
                }

                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, sourceClass + " biometric auth fail");
                    }
                    try {
                        activity.runOnUiThread(() -> Toast.makeText(activity,
                                activity.getApplicationContext().getResources().getString(R.string.auth_failure),
                                Toast.LENGTH_LONG).show());
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, sourceClass + " failed to toast", e);
                        }
                    }
                    broadcastAuthResult(false, authFromNotificationType, false, canDoBiometrics, pinPassOnly);
                    if (!isOnboarding) {
                        //I think here you might need a counter to avoid immediate minimize of activity. give them a few tries before kickout.
                        //activity.finishAffinity();
                        iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
                    } else {
                        authOnboardingResult(false, false);
                    }
                }

            });

            myBiometricPrompt.authenticate(rlBiometricPrompt);

            }


        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, sourceClass + " user not using biometric auth or is within 5 minute cutoff");
            }
            broadcastAuthResult(true, authFromNotificationType, false, false, false);
        }
    }
    private void broadcastAuthResult(boolean isSuccess, int authFromNotificationType, boolean hwSwFailure, boolean canDoBiometrics, boolean pinPassOnly){

        Intent authResult = new Intent("com.merrycachemiss.rl.authActivityResult");
        authResult.putExtra("success", isSuccess);
        authResult.putExtra("canDoBiometrics", canDoBiometrics);
        authResult.putExtra("pinPassOnly", pinPassOnly);
        if (hwSwFailure){
            authResult.putExtra("hwSwFailure", hwSwFailure);
        }
        if (authFromNotificationType != 0) {
            authResult.putExtra("authFromNotificationType", authFromNotificationType);
        }
        LocalBroadcastManager.getInstance(activity).sendBroadcast(authResult);

    }


    public void adjustTimeoutForPresence(boolean potentiallyMinimizedApp) {
        if (potentiallyMinimizedApp) {
            //bump re-auth time by a little. If they then go into a new activity within this app, it will be bumped by 5 minutes.
            //Otherwise, minimizing this app and going into another app will require PIN again, immediately.
            reauthTriggerTime = SystemClock.elapsedRealtime() + 5000;
            iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, reauthTriggerTime);
        } else {
            if (SystemClock.elapsedRealtime() < iqd.getSettingsTempLongVal(REAUTH_TRIGGER_TIME)) {
                //bump re-auth time by 5 minutes.
                reauthTriggerTime = SystemClock.elapsedRealtime() + 30000;
                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, reauthTriggerTime);
            }
        }
    }




    //kinda messy to control an activity's ui element from this class, but I'm not sure how else to do it so easily.
    //The solution I'd seen seemed to require a lot of extra fluff just to properly separate concerns, which is ideal, but I'm not good enough to fully understand it quickly.
    public void authOnboardingResult(boolean isSuccess, boolean isHardwareSoftwareError) {
        if (requireBiometricAuthSwitch == null || requirePinPassAuthOnlyCheckbox == null) {
            requireBiometricAuthSwitch = activity.findViewById(R.id.requireBiometricAuthSwitch);
            requirePinPassAuthOnlyCheckbox = activity.findViewById(R.id.requirePinPassAuthOnlyCheckbox);
        }
        if (isSuccess) {
            requireBiometricAuth = 1;
            iqd.setSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP, requireBiometricAuth);
            activity.runOnUiThread(() -> {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + " set biometric auth switch to checked");
                }
                requireBiometricAuthSwitch.setChecked(true);
                requireBiometricAuthSwitch.setText(activity.getApplicationContext().getResources().getString(R.string.require_auth_open_app));

                //only enable checkbox if user has biometrics. Otherwise, they're just using PIN anyway.
                if (BiometricManager.from(activity.getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK) == BiometricManager.BIOMETRIC_SUCCESS
                        || BiometricManager.from(activity.getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS){
                    requirePinPassAuthOnlyCheckbox.setEnabled(true);
                }

            });

        } else {
            requireBiometricAuth = 0;
            iqd.setSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP, requireBiometricAuth);
            activity.runOnUiThread(() -> {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + " reset biometric auth switch to unchecked");
                }
                requireBiometricAuthSwitch.setChecked(false);
                if (isHardwareSoftwareError){
                    requireBiometricAuthSwitch.setText(activity.getApplicationContext().getResources().getString(R.string.require_auth_open_app_unavailable));
                }
            });
        }


    }


}
