/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_REFRESHER_QUEUED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SerialProjection.SERIAL_PROJECTION;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_TASK;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.lockTasks.lockDecider;

import java.util.Objects;

public class connDisconnTasks {

    final Context context;
    private final String TAG = "connDisconnTasks";
    private final String sourceClass;
    public connDisconnTasks(Context context, String sourceClass){
        this.context = context;
        this.sourceClass = sourceClass;
    }

    public void connTasks(String connectedDeviceSerial){
        boolean btEnabled = false;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            btEnabled = bluetoothAdapter.isEnabled();
        }

        if (btEnabled){
            rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
            //now only adds to table as connected=1 if BT is on. Sometimes the test device would have phantom reconnections after toggling BT a couple of times. Fools.
            //add/update to connected device table, connected = 1
            Intent btDeviceConnDisconnIntent = new Intent("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
            iqd.BTConnStatusInsert(Objects.requireNonNull(connectedDeviceSerial), 1);
            context.sendBroadcast(btDeviceConnDisconnIntent, "com.merrycachemiss.rl");

            if (BuildConfig.DEBUG) {
                Log.d(TAG, sourceClass + ": " + connectedDeviceSerial + " connected and added to db");
            }
            final String[] selectionArgs = {connectedDeviceSerial};
            boolean isMonitoredBTdevice = iqd.genericSingleItemExistsQuery(rldbContract.MonitoredBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);
            if (isMonitoredBTdevice) {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + ": " + "try to cancel active LOCK RETRY scheduled job after wearable returns - failure could suggest there isn't one");
                }
                try {
                    jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                    cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                    cancelClear.cancelJobClearNotif();
                } catch (Exception e) {

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, sourceClass + ": " + "failed to cancel active LOCK RETRY scheduled job -- maybe there was none?");
                    }

                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + ": " + "try to cancel active TASKER scheduled job after wearable returns - failure could suggest there isn't one");
                }
                try {
                    jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                    cancelClear.setJobAndNotifParams(TASKER_TASK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_TASK, TASKER_JOB_SCHEDULED);
                    cancelClear.cancelJobClearNotif();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, sourceClass + ": " + "failed to cancel active TASKER scheduled job -- maybe there was none?");
                    }

                }

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, sourceClass + ": " + "try to cancel alarm after monitored BT return");
                }
//                try {
//                    cancelDelayedLock(context);
//                } catch (Exception e) {
//                    if (BuildConfig.DEBUG) {
//                        Log.e(TAG, sourceClass + ": " + "failed to cancel alarm after lock", e);
//                    }
//
//                }

                //used for constant PIN related stuff for when watch is missing and user wants constant PIN mode because of it
                iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED, 0);

            }
            iqd = null;
        }
        bluetoothAdapter = null;

        //release this stuff immediately, in case such a thing is needed in a long-running service.
        //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
        //Though, my linter is complaining weirdly about this.
    }


    @SuppressLint("MissingPermission")
    public void disconnTasks(final BluetoothDevice disconnectedDevice){
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);

        permissionHelper perm = new permissionHelper(context);

        Intent btDeviceConnDisconnIntent = new Intent("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
        iqd.BTConnStatusInsert(disconnectedDevice.getAddress(), 0);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, sourceClass + ": " + disconnectedDevice + " disconnected and removed from db");
        }
        context.sendBroadcast(btDeviceConnDisconnIntent, "com.merrycachemiss.rl");

        String deviceName = "";
        if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S, iqd, BT_REFRESHER_QUEUED, 1)){
            deviceName = disconnectedDevice.getName();
        } else {
            deviceName = "missingperm";
            perm.surfaceNotificationForMissingPerms();
            //If they use a BT device for snoozing (including multi-watch lock prevention), we will make it so the user will
            //refresh BT when they go into the main activity, since we temporarily lost the state of their devices
            //while the permission was missing:
            if (iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE) == 1 || iqd.genericSingleItemExistsQuery(rldbContract.ConnectedSafeBTQuery.CONTENT_URI, null, null)){
                iqd.setSettingsTempLongVal(BT_REFRESHER_QUEUED, 1);
            }
        }
        final lockDecider locker = new lockDecider();
        final long currentTime = SystemClock.elapsedRealtime();
        try {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, sourceClass + ": " + "putting locker.lockIfValid in its own new thread," +
                        " so the lock delay option can do its countdown latch business without blocking other disconnect processing");
            }
            String finalDeviceName = deviceName;

            Runnable run = () -> {
                locker.lockIfValid(context, disconnectedDevice.getAddress(), finalDeviceName, 0, 0, 0, currentTime);
            };

            Thread disconnectThread = new Thread(run);
            disconnectThread.start();

        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, sourceClass + ": " + "Couldn't try to lock when locking device disconnected. Maybe in preboot mode.", e);
            }
        }
    }

    @SuppressLint("MissingPermission")
    public void disconnTasksWearNode(String serial){
        //WearableListenerSvc already sends signal to main service for locking. this just performs the DB ops related to a disconnect.

        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);

        permissionHelper perm = new permissionHelper(context);

        iqd.BTConnStatusInsert(serial, 0);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, sourceClass + ": " + serial + " disconnected and removed from db");
        }

        if (perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S, iqd, BT_REFRESHER_QUEUED, 1)){
            perm.surfaceNotificationForMissingPerms();
            //If they use a BT device for snoozing (including multi-watch lock prevention), we will make it so the user will
            //refresh BT when they go into the main activity, since we temporarily lost the state of their devices
            //while the permission was missing:
            if (iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE) == 1 || iqd.genericSingleItemExistsQuery(rldbContract.ConnectedSafeBTQuery.CONTENT_URI, null, null)){
                iqd.setSettingsTempLongVal(BT_REFRESHER_QUEUED, 1);
            }
        }


        serial = null;

    }

}
