/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.MainActivity;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.notifHandlerEmptyActivity;

import static androidx.core.app.NotificationCompat.CATEGORY_STATUS;
import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;
import static androidx.core.app.NotificationCompat.PRIORITY_MAX;

public final class NotificationHelper {
    private NotificationManager mNotificationManager;
    private String channel;
    private final Context context;
    private final String TAG = "NotificationHelper";
    public static final int NOTIF_ID_BT_MON_SERVICE = 1;
    public static final int NOTIF_ID_LOCK_TASK = 2;
    public static final int NOTIF_ID_BT_REFRESH = 3;
    public static final int NOTIF_ID_CANCEL_SCHEDULED_LOCK = 4;
    public static final int NOTIF_ID_CANCEL_SCHEDULED_TASK = 5;
    public static final int NOTIF_ID_CONSTANT_PIN_MODE = 6;
    public static final int NOTIF_ID_NEW_EULA = 7;
    public static final int NOTIF_ID_MISSING_PERMISSIONS = 8;

    public static final String SCHEDULED_EVENTS = "scheduled_events";


    public NotificationHelper(Context context) {
        this.context = context;
    }

    public void createNotif (String title, String message, String channel, String channelNameLocalized, boolean useSound, int importance, int icon, int notifId, boolean isCancellable) {
        permissionHelper perm = new permissionHelper(context);

        createNotifChannelOreo(channel, channelNameLocalized, importance, useSound);
        try {
        NotificationCompat.Builder builder = getScheduledItemNotificationBuilder(title, message, icon, notifId, isCancellable);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        if (!perm.isMissingRequiredPerm(Manifest.permission.POST_NOTIFICATIONS, Build.VERSION_CODES.TIRAMISU)){
            notificationManager.notify(notifId, builder.build());
        }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to create notif", e);
            }
        }
    }


    public void createNotifChannelOreo(String channelID, String channelName, int importance, boolean useSound) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "attempt createNotifChannelOreo");
            }
            this.channel = channelID;

            try {
                // Created channelID obj w unique ID
                NotificationChannel myChannel = new NotificationChannel(channel, channelName, importance);

                if (!useSound) {
                    //most RL notifications don't need sound. Only super important stuff, like the "issues" channel, should use it.
                    myChannel.setSound(null, null);
                }
                myChannel.setShowBadge(false);
                myChannel.enableLights(false);

                getNotificationManager().createNotificationChannel(myChannel);
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to create notication channel: " + channel + ", name: " + channelName + ", importance: " + importance + " error: " + e);
                }
            }
        }
    }




    @SuppressLint("UnspecifiedImmutableFlag")
    public NotificationCompat.Builder btMonRefreshLockerNotifBuilder(String title, int icon, Intent intent, boolean onGoing) {

        //launch main activity if notif tapped
        Intent mainActivityLaunchIntent = new Intent(context, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent mainActivityLaunchPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return new NotificationCompat.Builder(context, channel)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentIntent(mainActivityLaunchPendingIntent)
                .setShowWhen(false)
                .setOngoing(onGoing)
                .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                .setAutoCancel(false)
                .setLocalOnly(true)
                .setGroup(title)//easy way to split this up, already passing in title.
                .setPriority(PRIORITY_DEFAULT);
    }


    //this is used for both bedtime protection mode AND generic mode to constantly require PIN until cancelled.
    @SuppressLint("UnspecifiedImmutableFlag")
    public NotificationCompat.Builder constantPINNotifBuilder(String title, String message, int icon, Intent intent, boolean onGoing, int cancelPauseButtonItem) {

        //launch main activity if notif tapped
        Intent mainActivityLaunchIntent = new Intent(context, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent mainActivityLaunchPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        //if (isPausible) {
        Intent pauseAction = new Intent(context, notifHandlerEmptyActivity.class);
        pauseAction.setAction("pauseAction");
        pauseAction.putExtra("pauseButtonItem", cancelPauseButtonItem);

        PendingIntent pauseButtonPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pauseButtonPendingIntent = PendingIntent.getActivity(
                    context,
                    cancelPauseButtonItem,
                    pauseAction,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pauseButtonPendingIntent = PendingIntent.getActivity(
                    context,
                    cancelPauseButtonItem,
                    pauseAction,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        Intent cancelAction = new Intent(context, notifHandlerEmptyActivity.class);
        cancelAction.setAction("cancelAction");
        cancelAction.putExtra("cancelButtonItem", cancelPauseButtonItem);
        PendingIntent cancelButtonPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            cancelButtonPendingIntent = PendingIntent.getActivity(
                    context,
                    cancelPauseButtonItem,
                    cancelAction,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            cancelButtonPendingIntent = PendingIntent.getActivity(
                    context,
                    cancelPauseButtonItem,
                    cancelAction,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        String cancelText;
        try {
            cancelText = context.getApplicationContext().getResources().getString(android.R.string.cancel);
        } catch (Exception e) {
            cancelText = "Cancel";
        }


        return new NotificationCompat.Builder(context, channel)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentIntent(mainActivityLaunchPendingIntent)
                .setShowWhen(false)
                .setOngoing(onGoing)
                .setAutoCancel(false)//must be false, don't want user to open activity by normal press on content and then have it clear itself
                .setLocalOnly(true)
                .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                .setPriority(PRIORITY_DEFAULT)
                .setGroup(title)//easy way to split this up, already passing in title.

                .addAction(R.drawable.ic_cancel_black_24dp, cancelText, cancelButtonPendingIntent)
                .addAction(R.drawable.ic_pause_circle_filled_black_24dp, context.getApplicationContext().getResources().getString(R.string.pause_15_notif_button), pauseButtonPendingIntent);
    }
    
    @SuppressLint("UnspecifiedImmutableFlag")
    public NotificationCompat.Builder getScheduledItemNotificationBuilder(String title, String message, int icon, int cancelButtonItem, boolean isCancellable) {

        //launch main activity if notif tapped
        Intent mainActivityLaunchIntent = new Intent(context, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent mainActivityLaunchPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        //this never works. So for now, the cancel/pause buttons do nothing, when the screen is locked. There's a screen locked check upon exec.
//        NotificationCompat.Builder publicVersionOfNotif = new NotificationCompat.Builder(context, channel)
//                .setSmallIcon(icon)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
//                .setContentIntent(mainActivityLaunchPendingIntent)
//                .setWhen(System.currentTimeMillis())
//                .setShowWhen(true)
//                .setOngoing(true)
//                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                .setCategory(CATEGORY_STATUS)
//                .setAutoCancel(false)//must be false, don't want user to open activity by normal press on content and then have it clear itself
//                .setPriority(PRIORITY_DEFAULT);

        if (isCancellable) {
            Intent cancelAction = new Intent(context, notifHandlerEmptyActivity.class);
            cancelAction.setAction("cancelAction");
            cancelAction.putExtra("cancelButtonItem", cancelButtonItem);
            PendingIntent cancelButtonPendingIntent;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                cancelButtonPendingIntent = PendingIntent.getActivity(
                        context,
                        cancelButtonItem,
                        cancelAction,
                        PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                cancelButtonPendingIntent = PendingIntent.getActivity(
                        context,
                        cancelButtonItem,
                        cancelAction,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }

            String cancelText;
            try{
                cancelText = context.getApplicationContext().getResources().getString(android.R.string.cancel);
            } catch (Exception e){
                cancelText = "Cancel";
            }

            return new NotificationCompat.Builder(context, channel)
                    .setSmallIcon(icon)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setContentIntent(mainActivityLaunchPendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)
                    .setOngoing(true)
                    .setLocalOnly(true)
                    //.setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                    .setCategory(CATEGORY_STATUS)
                    .setAutoCancel(false)//must be false, don't want user to open activity by normal press on content and then have it clear itself
                    .setPriority(PRIORITY_DEFAULT)
                    .setGroup(title)//easy way to split this up, already passing in title.
                    .addAction(R.drawable.ic_cancel_black_24dp, cancelText, cancelButtonPendingIntent)
                    //.setPublicVersion(publicVersionOfNotif.build())
                    ;
        } else {
            //this is used for the backup of alarm manager lock job. not cancellable but can be swiped away. no notif description shown.
            return new NotificationCompat.Builder(context, channel)
                    .setSmallIcon(icon)
                    .setContentTitle(title)
                    //.setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setContentIntent(mainActivityLaunchPendingIntent)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(true)
                    .setOngoing(false)
                    .setLocalOnly(true)
                    //.setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                    .setCategory(CATEGORY_STATUS)
                    .setAutoCancel(false)//must be false, don't want user to open activity by normal press on content and then have it clear itself
                    .setPriority(PRIORITY_DEFAULT)
                    .setGroup(title)//easy way to split this up, already passing in title.
                    //.addAction(R.drawable.ic_cancel_black_24dp, cancelText, cancelButtonPendingIntent)
                    //.setPublicVersion(publicVersionOfNotif.build())
                    ;
        }
    }




    @SuppressLint("UnspecifiedImmutableFlag")
    public NotificationCompat.Builder getGeneralStickyNotificationBuilder(String title, String message, int icon) {

        //launch main activity if notif tapped
        Intent mainActivityLaunchIntent = new Intent(context, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent mainActivityLaunchPendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            mainActivityLaunchPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    mainActivityLaunchIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }
            return new NotificationCompat.Builder(context, channel)
                    .setSmallIcon(icon)
                    .setContentTitle(title)
                    //.setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setContentIntent(mainActivityLaunchPendingIntent)
                    .setOngoing(true)
                    .setLocalOnly(true)
                    .setCategory(CATEGORY_STATUS)
                    .setAutoCancel(false)//must be false, don't want user to open activity by normal press on content and then have it clear itself
                    .setPriority(PRIORITY_MAX)
                    .setGroup(title)//easy way to split this up, already passing in title.
                    //.addAction(R.drawable.ic_cancel_black_24dp, cancelText, cancelButtonPendingIntent)
                    //.setPublicVersion(publicVersionOfNotif.build())
                    ;
    }

    public static void clearNotification(Context context, String srcClassTag, int cancelItem){
        if (BuildConfig.DEBUG) {
            Log.d("notifHelper", "try to clear notif id " + cancelItem + " after cancel related job" + " from " + srcClassTag);
        }
        try {//cancelItem matches notification ID
            NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancel(cancelItem);
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d("notifHelper", "failed to clear notif id " + cancelItem + " after cancel related job" + " from " + srcClassTag, e);
            }
        }
    }

    private NotificationManager getNotificationManager() {
        if (mNotificationManager == null) {
            mNotificationManager =
                    (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }



}

