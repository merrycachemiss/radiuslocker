/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import static android.content.Context.JOB_SCHEDULER_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

import android.Manifest;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.SystemClock;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.Wearable;
import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.jobs.constantPinHeartbeatJob;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.jobs.postPauseConstantPinLockJob;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.receivers.DeviceAdmin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.merrycachemiss.rl.dataStorage.dataConstants.BEDTIME_PROTECTION_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_UPON_LOCKING_WATCH_DISCONNECT_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_BY_CHOICE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CONSTANT_PIN_MODE;
import static com.merrycachemiss.rl.util.NotificationHelper.SCHEDULED_EVENTS;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public final class constantPinHelper {
    private static final String TAG = "constantPinHelper";
    private static final int constPINAlarmRequestCode = 6969;
    private final Context context;

    public constantPinHelper(Context context) {
        this.context = context;
    }

    //references to "service" in the log entries are old. It's no longer its own service, but merged into the main long-running service
    //because of the insane background execution limits that go a bit too far. Instead it just creates a generic notif that's not service-related,
    //if constant PIN mode activation is valid.

    public boolean watchMissingAndConstantPinEnabledOrIsManuallyEnabled(rldbInsertQueryDelete iqd) {
        //CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED is an unused feature atm. It would start constant PIN mode when a locking wearable disconnects and locks the phone.

        return (iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_UPON_LOCKING_WATCH_DISCONNECT_SETTING) == 1
                && iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED) == 1) || iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE) == 1;
    }


    public long getAlarmTime() {
        long alarmClockTime = 0;
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                AlarmManager.AlarmClockInfo alarmClockInfo = alarmManager.getNextAlarmClock();
                alarmClockTime = alarmClockInfo.getTriggerTime();
                if (BuildConfig.DEBUG) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                    String timeFormatted = sdf.format(new Date(alarmClockTime));
                    Log.d(TAG, "next alarm time: " + alarmClockTime + ", " + timeFormatted);
                }
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "no alarm time set, so it crashed the alarm time check");
            }
        }
        return alarmClockTime;
    }

    public void activateConstantPINServiceIfNeeded(Context context, rldbInsertQueryDelete iqd) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "app was updated or heartbeat bumped, checking if need to run service (only if within 8hr) or if need to run/continue heartbeat (only if is charging or sleep mode)");
        }

        if (bedtimeModeAndWithin8hrLockingIsValid(iqd) || watchMissingAndConstantPinEnabledOrIsManuallyEnabled(iqd)) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "though we've just scheduled the heartbeat job, we are going to try and start constantPINservice now anyway since it's within the locking period. it will cancel the job again, but restart it");
            }

            constantPinModeActivateNow(false);

        } else if (isCharging(context)) {
            jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
            cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID, 0, 0);
            cancelClear.cancelJobClearNotif();

            if (BuildConfig.DEBUG) {

                Log.d(TAG, "Didn't start bedtime mode, because it's not within 8HR. Instead, just cancelled heartbeat job and then sleeping for .45s before trying to re-reg heartbeat again (in case it was already running, don't want to risk two of them if that can even happen.");
            }
            try {
                Thread.sleep(450);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to sleep", e);
                }
            }

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "try to set up constant PIN scheduleHeartbeatJob() after just cancelling it");
            }
            ComponentName componentName = new ComponentName(context, constantPinHeartbeatJob.class);
            JobInfo jobInfo = new JobInfo.Builder(CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID, componentName)
                    .setMinimumLatency((15 * 60 * 1000))//15 MIN
                    .setOverrideDeadline((16 * 60 * 1000))//16 MIN
                    .setPersisted(false)
                    .setBackoffCriteria((15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
                    .build();
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
            int resultCode = 0;
            if (jobScheduler != null) {
                resultCode = jobScheduler.schedule(jobInfo);
                if (resultCode == JobScheduler.RESULT_SUCCESS) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Job schedule success: constant PIN scheduleHeartbeatJob()");
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Job schedule fail: constant PIN scheduleHeartbeatJob()");
                    }
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Job schedule fail: constant PIN scheduleHeartbeatJob(). Jobscheduler is null.");
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "constant pin not started because not charging and not within 8hr of alarm, or manual mode not enabled");
            }
        }


    }

    public void createConstantPINNotificationIfNotExists() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createConstantPINNotificationIfNotExists");
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            StatusBarNotification[] notifications = mNotificationManager.getActiveNotifications();
            //only create notification if not already exists. if found in the loop, the method will just "return" and not create it.
            //otherwise, the loop will complete and reach the next method to create it.
            for (StatusBarNotification notification : notifications) {
                if (notification.getId() == NOTIF_ID_CONSTANT_PIN_MODE) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Constant PIN notification already exists. Won't make a new one.");
                    }
                    return;
                }
            }
            createConstantPINNotif();

        } else {
            createConstantPINNotif();
        }
    }


    private void createConstantPINNotif() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createConstantPINNotif(), as a notif for it was not already found.");
        }
        NotificationHelper notificationHelper = new NotificationHelper(context.getApplicationContext());
        notificationHelper.createNotifChannelOreo("constant_pin_mode", context.getApplicationContext().getResources().getString(R.string.constant_pin_notif_channel),
                3, false);
        NotificationCompat.Builder builder = notificationHelper.constantPINNotifBuilder(context.getApplicationContext().getResources().getString(R.string.constant_pin_notif_description), context.getApplicationContext().getResources().getString(R.string.constant_pin_notif_cancel_message), R.drawable.ic_lock,
                null, true, NOTIF_ID_CONSTANT_PIN_MODE);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
            notificationManager.notify(NOTIF_ID_CONSTANT_PIN_MODE, builder.build());
        }

    }






    //setting is enabled, and was not flagged as cancelled for the night by user (checks alarm time vs temp stored val).
    //this is usually queried when trigger job is started from charging while idle.
    //the service will start and then kick off the heartbeat. the heartbeat will then determine whether or not the service will continue.
    public boolean startBedtimeModeHeartbeatAfterChargeValid(rldbInsertQueryDelete iqd) {
        //since we are charging, we can start the service, which kicks off the heartbeat.
        //but right here we won't check if within 8HR window, as the service does that.
        //The service will shut itself down if not within 8HR, but will keep the heartbeat going.
        long alarmClockTime = getAlarmTime();
        if (BuildConfig.DEBUG){
            Log.d(TAG, "even if user has no alarms set, we plan to start the service and heartbeat. service will die if no alarm set, but if charging then the heartbeat will continue, in case they later set an alarm. alarmClockTime: " + alarmClockTime);
        }
        return iqd.getSettingsTempLongVal(BEDTIME_PROTECTION_SETTING) == 1
                && (alarmClockTime != iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP) || alarmClockTime == 0)

                //&& System.currentTimeMillis() < alarmClockTime//in case they don't have a future alarm right now
                //&& alarmClockTime > 0
                ;
    }

    public void constantPinModeActivateNow(boolean manuallyEngaged){

        //when constant PIN mode starts, the first thing it must do is lockdown if the screen is OFF, if activated automatically
        //if activated manually, it does it if the screen is on but display is locked
        if (manuallyEngaged) {
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (keyguardManager != null && keyguardManager.isKeyguardLocked()) {

                try {
                    immediateLocker lock = new immediateLocker(context, false, true, TAG);
                    lock.lockNOW();

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "manual constant PIN mode in effect");
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to lock phone for constant PIN mode start");
                    }
                }
            }
        } else {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            if (dm != null) {
                for (Display display : dm.getDisplays()) {
                    if (display.getState() != Display.STATE_ON) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "const pin mode just started via charging trigger job, just scheduled heartbeat due to time cutoff validity, and display is off. lock.");
                        }
                        try {
                            immediateLocker lock = new immediateLocker(context, false, true, TAG);
                            lock.lockNOW();
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "failed to lock phone for constant PIN mode start");
                            }
                        }
                    }
                }
            }
        }

        createConstantPINNotificationIfNotExists();
    }


    //setting is enabled, and phone is within 8hr of next alarm and was not flagged as cancelled for the night by user (checks alarm time vs temp stored val)
    public boolean bedtimeModeAndWithin8hrLockingIsValid(rldbInsertQueryDelete iqd) {

        long alarmClockTime = getAlarmTime();
        //CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP is for when they've cancelled bedtime mode inside of their active wakeup alarm, but for the situation where
        //they later hit "snooze" on their active alarm. RL would think it's a new alarm when this happens. This var helps prevent returning of bedtime mode when they snooze their alarm,
        //for a little bit.
        long cancelledConstantPinAlarmTime = iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP);
        return iqd.getSettingsTempLongVal(BEDTIME_PROTECTION_SETTING) == 1
                && System.currentTimeMillis() > alarmClockTime-28800000
                && System.currentTimeMillis() < alarmClockTime//in case they don't have a future alarm right now
                && alarmClockTime != cancelledConstantPinAlarmTime
                && alarmClockTime > 0
                && !inSnoozeOfCancelledAlarm(cancelledConstantPinAlarmTime)//if they hit snooze, don't want to bring back the constant PIN. Snooze changes next alarm time. 16 min window
                ;
    }



    //don't want to keep bringing back cancelled constant PIN after that particular alarm starts snoozing. it's set to a 15 minute window.
    private boolean inSnoozeOfCancelledAlarm(long cancelledConstantPinAlarmTime){
        if (BuildConfig.DEBUG){
            boolean inSnoozeOfCancelledAlarm = System.currentTimeMillis() > cancelledConstantPinAlarmTime && System.currentTimeMillis() < cancelledConstantPinAlarmTime+900000;
            Log.d(TAG, "inSnoozeOfCancelledAlarm: " + inSnoozeOfCancelledAlarm);
            return inSnoozeOfCancelledAlarm;
        } else {
            return System.currentTimeMillis() > cancelledConstantPinAlarmTime && System.currentTimeMillis() < cancelledConstantPinAlarmTime + 900000;
        }
    }

    public int stopConstantPinIfNecessary(Context context, rldbInsertQueryDelete iqd, boolean fromHeartBeatItself){
        //not charging, not within 8hr, not manually enabled
        boolean isCharging = isCharging(context);

        boolean bedtimeModeAndWithin8hrLockingIsValid = bedtimeModeAndWithin8hrLockingIsValid(iqd);
        boolean watchMissingAndConstantPinMissingEnabledOrManuallyEnabled = watchMissingAndConstantPinEnabledOrIsManuallyEnabled(iqd);
        if (!isCharging && !bedtimeModeAndWithin8hrLockingIsValid && !watchMissingAndConstantPinMissingEnabledOrManuallyEnabled){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user is no longer charging the phone AND no disconnected wearable is flagged (or user is not using that setting). didn't lock. clearing notif AND heartbeat job.");
            }

            killConstantPinServiceNotif(context, true, fromHeartBeatItself);
            manageConstantPIN15minPauseJob(context, false);

            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);
            return 1;


        //still charging, no 8hr, no watches missing. keep heartbeat going though!
        } else if (isCharging && !bedtimeModeAndWithin8hrLockingIsValid && !watchMissingAndConstantPinMissingEnabledOrManuallyEnabled){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "charging, no disconnected wearable is flagged. clearing notif but keeping heartbeat job since still charging.");
            }
            killConstantPinServiceNotif(context, false, false);
            manageConstantPIN15minPauseJob(context, false);
            return 2;
        } else{
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "stopConstantPinIfNecessary - no action needed. isCharging: " + isCharging + " bedtimeModeAndWithin8hrLockingIsValid: " + bedtimeModeAndWithin8hrLockingIsValid + " watchMissingAndConstantPinMissingEnabledOrManuallyEnabled " + watchMissingAndConstantPinMissingEnabledOrManuallyEnabled);
            }
        }
        return 0;

    }


    private void killConstantPinServiceNotif(Context context, boolean changeHeartbeat, boolean fromHeartBeatItself) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "killConstantPinServiceNotif() - will also kill heartbeat job if necessary");
        }
        jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);

        if (changeHeartbeat) {
            if (!fromHeartBeatItself) {//heartbeat will do the killing of itself when calling this
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "killing heartbeat job since this was not called from the job itself. the job itself will kill itself instead when the time comes");
                }
                cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID, 0, 0);
                cancelClear.cancelJobClearNotif();
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "heartbeat job not killed from helper, should kill itself");
                }
            }
        }
        cancelClear.setJobAndNotifParams(0, NOTIF_ID_CONSTANT_PIN_MODE, 0);
        cancelClear.cancelJobClearNotif();



    }
    //all scheduling and cancellation should eventually be migrated into here, since there's a lot of duplication in the service itself, settings,
    //heartbeat job, and trigger job

    public String startConstantPINByChoice(Context context, boolean fromWear, String sourceNodeId, boolean fromQuickTile) {//}, boolean fromAppShortcut){
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName deviceAdminComponentName = new ComponentName(context, DeviceAdmin.class);
        boolean has_admin_rights = false;
        try {
            if (devicePolicyManager != null) {
                has_admin_rights = devicePolicyManager.isAdminActive(deviceAdminComponentName);
            }
        } catch (Exception e){
            Log.d(TAG, "failed to check if has admin, devicePolicyManager likely null");
        }

        if (has_admin_rights) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "clicked constant PIN quick tile, starting constant PIN service");
            }
            rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE, 1);


            //Intent constantPINServiceIntent = new Intent(context, constantPINservice.class);

            //constantPINServiceIntent.putExtra("fromWear", fromWear);
            KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
            if (fromWear || (keyguardManager != null && keyguardManager.isKeyguardLocked())){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Constant PIN service started from wear (" + fromWear + "), or from quick tile (" + fromQuickTile + "), or from double tap power, and phone is locked, so locking immediately.");
                }
                //constantPINServiceIntent.putExtra("sourceNodeId", sourceNodeId);
                try {
                    devicePolicyManager.lockNow();
                } catch (Exception e){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to lock phone right before watch-triggered constant PIN service");
                    }
                }
            } else if (keyguardManager != null && !keyguardManager.isKeyguardLocked()){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Started constant PIN service from quick tile or double tap power, but phone was unlocked. didn't lock immediately");
                }
            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                context.startForegroundService(constantPINServiceIntent);
//            } else {
//                context.startService(constantPINServiceIntent);
//            }
            constantPinModeActivateNow(true);
            if (fromQuickTile) {
                try {
                    Intent closeIntent = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                    context.sendBroadcast(closeIntent);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Failed to close quick settings overlay when starting constant PIN service from quick tile");
                    }
                }
            }

            iqd = null;

            Runnable confirmConstantPINNotif = () -> {
                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                StatusBarNotification[] notifications;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    notifications = mNotificationManager.getActiveNotifications();
                    for (StatusBarNotification notification : notifications) {
                        if (notification.getId() == NOTIF_ID_CONSTANT_PIN_MODE) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Constant PIN notification exists. Send to watch that this happened.");
                            }
                            returnResultToWatch(sourceNodeId);
                            break;
                        }
                    }
                }
            };
            new Thread(confirmConstantPINNotif).start();


            return "4";


        } else{
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "clicked constant PIN quick tile, but has no device admin rights");
            }
            try {
                Toast.makeText(context, "RadiusLocker is not a Device Admin.\nGo into the app and enable Locking to request the permission.", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to make toast about user not having device admin");
                }
            }
            return "2";
        }
    }


    private void returnResultToWatch(final String sourceNodeId) {

        if (BuildConfig.DEBUG) {
            Log.d("WearableListenerSvc", "sendMessage() back to watch");
        }
        try {
            Runnable sendMessage = () -> {
                try {
                    Task<Integer> sendMessageTask =
                            Wearable.getMessageClient(context).sendMessage(sourceNodeId,
                                    "/lock_phone_result", "5".getBytes());
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d("WearableListenerSvc", "sendMessage() back to watch about success status: " + e);
                    }
                }
            };
            new Thread(sendMessage).start();
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d("WearableListenerSvc", "failed to create new thread for sending message back to watch " + e);
            }
        }

        //fromWear = false;
    }


    public void manageConstantPIN15minPauseJob(Context context, boolean reschedule) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "manageConstantPIN15minPauseJob(): cancelling job before rescheduling or after it was fired. user might have hit Pause a second time, only extending if this is true.");
        }

        //cancel the job here
        jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
        cancelClear.setJobAndNotifParams(CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID, 0, 0);
        cancelClear.cancelJobClearNotif();

        if (reschedule) {//reschedule if the user only paused constant PIN mode. This extends the do-not-lock grace period to 15 minutes from now, since they paused it
            try {

                ComponentName componentName = new ComponentName(context, postPauseConstantPinLockJob.class);
                JobInfo jobInfo = new JobInfo.Builder(CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID, componentName)
                        .setMinimumLatency((15 * 60 * 1000))//15 MIN
                        .setOverrideDeadline((16 * 60 * 1000))//16 MIN
                        //.setPeriodic((15 * 60 * 1000))//16 MIN
                        .setPersisted(false)
                        .setBackoffCriteria((15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
                        .build();
                JobScheduler jobScheduler = (JobScheduler)context.getSystemService(JOB_SCHEDULER_SERVICE);
                int resultCode = 0;
                if (jobScheduler != null) {
                    resultCode = jobScheduler.schedule(jobInfo);
                    if (resultCode == JobScheduler.RESULT_SUCCESS) {
                        Log.d(TAG, "Job schedule success for manageConstantPIN15minPauseJob()");


                    } else {
                        Log.d(TAG, "Job schedule fail manageConstantPIN15minPauseJob()");

                    }
                }


//                PendingIntent alarmIntent = PendingIntent.getBroadcast(context, constPINAlarmRequestCode, intent, PendingIntent.FLAG_IMMUTABLE);
//                long delayPeriod = SystemClock.elapsedRealtime() + 900000;
//                if (android.os.Build.VERSION.SDK_INT >= 23) {
//                    if (alarmManager != null) {
//                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                                delayPeriod, alarmIntent);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                            if (!alarmManager.canScheduleExactAlarms()) {
//                                permissionHelper perm = new permissionHelper(context);
//                                perm.surfaceNotificationForMissingPerms();
//                            }
//                        }
//                        if (BuildConfig.DEBUG) {
//                            long alarmExpiryDate = System.currentTimeMillis() + 900000;
//                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
//                            String delayTimeFormatted = sdf.format(new Date(alarmExpiryDate));
//                            Log.d(TAG, "constant PIN 15m job - delayed to: " + delayTimeFormatted);
//                        }
//                    } else {
//                        if (BuildConfig.DEBUG) {
//                            Log.d(TAG, "constant PIN 15m job - failed to create job");
//                        }
//                    }
//                } else {
//                    if (alarmManager != null) {
//                        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                                delayPeriod, alarmIntent);
//
//                    } else {
//                        if (BuildConfig.DEBUG) {
//                            Log.d(TAG, "constant PIN 15m pause job - failed to create");
//                        }
//                    }
//                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "constant PIN 15m pause job - failed to create", e);
                }
            }
        }

    }



    public void setTempAlarmVarToCancelConstantPIN(Context context, rldbInsertQueryDelete iqd){

        long alarmClockTime = 0;
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                AlarmManager.AlarmClockInfo alarmClockInfo = alarmManager.getNextAlarmClock();
                alarmClockTime = alarmClockInfo.getTriggerTime();
                if (BuildConfig.DEBUG) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                    String timeFormatted = sdf.format(new Date(alarmClockTime));
                    Log.d(TAG, "next alarm time: " + alarmClockTime + ", " + timeFormatted);
                }
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "no alarm time set, so it crashed the alarm time check");
            }
        }
        //only save the temp timestamp to cancel the PIN service for CURRENT alarm.
        //this way, after waking up and if the leftover notif is still there, they can hit cancel and it will still come back later that night.
        //user wouldn't expect the Cancel button to cancel their next bedtime.
        if (System.currentTimeMillis() > alarmClockTime-28800000 && System.currentTimeMillis() < alarmClockTime){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user hit cancel within active alarm period, so going to set the temp var now.");
            }
            //CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP is for when they've cancelled bedtime mode inside of their active wakeup alarm, but for the situation where
            //they later hit "snooze" on their active alarm. RL would think it's a new alarm when this happens. This var helps prevent returning of bedtime mode when they snooze their alarm.
            iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP, alarmClockTime);
        } else if (BuildConfig.DEBUG) {
            Log.d(TAG, "user hit cancel, but was not inside of active alarm. so didn't set the temp cancellation var. only stopping the leftover notif");
        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setting manual constant pin and watch disconnected flags to 0, as this has been cancelled like a celeb in the 2020s");
        }
        iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED, 0);
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE, 0);
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, 0);

        //stop service, and also if not charging then stop heartbeat.
        stopConstantPinIfNecessary(context.getApplicationContext(), iqd, false);


    }

    private boolean isCharging(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.getApplicationContext().registerReceiver(null, ifilter);
        // Are we charging / charged?
        if (batteryStatus != null) {
            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
            boolean wirelessCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS;
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean charging = status == BatteryManager.BATTERY_STATUS_CHARGING;
            if (BuildConfig.DEBUG) {
                boolean battery_full = status == BatteryManager.BATTERY_STATUS_FULL;
                Log.d(TAG, "usbCharge: " + usbCharge + " acCharge: " + acCharge + " charging: " + charging + " battery_full: " + battery_full);
            }
            return  charging || usbCharge || acCharge || wirelessCharge;// || ((usbCharge || acCharge) && battery_full);
        }
        return false;
    }


}
