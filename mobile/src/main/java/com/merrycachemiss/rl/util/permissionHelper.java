/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.util;

import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_HIGH;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_MISSING_PERMISSIONS;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.receivers.DeviceAdmin;

public class permissionHelper {
    final Context context;
    private final String TAG = "permissionHelper";
    public static final int BT_CONNECT_REQ_CODE = 69420;
    public static final int EXACT_ALARM_REQ_CODE = 42069;
    public static final int NOTIF_POST_REQ_CODE = 6660;

    public permissionHelper(Context context) {
        this.context = context;
    }

    public boolean isMissingRequiredPerm(String permission, int minApiLevel) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "check for permission: " + permission + ", isMissing:" + (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= minApiLevel));
        }
        return ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= minApiLevel;
    }

    public boolean isMissingRequiredPerm(String permission, int minApiLevel, rldbInsertQueryDelete iqd, int settingToAlter, int settingValueIfMissing) {
        boolean isMissing = isMissingRequiredPerm(permission, minApiLevel);
        if (isMissing) {
            iqd.setSettingsTempLongVal(settingToAlter, settingValueIfMissing);
        }
        return isMissing;
    }

//    public boolean isMissingRequiredPerm(String permission, int minApiLevel, int maxApiLevel) {
//        return ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= minApiLevel && Build.VERSION.SDK_INT <= maxApiLevel;
//    }

    public void toastMissingPermission(String toast) {
        Toast.makeText(context,
                toast,
                Toast.LENGTH_SHORT).show();
    }


    public void promptForPermission(Activity activity, String[] permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, permission, requestCode);

    }


    public void surfaceNotificationForMissingPerms() {
        String permDispTitle = context.getResources().getString(R.string.missing_critical_perms);
        String permNotificDispMessage = context.getResources().getString(R.string.tap_to_fix);
        NotificationHelper notificationHelper = new NotificationHelper(context);
        notificationHelper.createNotifChannelOreo("issues", context.getResources().getString(R.string.issues), IMPORTANCE_HIGH, true);
        NotificationCompat.Builder builder = notificationHelper.getGeneralStickyNotificationBuilder(permDispTitle, permNotificDispMessage,
                R.drawable.ic_baseline_warning_24);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
            notificationManager.notify(NOTIF_ID_MISSING_PERMISSIONS, builder.build());
        }
    }

//    public boolean hasAlarmPermission() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
//            //doesn't matter < S.
//            return true;
//        } else {
//            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//            try {
//                return (alarmManager.canScheduleExactAlarms());
//            } catch (Exception e) {
//                //for whatever reason, alarm manager crashed.
//                //But we don't want the user to be stuck with the sticky notification warning them that they are missing the permission... have to return true I guess.
//                return true;
//            }
//        }
//    }

    private boolean hasAdminAccess() {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName deviceAdminComponentName = new ComponentName(context, DeviceAdmin.class);

        if (devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Has admin access");
            }
            return true;
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "no admin access");
            }
            return false;
        }
    }

    public boolean canPostNotifs() {

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        return notificationManager.areNotificationsEnabled();
    }


    public boolean hasAllBaselinePermissions() {

        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                return !isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S) && hasAdminAccess() && canPostNotifs();
            } else {
                return hasAdminAccess() && canPostNotifs();
            }


        } catch (Exception e) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to check to see if user has bluetooth connect perm");
            }
        }

        return false;
    }


}
