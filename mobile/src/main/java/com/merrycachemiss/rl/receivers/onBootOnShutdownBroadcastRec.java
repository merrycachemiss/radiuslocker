/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.receivers;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.dataStorage.shPrefsGetterSetter;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.jobs.postSnoozeLockCheckJob;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.services.mainSystemEventReceiverService;
import com.merrycachemiss.rl.util.NotificationHelper;
import com.merrycachemiss.rl.util.getSetCurrentWIFISSID;
import com.merrycachemiss.rl.util.permissionHelper;


import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static android.content.Context.JOB_SCHEDULER_SERVICE;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_HIGH;
import static com.merrycachemiss.rl.dataStorage.dataConstants.APP_VERSION;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BEDTIME_PROTECTION_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_REFRESHER_QUEUED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_UPON_LOCKING_WATCH_DISCONNECT_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_BY_CHOICE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DEVICE_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_IF_MAPS_OPEN;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LATEST_CONNECTED_WIFI_SSID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.PURCHASE_VERIF_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REAUTH_TRIGGER_TIME;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_IF_DRIVING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.PHONE_LOCKING_ENABLED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.POST_SNOOZE_LOCK_CHECK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.USER_IS_DRIVING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP;
import static com.merrycachemiss.rl.util.shortcutAdder.getShortcutsAddNewOnes;

import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_NEW_EULA;

public final class onBootOnShutdownBroadcastRec extends BroadcastReceiver {
    private static final String TAG = "bootShutdownRec";
    private shPrefsGetterSetter sPrefs;
    private int isAlreadyMigrated;
    private int acceptedEula;
    @Override
    public void onReceive(Context context, Intent intent) {
        Context c = context.getApplicationContext();
        sPrefs = new shPrefsGetterSetter(c);//inside of sPrefs class, ALL sharedpreferences and DATABASE get migrated.

        acceptedEula = 0;
        try {
            acceptedEula = sPrefs.getSharedPrefsInt("eula_accepted");
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "sharedprefs failed to get eula_accepted status. likely non-N device, or storage not migrated yet.");
            }
        }

        String action = intent.getAction();
        isAlreadyMigrated = 0;
        try{
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "check if sharedprefs storage already migrated before. will also later determine migration of DB");
            }
            isAlreadyMigrated = sPrefs.getSharedPrefsInt("storage_migrated_direct_boot");
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "sharedprefs storage not migrated before");
            }
        }

        if (action != null) {
            if (action.equals(Intent.ACTION_BOOT_COMPLETED) || action.equals(Intent.ACTION_MY_PACKAGE_REPLACED)
                    || action.equals("android.intent.action.QUICKBOOT_POWERON")) {

                /*                                           NORMAL BOOT TASKS                                                     */

                Log.d(TAG, action + " received");
                startupTasks(context, false, action.equals(Intent.ACTION_MY_PACKAGE_REPLACED));

                sPrefs.setSharedPrefsInt("directBootHandled", 0);//reset startuptasks for next boot


            } else if (action.equals(Intent.ACTION_LOCKED_BOOT_COMPLETED)
                    && isAlreadyMigrated == 1 && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

                /*                                           DIRECT BOOT TASKS                                                     */

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ACTION_LOCKED_BOOT_COMPLETED received and isAlreadyMigrated == 1 and on >= N. Beginning startup tasks");
                }
                try {
                    startupTasks(context, true, false);
                    sPrefs.setSharedPrefsInt("direct_boot_handled", 1);
                    //sPrefs.setSharedPrefsInt("startupTasksHandled", 1);//startup tasks handled, record as such so it won't re-do them after first user unlock.
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "ACTION_LOCKED_BOOT_COMPLETED -- STARTUP TASKS FAILED");
                    }
                    sPrefs.setSharedPrefsInt("directBootHandled", 0);
                    //sPrefs.setSharedPrefsInt("startupTasksHandled", 0);//startup tasks handled, record as such so it won't re-do them after first user unlock.
                }

            } else if (action.equals(Intent.ACTION_SHUTDOWN) || action.equals("android.intent.action.QUICKBOOT_POWEROFF")) {//phone shutdown

                Log.d(TAG, action + " received");

                try {
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
                    iqd.updateAllBTConnStatusToDisconn();
                    iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                    iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE, 0);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to write to DB on shutdown", e);
                    }
                }

            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "received null intent/action for boot/package receiver");
            }
        }
    }

    private void startupTasks(final Context context, Boolean directBoot, Boolean appPackageReplaced){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "startupTasks called. Direct boot: " + directBoot);
        }

        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);

        if (acceptedEula == 1) {//only start service if user had accepted EULA/PP
            Intent connectionReceiverServiceStartIntent = new Intent(context, mainSystemEventReceiverService.class);
            Log.d(TAG, "startupTasks: start connectionReceiverService");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(connectionReceiverServiceStartIntent);
            } else {
                context.startService(connectionReceiverServiceStartIntent);
            }
        }

        if (!directBoot) {
            //phone boot has completed (post-directBoot is after user unlock)
            if (!appPackageReplaced) {
                try {
                    migrateDBtoDPTifNeeded(context);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "startupTasks: failed migrateDBtoDPTifNeeded.");
                    }
                }
                warnIfOnOS12AndMissingPermsLockIfNeeded(false, context, iqd);
            } else {
                try {
                    PackageInfo appPackageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    long version;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        version = appPackageInfo.getLongVersionCode();
                    } else {
                        version = appPackageInfo.versionCode;
                    }
                    if (version != 0 && iqd.getSettingsTempLongVal(APP_VERSION) == 0){
                        //if "version" is obtainable, ensure that it was never set. if it was never set, then
                        //the app never performed the below migration step, as this variable was introduced at the same time
                        iqd.updateAllBTConnStatusTimesOnceForFirstTransitionAwayFromAlarmmanager();
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "appPackageReplaced with version " + version + ", last version installed was " + iqd.getSettingsTempLongVal(APP_VERSION));
                    }
                    if (version != 0) {
                        iqd.setSettingsTempLongVal(APP_VERSION, version);
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "appPackageReplaced: failed to check build version of your app, last version installed was " + iqd.getSettingsTempLongVal(APP_VERSION));
                    }
                }
            }

            if (!appPackageReplaced && iqd.genericSingleItemExistsQuery(rldbContract.HasAnyMonitoredBTQuery.CONTENT_URI, null, null)) {
                iqd.setSettingsTempLongVal(PHONE_LOCKING_ENABLED, 1);
            }

            iqd.setSettingsTempLongVal(DONT_LOCK_IF_DRIVING, 0);

            iqd.setSettingsTempLongVal(USER_IS_DRIVING, 0);

            iqd.setSettingsTempStringVal(DEVICE_ID, "");

            if (appPackageReplaced) {
                //what a weird name this was. if is set to "2", the app knows that an upgrade happened and they accepted the new EULA
                sPrefs.setSharedPrefsInt("not_new_install", 2);
                if (sPrefs.getSharedPrefsInt("eula_accepted") != 1) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "new eula not yet accepted after app upgrade - surfacing the notification for it");
                    }
                    try {
                        String newEULADispTitle = context.getResources().getString(R.string.new_tos_title);
                        String newEULANoficDispMessage = context.getResources().getString(R.string.new_tos_message);
                        NotificationHelper notificationHelper = new NotificationHelper(context);
                        notificationHelper.createNotifChannelOreo("issues", context.getResources().getString(R.string.issues), IMPORTANCE_HIGH, true);
                        NotificationCompat.Builder builder = notificationHelper.getGeneralStickyNotificationBuilder(newEULADispTitle, newEULANoficDispMessage,
                                R.drawable.ic_baseline_message_24);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                        notificationManager.notify(NOTIF_ID_NEW_EULA, builder.build());
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to create notif regarding new eula", e);
                        }
                    }
                }

                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);

                if (iqd.getSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK) == 1 && iqd.getSettingsTempLongVal(RELOCK_JOB_SCHEDULED) == 1) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "try to set up lock retry job again from boot receiver because package was replaced");
                    }
                    jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                    cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                    cancelClear.cancelJobClearNotif();
                    try {
                        ComponentName componentName = new ComponentName(context, postSnoozeLockCheckJob.class);
                        JobInfo jobInfo = new JobInfo.Builder(RELOCK_JOB_ID, componentName)
                                .setMinimumLatency((15 * 60 * 1000))//15 MIN
                                .setOverrideDeadline((16 * 60 * 1000))//16 MIN
                                //.setPeriodic((15 * 60 * 1000))//16 MIN
                                .setPersisted(false)
                                .setBackoffCriteria((15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
                                .build();
                        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
                        int resultCode = 0;
                        if (jobScheduler != null) {
                            resultCode = jobScheduler.schedule(jobInfo);
                            if (resultCode == JobScheduler.RESULT_SUCCESS) {

                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "lock retry job schedule success - from boot receiver because package was replaced");
                                }
                                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 1);

                                try {
                                    String lockScheduleNoficDispTitle = context.getResources().getString(R.string.schedule_lock_title);
                                    String lockScheduleNoficDispMessage = context.getResources().getString(R.string.scheduled_lock_descr);
                                    NotificationHelper notificationHelper = new NotificationHelper(context);
                                    notificationHelper.createNotifChannelOreo("scheduled_events", context.getResources().getString(R.string.scheduled_events),
                                            3, false);
                                    NotificationCompat.Builder builder = notificationHelper.getScheduledItemNotificationBuilder(lockScheduleNoficDispTitle, lockScheduleNoficDispMessage,
                                            R.drawable.ic_access_time_black_24dp, NOTIF_ID_CANCEL_SCHEDULED_LOCK, true);
                                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                                    notificationManager.notify(NOTIF_ID_CANCEL_SCHEDULED_LOCK, builder.build());
                                } catch (Exception e) {
                                    if (BuildConfig.DEBUG) {
                                        Log.e(TAG, "failed to create cancellable notif for lock retry job after an app update", e);
                                    }
                                }


                            } else {
                                Log.d(TAG, "Job schedule fail - from boot receiver because package was replaced");
                                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                            }
                        } else {
                            Log.d(TAG, "Job schedule fail - from boot receiver because package was replaced. Jobscheduler was null.");
                            iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                        }
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to reschedule lock retry job from boot receiver after an app update", e);
                        }
                        iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                        
                    }
                } else {

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "lock retry job was not detected as scheduled from boot receiver after package was replaced");
                    }
                }

                //after app update, ensure that old jobs are cancelled and that app shortcuts are added
                try {
                    jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                    cancelClear.setJobAndNotifParams(420, 0, 0);//CANCEL OLD PURCHASE VERIF JOB
                    cancelClear.cancelJobClearNotif();
                    cancelClear.setJobAndNotifParams(PURCHASE_VERIF_JOB_ID, 0, 0);//CANCEL REPLACEMENT PUTCHASE VERIF JOB
                    cancelClear.cancelJobClearNotif();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "purchase jobs cancelled");
                    }
                    cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID, 0, 0);
                    cancelClear.cancelJobClearNotif();

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "charging trigger for constant PIN job cancelled - it will do the work when plugged in, and also start the heartbeat");
                    }
                    
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to cancel jobs after app update", e);
                    }
                    
                }


                if (Build.VERSION.SDK_INT >= 25) {
                    try {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "try to add dynamic shortcuts");
                        }
                            getShortcutsAddNewOnes(context, TAG);

                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to add app shortcuts for users >= api 25 after an app update");
                        }
                        
                    }
                }
                warnIfOnOS12AndMissingPermsLockIfNeeded(true, context, iqd);
                //removed maps stuff since it barely worked
                iqd.setSettingsTempLongVal(DONT_LOCK_IF_MAPS_OPEN, 0);


                //reset biopin authentication timeout on app upgrade, so it asks them for it again
                iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, 0);


            }

            if (BuildConfig.DEBUG){
                Log.d(TAG, "package is replaced or user just rebooted, starting bedtime protection mode service/heartbeat. will kill itself off if not needed.");
            }
            if (iqd.getSettingsTempLongVal(BEDTIME_PROTECTION_SETTING) == 1 || iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_UPON_LOCKING_WATCH_DISCONNECT_SETTING) == 1
                    || iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE) == 1) {
                constantPinHelper constHelp = new constantPinHelper(context);
                constHelp.activateConstantPINServiceIfNeeded(context, iqd);
            } else{
                if (BuildConfig.DEBUG){
                    Log.d(TAG, "bedtime protection mode is not enabled. not going to fire the service. instead, we will kill the associated jobs.");
                }
                jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID, 0, 0);
                cancelClear.cancelJobClearNotif();

                cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID, 0, 0);
                cancelClear.cancelJobClearNotif();

                cancelClear.setJobAndNotifParams(CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID, 0, 0);
                cancelClear.cancelJobClearNotif();
            }

            //ensure that user enabled extra launcher icons are in use, otherwise remove (necessary?)
            if (iqd.getSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP) != 1) {
                context.getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.constantPINServiceAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                context.getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.lockNowAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            }

            //start constant PIN if user has since left wifi during app upgrade or boot completion post-unlock
            getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(context, TAG);
            String ssid = wifiSSIDGetSet.getCurrentWIFISSID();
            if (iqd.getSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE) == 1) {
                //disconnected from wifi, check if the last stored connection was in wifi snooze list
                if ((ssid.isEmpty() || ssid.trim().isEmpty() || !wifiSSIDGetSet.currentlyOnWIFISnoozeSSID(iqd, ssid)) && wifiSSIDGetSet.justLeftAnyWIFISnoozeSSID(iqd)) {

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "starting constant PIN mode because left wifi snooze network SSID: " + iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID));
                    }
                    constantPinHelper constHelp = new constantPinHelper(context);
                    try {
                        constHelp.startConstantPINByChoice(context, false, null, false);
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to launch service for constant PIN from app shortcut", e);
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "left wifi network, but it wasn't used for snoozing. SSID: " + iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID));
                    }
                }
            }
            //set current ssid info, but only after comparison of before vs after network change.
            wifiSSIDGetSet.setCurrentWIFISSID(iqd, ssid);


        } else {
            //if in direct boot before unlock, reset some vars
            iqd.setSettingsTempLongVal(USER_IS_DRIVING, 0);
            iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE, 0);
            iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, 0);
            //get current wifi network for constant PIN launch option upon leaving chosen wifi snooze network
            getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(context, TAG);
            wifiSSIDGetSet.setCurrentWIFISSID(new rldbInsertQueryDelete(context), wifiSSIDGetSet.getCurrentWIFISSID());
        }

    }


    private void warnIfOnOS12AndMissingPermsLockIfNeeded(boolean lock, Context context, rldbInsertQueryDelete iqd) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            permissionHelper perm = new permissionHelper(context);
            //user is missing bluetooth and/or alarm permissions after app upgrade on OS 12+.
            //BT: Since we are targeting 12, we are now missing information on their BT devices until they grant the permission.
            //We will force a single lock if the screen is off, so we won't miss a lock upon disconnect,
            //if there are problems with that scenario.
            //When they unlock, they'll see that the permission is missing via the warning notification, and hopefully Google didn't
            //make too much work for us and them and they'll actually grant it.
            //Permissions are becoming too much work for both the user and devs.
            //AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            try {
                if (perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S)) {
                    perm.surfaceNotificationForMissingPerms();
                    if (perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S)) {
                        iqd.updateAllBTConnStatusToDisconn();
                        //If they use a BT device for snoozing (including multi-watch lock prevention), we will make it so the user will
                        //refresh BT when they go into the main activity, since we temporarily lost the state of their devices
                        //while the permission was missing:
                        if (iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE) == 1 || iqd.genericSingleItemExistsQuery(rldbContract.ConnectedSafeBTQuery.CONTENT_URI, null, null)) {
                            iqd.setSettingsTempLongVal(BT_REFRESHER_QUEUED, 1);
                        }
                        //if the screen is off, lock the device.
                        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                        if (keyguardManager != null && keyguardManager.isKeyguardLocked() && lock) {
                            immediateLocker locker = new immediateLocker(context.getApplicationContext(), false, false, TAG);
                            locker.lockNOW();
                        }
                    }
                }
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "warnIfOnOS12AndMissingPermsLockIfNeeded crashed. Likely related to the damn alarm manager.", e);
                }
            }
        }
    }

    private void migrateDBtoDPTifNeeded(Context context){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N && isAlreadyMigrated == 0) {
            final Context deviceContext = context.createDeviceProtectedStorageContext();
            deviceContext.moveDatabaseFrom(context, "rldb.db");//see that var matches: rldbSchema.DB_NAME
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "rldb.db now migrated to device protected storage");
            }
        } else if (BuildConfig.DEBUG) {
            Log.d(TAG, "rldb.db was already previously migrated to device protected storage. didn't have to do so on startup");

        }

    }

}
