/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.receivers;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;

import com.merrycachemiss.rl.services.mainSystemEventReceiverService;
import com.merrycachemiss.rl.util.connDisconnTasks;

import static com.merrycachemiss.rl.services.mainSystemEventReceiverService.isInstanceCreated;

public final class btConnReceiver extends BroadcastReceiver {

    //this exists purely as a backup in case the main service is killed/restarting/etc.
    //checks if service is running, and if not, tries to perform lock tasks.
    //since it's not a foreground service, it might lose out on the location check (service is tagged with this in the manifest)
    //it will also attempt to start the service, though on modern Android this will fail. It might work if the user applied the right battery settings,
    //according to their OEM's garbage version of the OS
    private static final String TAG = "btConnReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (!isInstanceCreated()) {

            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);


            if (BuildConfig.DEBUG) {
                Log.d(TAG, "BT device connection status change");
            }
            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action) && device != null) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ACTION_ACL_DISCONNECTED. Performing tasks in the receiver, since service is not running.");
                }
                connDisconnTasks discconn = new connDisconnTasks(context, TAG);
                discconn.disconnTasks(device);
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action) && device != null) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ACTION_ACL_CONNECTED. Performing tasks in the receiver, since service is not running.");
                }
                connDisconnTasks conn = new connDisconnTasks(context, TAG);
                conn.connTasks(device.getAddress());
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Main service wasn't running. Trying to recover it from the background, though this will probably fail on modern Android.");
            }
            try {
                Intent connectionReceiverServiceIntent = new Intent(context, mainSystemEventReceiverService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(connectionReceiverServiceIntent);
                } else {
                    context.startService(connectionReceiverServiceIntent);
                }
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to recover the service from the background");
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "main service was already running, no need to attempt the backup of locking tasks from the bt conn receiver");
            }
        }
    }

}
