/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static android.Manifest.permission.POST_NOTIFICATIONS;

import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_HIGH;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.merrycachemiss.rl.adapters.btDeviceLVAdapter;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.receivers.DeviceAdmin;
import com.merrycachemiss.rl.dataStorage.shPrefsGetterSetter;
import com.merrycachemiss.rl.services.mainSystemEventReceiverService;
import com.merrycachemiss.rl.util.NotificationHelper;
import com.merrycachemiss.rl.util.bioPinAuthentication;
import com.merrycachemiss.rl.util.permissionHelper;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

import static com.merrycachemiss.rl.dataStorage.dataConstants.BEDTIME_PROTECTION_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_HAS_BEEN_REFRESHED_BEFORE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DELAY_BEFORE_LOCK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_IF_CHARGING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_TWICE_WITHIN;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_CABLE_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REAUTH_TRIGGER_TIME;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_SIM_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.PHONE_LOCKING_ENABLED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.POST_SNOOZE_LOCK_CHECK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REQUIRE_BIOMETRIC_AUTH_USE_APP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.ONLY_USE_PIN_PASS_FOR_RL_AUTH;
import static com.merrycachemiss.rl.dataStorage.dataConstants.START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_DELAY;
import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_WEAR_APIS;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SerialProjection.SERIAL_PROJECTION;

import static com.merrycachemiss.rl.dataStorage.rldbContract.MonitoredBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SafeBTQuery;
import static com.merrycachemiss.rl.services.mainSystemEventReceiverService.isInstanceCreated;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_MISSING_PERMISSIONS;
import static com.merrycachemiss.rl.util.NotificationHelper.clearNotification;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY;
import static com.merrycachemiss.rl.util.permissionHelper.BT_CONNECT_REQ_CODE;
import static com.merrycachemiss.rl.util.permissionHelper.NOTIF_POST_REQ_CODE;


public final class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothDevice> pairedDeviceSerialsArrayList;
    private ListView btDeviceListview;
    private TextView settingsTextLink;
    private SwitchCompat phoneLockToggleSwitch;
    private TextView aboutTextLink;
    private TextView nobtDevicesFoundText;
    private TextView adminStateText;
    public static boolean[] btDeviceListCheckedState;
    public static boolean[] connectedBTDeviceListCheckedState;
    private boolean btIsRefreshing = false;
    private TextView securityStateText;
    private static final int ADMIN_ACTIVATION_REQUEST = 47; //Request ID for Device Administrator
    private DevicePolicyManager devicePolicyManager;
    private ComponentName deviceAdminComponentName;
    private TextView missingPermissionsText;
    private Button btPermissionButton;
    private TextView btPermissionRefresherMessageTextView;
    private Button notifPermissionButton;
    private TextView notifPermissionMessageTextView;
    private rldbInsertQueryDelete iqd;
    private boolean onCreateRefreshedUIAlready = true;
    private bioPinAuthentication auth;
    private permissionHelper perm;
    private boolean missingPermissions = false;
    private boolean isSettingUpPermission = false;
    private AlarmManager alarmManager;

    static int dontLockTwiceSavedVal;
    static int delayTaskSpinnerDelayMultiplierSavedVal;
    static int dontLockIfCharging;
    static int useWearApis;
    static int postSnoozeLockCheck;
    static int lockIfSIMRemoved;
    static int lockIfCableRemoved;
    static int startConstantPinIfLeaveSnoozeWifi;
    public static int requireBiometricAuth;
    static int requirePinPassOnlyForAuth;
    public static long reauthTriggerTime = 0;
    static int delayLockSavedVal;
    static int bedtimeMode;
    static int multiWatchMode;
    static int buttonRemap;
    public static boolean shortcutsMade = false;
    static int retrievedSettingsVars;
    static int acceptedEula;
    public static boolean isAuthenticated = false;
    private long adminTimeout = 0;
    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TAG = "MainActivity";

        Toolbar myToolbar = findViewById(R.id.mainactivityToolbar);
        setSupportActionBar(myToolbar);
        iqd = new rldbInsertQueryDelete(this);


        shPrefsGetterSetter sPrefs = new shPrefsGetterSetter(this);
        acceptedEula = sPrefs.getSharedPrefsInt("eula_accepted");
        securityStateText = findViewById(R.id.lockScreenStateText);
        btDeviceListview = findViewById(R.id.btDeviceListview);
        nobtDevicesFoundText = findViewById(R.id.nobtDevicesFoundText);
        adminStateText = findViewById(R.id.adminStateText);
        phoneLockToggleSwitch = findViewById(R.id.lockEnableSwitch);
        aboutTextLink = findViewById(R.id.aboutTextLink);
        settingsTextLink = findViewById(R.id.settingsTextLink);
        missingPermissionsText = findViewById(R.id.missingPermissionsText);
        btPermissionButton = findViewById(R.id.btPermissionButton);
        btPermissionRefresherMessageTextView = findViewById(R.id.btPermissionRefresherMessageTextView);
        notifPermissionButton = findViewById(R.id.notifPermissionButton);
        notifPermissionMessageTextView = findViewById(R.id.notifPermissionMessageTextView);
        btPermissionButton.setOnClickListener(this);
        aboutTextLink.setOnClickListener(this);
        settingsTextLink.setOnClickListener(this);
        notifPermissionButton.setOnClickListener(this);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH) && !BuildConfig.DEBUG) {
            Toast.makeText(this,
                    "Bluetooth doesn't seem to be supported on your device",
                    Toast.LENGTH_LONG).show();
            //if (!BuildConfig.DEBUG) {
            finish();
            //}
        }
        perm = new permissionHelper(this);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null && !BuildConfig.DEBUG) {
            Toast.makeText(this,
                    "Bluetooth doesn't seem to be supported on your device",
                    Toast.LENGTH_LONG).show();
            //if (!BuildConfig.DEBUG) {
            finish();
            //}
        }
        IntentFilter btConnReceiverIntent = new IntentFilter();
        btConnReceiverIntent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        btConnReceiverIntent.addAction("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ContextCompat.registerReceiver(this, btConnReceiver, btConnReceiverIntent, ContextCompat.RECEIVER_EXPORTED);
        } else {
            registerReceiver(btConnReceiver, btConnReceiverIntent);
        }
        requireBiometricAuth = (int) iqd.getSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP);

        if (acceptedEula == 1) {
            if (!isInstanceCreated()) {
                //only start the service if this is not a new install and service is not started for some reason.
                //if new install, then the Accept button on the EULA page will start the service
                Log.d(TAG, "accepted EULA, so starting service");
                startBtMonService(this);

            } else {
                Log.d(TAG, "service already started, avoided starting it again");
            }

            //only generate BT list if not new install - don't want to waste time generating list, then going to EULA activity. just go to EULA.
            refreshUIbgThread();
            onCreateRefreshedUIAlready = true;
        } else if (acceptedEula == 0 && sPrefs.getSharedPrefsInt("not_new_install") == 2) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user accepted old EULA, but not new one. Should still start the service, so data and monitoring isn't compromised");
            }
            startBtMonService(this);
        }

        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(this, DeviceAdmin.class);

        getSprefsSettingsVars();



        btDeviceListview.setOnItemClickListener((parent, view, position, id) -> {

            if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S) && isAuthenticated) {
                //get actual bt device info from second array (the indexes match) - just shows in toast for now
                //later you'll want to store the info in sql lite db or something when they check the item
                BluetoothDevice device = pairedDeviceSerialsArrayList.get(position);
                final String deviceAddress = device.getAddress();


                @SuppressLint("MissingPermission") final String deviceName = device.getName();

                //toggle checkbox indirectly for row chosen
                CheckBox btDeviceNameCheckBox = view.findViewById(R.id.btDeviceNameCheckBox);
                btDeviceNameCheckBox.toggle();
                //store checkbox states for scrolling
                if (btDeviceNameCheckBox.isChecked()) {
                    btDeviceListCheckedState[position] = true;
                    //store into DB


                    Runnable insertThread = () -> {
                        iqd.monitoredSafeBTInsert(MonitoredBTQuery.CONTENT_URI, deviceAddress, deviceName);
                        Log.d(TAG, "inserted " + deviceAddress + " " + deviceName);
                    };
                    new Thread(insertThread).start();

                } else {
                    btDeviceListCheckedState[position] = false;

                    Runnable deleteThread = () -> {
                        iqd.monitoredSafeBTDelete(MonitoredBTQuery.CONTENT_URI, deviceAddress);
                        Log.d(TAG, "deleted " + deviceAddress + " " + deviceName);
                    };
                    new Thread(deleteThread).start();

                }
            }
        });

        phoneLockToggleSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isChecked && iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED) == 1) {
                iqd.setSettingsTempLongVal(PHONE_LOCKING_ENABLED, 0);
                if (BuildConfig.DEBUG) {
                    long val = iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED);
                    Log.d(TAG, "phone lock setting: " + val);
                }
            } else if (isChecked && iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED) == 0) {
                adminRequestIfNeededAndOrSaveLockingVal();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            //only care about alarmmanager if OS >= 12
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        }

        onCreateRefreshedUIAlready = true;
        auth = new bioPinAuthentication(this, TAG);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.settingsTextLink) {
            launchOtherActivity(SettingsActivity.class);
        } else if (v.getId() == R.id.aboutTextLink) {
            launchOtherActivity(aboutAppActivity.class);
        } else if (v.getId() == R.id.btPermissionButton && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            isSettingUpPermission = true;
            String[] perms = new String[1];
            perms[0] = BLUETOOTH_CONNECT;
            perm.promptForPermission(this, perms, BT_CONNECT_REQ_CODE);
        } else if (v.getId() == R.id.notifPermissionButton && Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            isSettingUpPermission = true;
            String[] perms = new String[1];
            perms[0] = POST_NOTIFICATIONS;
            perm.promptForPermission(this, perms, NOTIF_POST_REQ_CODE);
        }
    }

    private void startBtMonService(Context context){
        Intent btMonStartServiceIntent = new Intent(context, mainSystemEventReceiverService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(btMonStartServiceIntent);
        } else {
            startService(btMonStartServiceIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "ONPAUSE");
        }
        if (missingPermissions && !isSettingUpPermission && acceptedEula == 1) {
            //ensure user is notified that permissions are still not set up, when they leave the activity.
            //But only if they're not already setting one up...
            perm.surfaceNotificationForMissingPerms();
        } else {
            isSettingUpPermission = false;
        }
        if (adminTimeout == 0) {
            //if adminTimeout was > 0, then we'd just asked the user for device admin perm within the last 30 sec. don't want to cut the auth timeout prematurely
            auth.adjustTimeoutForPresence(true);
        }
    }

    private void authBiometrics() {
        if (requireBiometricAuth == 1 && SystemClock.elapsedRealtime() > iqd.getSettingsTempLongVal(REAUTH_TRIGGER_TIME)) {
            auth = new bioPinAuthentication(this, TAG);

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user must authenticate if < 5 minutes of reauth timeout");
            }
            auth.authenticate(true, false, 0);

        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user doesn't use biometrics or is within 5 minute auth grace period");
            }
            alterUIAuthenticationForState(true);
            isAuthenticated = true;
        }

    }

    private final BroadcastReceiver authReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null){
                //user successfully authenticated, so enable UI via receiver sent from biometric auth.
                //...
                //...I know. But it's super easy to do it this way, and I'm not going to start using Kotlin just for coroutines at this moment.

                int isForUI = intent.getIntExtra("authFromNotificationType", 0);
                if (isForUI == 0) {//don't act on potential crosstalk from notification-related auth actions
                    isAuthenticated = intent.getBooleanExtra("success", false);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "authReceiver's isAuthenticated: " + isAuthenticated);
                    }
                    alterUIAuthenticationForState(isAuthenticated);
                }

            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "intent was null. Probably cancelled request. Didn't action.");
                }
                alterUIAuthenticationForState(false);
            }
        }
    };

    private void alterUIAuthenticationForState(boolean uiEnabled) {
        //a system-level bug that affected biometricprompt was discovered during the hellscape that was Android 12's first 6 months of release, prior to 12.1:
        //if the system had the problem of RAM filling up and/or being barely responsive (with an upcoming System UI crash that would temporarily fix
        //things for a few hours), this app would launch and the biometrics would take a long time to surface. During this window of time, the user
        //could interact with the app and make changes before authentication window popped up.
        //So now the UI is disabled until authentication succeeds.
        btDeviceListview.setEnabled(uiEnabled);
        settingsTextLink.setEnabled(uiEnabled);
        aboutTextLink.setEnabled(uiEnabled);
        phoneLockToggleSwitch.setEnabled(uiEnabled);
    }

    private void getSprefsSettingsVars() {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "preload extra settings vars from db on BG thread");
        }
        Runnable runnable = () -> {
            dontLockTwiceSavedVal = (int) iqd.getSettingsTempLongVal(DONT_LOCK_TWICE_WITHIN);
            delayTaskSpinnerDelayMultiplierSavedVal = (int) iqd.getSettingsTempLongVal(TASKER_JOB_DELAY);
            dontLockIfCharging = (int) iqd.getSettingsTempLongVal(DONT_LOCK_IF_CHARGING);
            postSnoozeLockCheck = (int) iqd.getSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK);
            lockIfSIMRemoved = (int) iqd.getSettingsTempLongVal(LOCK_IF_SIM_REMOVED);
            lockIfCableRemoved = (int) iqd.getSettingsTempLongVal(LOCK_IF_CABLE_REMOVED);
            startConstantPinIfLeaveSnoozeWifi = (int) iqd.getSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE);
            delayLockSavedVal = (int) iqd.getSettingsTempLongVal(DELAY_BEFORE_LOCK);
            bedtimeMode = (int) iqd.getSettingsTempLongVal(BEDTIME_PROTECTION_SETTING);
            multiWatchMode = (int) iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE);
            buttonRemap = (int) iqd.getSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP);
            requirePinPassOnlyForAuth = (int) iqd.getSettingsTempLongVal(ONLY_USE_PIN_PASS_FOR_RL_AUTH);
            useWearApis = (int) iqd.getSettingsTempLongVal(USE_WEAR_APIS);
            retrievedSettingsVars = 1;
        };
        new Thread(runnable).start();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "enabling settings textview link");
        }
        settingsTextLink.setEnabled(true);
    }

    private void launchOtherActivity(Class<?> chosenClass) {
        Intent intent = new Intent(this, chosenClass);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(authReceiver, new IntentFilter("com.merrycachemiss.rl.authActivityResult"));
        if (iqd.getSettingsTempLongVal(REAUTH_TRIGGER_TIME) != 0) {
            //this was set specifically to 0 when user accepted new eula when coming from old eula.
            //so don't bump it, you must ensure that user is asked for PIN when returning from new EULA acceptance
            auth.adjustTimeoutForPresence(false);
        }
        setUpLockingEnabledSwitch();
        checkForAdminAccess();
        setUIForMissingAPI12AbovePermissionButtons("");
        if (!onCreateRefreshedUIAlready && acceptedEula == 1) {//so the first oncreate will refresh it, and then it will be refreshed in any onResume
            refreshUIbgThread();
        } else {
            onCreateRefreshedUIAlready = false;
        }


        if (acceptedEula == 0) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "acceptedEula is 0, going to EULA + PP activity");
            }
            //remove extra activity launchers if just first installed or they accepted new EULA,
            //in case launcher decided to add them and user never set RL up to use them.
            if (iqd.getSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP) != 1) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "remove extra activity launchers if just first installed or they accepted new EULA, in case launcher decided to add them and user never set RL up to use them.");
                }
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.constantPINServiceAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.lockNowAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            }

            launchOtherActivity(eulaPpActivity.class);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //now creates notification channels immediately, if accepted eula
                try {
                    NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
                    notificationHelper.createNotifChannelOreo("constant_pin_mode", getApplicationContext().getResources().getString(R.string.constant_pin_notif_channel),
                    3, false);
                    notificationHelper.createNotifChannelOreo("background_services", getApplicationContext().getResources().getString(R.string.services_and_tasks), 3, false);
                    notificationHelper.createNotifChannelOreo("scheduled_events", getApplicationContext().getResources().getString(R.string.scheduled_events), 3, false);
                    notificationHelper.createNotifChannelOreo("scheduled_events", getApplicationContext().getResources().getString(R.string.scheduled_events), 3, false);
                    notificationHelper.createNotifChannelOreo("issues", getApplicationContext().getResources().getString(R.string.issues), IMPORTANCE_HIGH, false);
                } catch (Exception e){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to create notification channels immediately");
                    }

                }
            }
            try {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "try activate biometric prompt if applicable");
                }

                authBiometrics();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "biometric auth failure", e);
                }
            }


        }
        isSettingUpPermission = false;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == BT_CONNECT_REQ_CODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generateBtDeviceList();
                setUIForMissingAPI12AbovePermissionButtons(BLUETOOTH_CONNECT);
            } else {
                setUIForMissingAPI12AbovePermissionButtons("");
            }
        }
//        } else if (requestCode == NOTIF_POST_REQ_CODE){
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//            }
//        }
    }


    private void refreshUIbgThread() {
        // refresh UI on background thread
        new Handler().post(() -> {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Refreshing some UI on bg thread. onCreateRefreshedUIAlready: " + onCreateRefreshedUIAlready);
            }
            btDeviceListview.setVisibility(View.GONE);
            nobtDevicesFoundText.setVisibility(View.VISIBLE);


            if (BuildConfig.DEBUG) {
                Log.d(TAG, "try to generate BT device list if BT is on");
            }
            if (bluetoothAdapter != null) {
                if (bluetoothAdapter.isEnabled()) {
                    generateBtDeviceList();
                }
            }
            nobtDevicesFoundText.setText(getApplicationContext().getResources().getString(R.string.no_bluetooth_devices_found_or_bluetooth_is_off));



            if (BuildConfig.DEBUG) {
                Log.d(TAG, "try to get state of using PIN/Pattern/Pass");
            }
            try {
                securityStateCheck();
            } catch (Exception e) {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Can't check lock screen state. Ensure you are using a PIN, Pattern or Password");
                }
                Toast.makeText(getApplicationContext(),
                        "Can't check lock screen state. Ensure you are using a PIN, Pattern or Password,\nif not already",
                        Toast.LENGTH_SHORT).show();
            }
        });


    }

    @SuppressLint("MissingPermission")
    private void generateBtDeviceList() {
        try {
            if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S)) {
                Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

                if (!pairedDevices.isEmpty()) {
                    //FIRST ARRAY FOR DISPLAYING THE HUMAN-READABLE BT DEVICE INFO IN LISTVIEW
                    ArrayList<String> pairedDeviceArrayList = new ArrayList<>();
                    //SECOND ARRAY FOR DEALING WITH THE BACKEND BT INFO (INVISIBLE TO USER)
                    pairedDeviceSerialsArrayList = new ArrayList<>();


                    int pos = 0;
                    //will need to figure out the size of the checked items list, and the size of the paired devices list
                    //create/store checkmark states based on DB result. listview adapter sets checks.
                    btDeviceListCheckedState = new boolean[pairedDevices.size()];//+ (int)dbHandler.countMonitoredBTdevicesStored()];
                    connectedBTDeviceListCheckedState = new boolean[pairedDevices.size()];

                    for (BluetoothDevice device : pairedDevices) {
                        final String[] selectionArgs = {device.getAddress()};
                        boolean isChosenSafeBTDevice = iqd.genericSingleItemExistsQuery(SafeBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);
                        if (!isChosenSafeBTDevice) {
                            //the device name gets passed to listview adapter:
                            pairedDeviceArrayList.add(device.getName());// + " - " + device.getAddress());
                            //store device serials only, for storing to DB
                            pairedDeviceSerialsArrayList.add(device);

                            boolean isChosenMonitoredBTDevice = iqd.genericSingleItemExistsQuery(MonitoredBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);

                            //creates/stores checkmark states based on DB result. listview adapter sets checks.
                            btDeviceListCheckedState[pos] = isChosenMonitoredBTDevice;
                            //as above, but used to determine whether or not should be highlighted green if connected
                            final String[] connectedSelectionArgs = {device.getAddress(), "1"};
                            boolean isConnectedBTDevice = iqd.isConnectedBTDeviceQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);
                            connectedBTDeviceListCheckedState[pos] = isConnectedBTDevice;
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "BT device connected state, according to db " + device.getName() + " " + device.getAddress() + " status: " + isConnectedBTDevice);
                            }
                            pos++;
                        }
                    }

                    if (!pairedDeviceSerialsArrayList.isEmpty()) {
                        nobtDevicesFoundText.setVisibility(View.GONE);
                        btDeviceListview.setVisibility(View.VISIBLE);
                        ListAdapter pairedDeviceAdapter = new btDeviceLVAdapter(this, pairedDeviceArrayList);
                        btDeviceListview.setAdapter(pairedDeviceAdapter);
                    } else {

                        nobtDevicesFoundText.setVisibility(View.VISIBLE);
                        btDeviceListview.setVisibility(View.GONE);
                    }
                } else {
                    btDeviceListview.setVisibility(View.GONE);
                    nobtDevicesFoundText.setVisibility(View.VISIBLE);
                    iqd.setSettingsTempLongVal(BT_HAS_BEEN_REFRESHED_BEFORE, 1);
                }
                btDeviceListview.setEnabled(true);

            } else {
                btDeviceListview.setVisibility(View.GONE);
                nobtDevicesFoundText.setVisibility(View.VISIBLE);
            }
        } catch (Exception e){
            Toast.makeText(MainActivity.this,
                    this.getApplicationContext().getResources().getString(R.string.bt_devicelist_fetch_fail),
                    Toast.LENGTH_LONG).show();
        }
    }

    private final BroadcastReceiver btConnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // BT radio state detection
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {

                if (bluetoothAdapter == null || bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                    btDeviceListview.setVisibility(View.GONE);
                    nobtDevicesFoundText.setVisibility(View.VISIBLE);
                    if (!btIsRefreshing) {
                        iqd.setSettingsTempLongVal(BT_HAS_BEEN_REFRESHED_BEFORE, 1);
                    }
                    btIsRefreshing = false;
                } else if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {

                    generateBtDeviceList();

                }
            } else if ("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED".equals(action)){
                boolean btEnabled = false;
                if (bluetoothAdapter != null){
                    btEnabled = bluetoothAdapter.isEnabled();
                }
                if (btEnabled) {
                    Log.d(TAG, "btConnDisconnRec: BT device connected/disconnected");
                    btDeviceListview.setEnabled(false);
                    generateBtDeviceList();
                }
            }
        }
    };


    private void setUpLockingEnabledSwitch() {

        if (iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED) == 1) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "phone_locking_enabled from db returns 1");
            }
            phoneLockToggleSwitch.setChecked(true);
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "phone_locking_enabled from db returns 0");
            }
            phoneLockToggleSwitch.setChecked(false);


        }
    }

    //TODO: look into this
    //https://stackoverflow.com/questions/16692536/good-solution-to-retain-listview-items-when-user-rotate-phone-and-keep-all-data
    /*
    @Override
    public void onSaveInstanceState(Bundle savedState) {

        super.onSaveInstanceState(savedState);

        // Note: getValues() is a method in your ArrayAdapter subclass
        String[] values = mAdapter.getValues();
        savedState.putStringArray("myKey", values);

    }*/

    private void securityStateCheck() {
        KeyguardManager keyGuard = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (keyGuard != null && !keyGuard.isKeyguardSecure()) {
            securityStateText.setVisibility(View.VISIBLE);
        } else if (keyGuard == null) {
            
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "MainActivity: Can't get security state, keyguard is null");
            }
        }
    }

    private void checkForAdminAccess() {
        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(this, DeviceAdmin.class);

        if (devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Has admin access");
            }
            adminStateText.setVisibility(View.GONE);
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "no admin access");
            }
            adminStateText.setVisibility(View.VISIBLE);
        }
    }

    private void setUIForMissingAPI12AbovePermissionButtons(String justGrantedPermission) {

        //only need to check this particular nonsense if on OS 12+.
        //A sigh for me and the user, for all this work.

        missingPermissions = false;//reset to default before every check.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //OS 13 requires notification perm
            //although, we will obviously have trouble notifying them outside of the app that this critical permission is missing.
            //only thing we can do is show the text here.
            if (perm.canPostNotifs()){
                notifPermissionButton.setVisibility(View.GONE);
                notifPermissionMessageTextView.setVisibility(View.GONE);
                isSettingUpPermission = false;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user granted notif permission");
                }
            } else {
                notifPermissionButton.setVisibility(View.VISIBLE);
                notifPermissionMessageTextView.setVisibility(View.VISIBLE);
                missingPermissions = true;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "missing notif permission");
                }
            }

        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            //pass in "justGrantedPermission" directly from the onresult listener to just switch up the UI without a check on "isMissingRequiredPerm".
            if (Objects.equals(justGrantedPermission, BLUETOOTH_CONNECT)) {
                btPermissionButton.setVisibility(View.GONE);
                btPermissionRefresherMessageTextView.setVisibility(View.GONE);
                isSettingUpPermission = false;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user just granted BT connect permission");
                }
            } else if (perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S)) {
                btPermissionButton.setVisibility(View.VISIBLE);
                btPermissionRefresherMessageTextView.setVisibility(View.VISIBLE);
                missingPermissions = true;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "missing BT connect permission");
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "app has BT connect permission");
                }
                btPermissionButton.setVisibility(View.GONE);
                btPermissionRefresherMessageTextView.setVisibility(View.GONE);
            }

            if (missingPermissions) {
                missingPermissionsText.setVisibility(View.VISIBLE);
                if (acceptedEula == 1) {
                    perm.toastMissingPermission(this.getApplicationContext().getResources().getString(R.string.missing_critical_perms));
                }
            } else {
                missingPermissionsText.setVisibility(View.GONE);
                clearNotification(this, TAG, NOTIF_ID_MISSING_PERMISSIONS);
            }
        }






    }

    private void adminRequestIfNeededAndOrSaveLockingVal() {
        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(this, DeviceAdmin.class);
        if (devicePolicyManager != null && !devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
            //Begin enabling device administrator
            Intent deviceAdminEnableIntent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);

            deviceAdminEnableIntent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminComponentName);
            deviceAdminEnableIntent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                    getApplicationContext().getResources().getString(R.string.device_admin_popup_explainer));
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "bumping auth timeout, as it's asking for device admin");
            }
            adminTimeout = SystemClock.elapsedRealtime() + 30000;//30 second expiry for admin-bumping thing
            iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, SystemClock.elapsedRealtime() + 300000);
            startActivityForResult(deviceAdminEnableIntent, ADMIN_ACTIVATION_REQUEST);
        } else if (devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)){
            iqd.setSettingsTempLongVal(PHONE_LOCKING_ENABLED, 1);
            if (BuildConfig.DEBUG) {
                long val = iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED);
                Log.d(TAG, "phone lock setting: " + val + ", now checking for admin");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADMIN_ACTIVATION_REQUEST) {//device admin intent enable was requested
            adminTimeout = 0;
            if (resultCode != Activity.RESULT_OK) {
                adminStateText.setVisibility(View.VISIBLE);
                phoneLockToggleSwitch.setChecked(false);

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "admin not granted.");
                }
                iqd.setSettingsTempLongVal(PHONE_LOCKING_ENABLED, 0);

            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "admin granted, saving PHONE_LOCKING_ENABLED = 1");
                }
                adminStateText.setVisibility(View.GONE);
                iqd.setSettingsTempLongVal(PHONE_LOCKING_ENABLED, 1);

                if (perm.hasAllBaselinePermissions()) {
                    //if this was the last permission they were missing, then clear the sticky notification since they now have it.
                    clearNotification(this, TAG, NOTIF_ID_MISSING_PERMISSIONS);
                }
                if (BuildConfig.DEBUG) {
                    long val = iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED);
                    Log.d(TAG, "phone lock setting: " + val + ", now checking for admin");
                }
            }
        } else if (requestCode == PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY){
            if (resultCode == RESULT_OK) {
                isAuthenticated = true;

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user used PIN-only for auth. isAuthenticated: " + isAuthenticated);
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "bumping auth timeout");
                }
                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, SystemClock.elapsedRealtime() + 300000);
                alterUIAuthenticationForState(true);
            } else {
                isAuthenticated = false;
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "set auth grace to 0 after auth fail, ensures was not overridden elsewhere by this scatterbrain code");
                }
                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user used PIN-only for auth. isAuthenticated: " + isAuthenticated);
                }
                alterUIAuthenticationForState(false);
                finishAffinity();
            }
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister broadcast listeners
        try {
            unregisterReceiver(btConnReceiver);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Couldn't unregister BTconnReceiver for unknown reasons", e);
            }
        }
        try {
            unregisterReceiver(authReceiver);
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Couldn't unregister authReceiver for unknown reasons", e);
            }
        }

        reauthTriggerTime = 0;
        iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, reauthTriggerTime);

        super.onDestroy();
    }



    @Override
    public void onBackPressed() {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Back pressed, fixing Q's memory leak manually. Backstack count:" + getSupportFragmentManager().getBackStackEntryCount());
        }
        try {
            if (Build.VERSION.SDK_INT == 29 && isTaskRoot() //&& navHostFragment().childFragmentManager.backStackEntryCount == 0/*seems only for fragments?*/
                    && getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "using finishAfterTransition() to fix Q leak.");
                }
                finishAfterTransition();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to do Q fix for onBackPressed memory leak.");
            }
            super.onBackPressed();
        }
    }


}

