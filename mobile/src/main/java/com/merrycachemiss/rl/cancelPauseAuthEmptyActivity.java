/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.util.bioPinAuthentication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK_JOB_ID;

import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_TASK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CONSTANT_PIN_MODE;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_CONSTANT_PIN;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_LOCK_RETRIES;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_TASKER_TASK;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_PAUSE_CONSTANT_PIN;

public class cancelPauseAuthEmptyActivity extends AppCompatActivity {
    private final String TAG = "cancelPauseAuthEmptAct";
    private int failCount = 0;
    private int item;
    private boolean isPause;
    private bioPinAuthentication auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Transparent);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biom_empty);

        Toolbar myToolbar = findViewById(R.id.emptyActivityToolbar);
        setSupportActionBar(myToolbar);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "handling event (pause, cancel, etc) via onCreate");
        }
        auth = new bioPinAuthentication(this, TAG);
        handleIntents(null);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "handling event (pause, cancel, etc) via onNewIntent");
        }
        handleIntents(intent);

    }


    private void handleIntents(Intent intent) {
        failCount = 0;

        Bundle extras;
        if (intent == null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "intent is null");
            }
            extras = getIntent().getExtras();
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "intent is NOT null");
            }
            extras = intent.getExtras();
        }

        if (extras != null) {//only perform actions if intents were actually sent
            item = extras.getInt("item", 0);
            isPause = extras.getBoolean("isPause", false);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "item: " + item + " ispause: " + isPause);
            }
            LocalBroadcastManager.getInstance(this).registerReceiver(authReceiver, new IntentFilter("com.merrycachemiss.rl.authActivityResult"));

            try {
                if (item == NOTIF_ID_CANCEL_SCHEDULED_LOCK) {
                    auth.authenticate(false, false, PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_LOCK_RETRIES);
                } else if (item == NOTIF_ID_CANCEL_SCHEDULED_TASK) {
                    auth.authenticate(false, false, PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_TASKER_TASK);
                } else if (item == NOTIF_ID_CONSTANT_PIN_MODE && !isPause) {
                    auth.authenticate(false, false, PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_CONSTANT_PIN);
                } else if (item == NOTIF_ID_CONSTANT_PIN_MODE && isPause) {
                    auth.authenticate(false, false, PIN_ONLY_AUTH_REQUEST_FOR_PAUSE_CONSTANT_PIN);
                } else {//fallback in case things get weird. Every day is Halloween, you know.
                    auth.authenticate(false, false, 0);
                }


            } catch (Exception e) {
                if (!isPause) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to launch biometric prompt (no PIN, other error? can't lock the user out if so), cancelling notif if screen isn't locked", e);
                    }
                    cancelItemDueToFailureIfScreenUnlocked(item);
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to launch biometric prompt (no PIN, other error? can't lock the user out if so), doing pause action if screen isn't locked", e);
                    }
                    pauseAction();
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "roses are red, extras are null");
            }
        }
    }

    private void pauseAction() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "performing pause action");
        }
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());
        //900k is 15 min
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, SystemClock.elapsedRealtime() + 900000);
        if (BuildConfig.DEBUG) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
            String pausedTimeFormatted = sdf.format(new Date(System.currentTimeMillis() + 900000));
            Log.d(TAG, "paused until " + pausedTimeFormatted);
        }
        try {
            Toast.makeText(getApplicationContext(),
                    "Paused constant PIN requirement for 15 minutes.",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to show paused constant PIN toast");
            }
        }
        constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
        constHelp.manageConstantPIN15minPauseJob(getApplicationContext(), true);

    }


    private void cancelItemDueToFailureIfScreenUnlocked(int cancelItem) {

        KeyguardManager keyguardManager = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        if (keyguardManager != null && !keyguardManager.isKeyguardLocked()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to get biometrics, but screen is unlocked, performing action on id: " + cancelItem);
            }
            if (cancelItem == NOTIF_ID_CANCEL_SCHEDULED_LOCK) {
                jobCancelClearNotif cancelClear = new jobCancelClearNotif(this, TAG);
                cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                cancelClear.cancelJobClearNotif();

                //finish();
                finishAndRemoveTask();
            } else if (cancelItem == NOTIF_ID_CANCEL_SCHEDULED_TASK) {
                jobCancelClearNotif cancelClear = new jobCancelClearNotif(this, TAG);
                cancelClear.setJobAndNotifParams(TASKER_TASK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_TASK, TASKER_JOB_SCHEDULED);
                cancelClear.cancelJobClearNotif();
            } else if (cancelItem == NOTIF_ID_CONSTANT_PIN_MODE) {

                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());
                constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
                constHelp.setTempAlarmVarToCancelConstantPIN(this, iqd);
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to use biometrics to cancel item, but screen is locked. didn't perform action on id: " + cancelItem);
            }
        }
        finishAndRemoveTask();

    }


    private void authSuccess(int authFromNotificationType) {
        failCount = 0;//in case it's kept around from an oversight, reset it.
        //20221022 - changed everything here from "item" to "authFromNotificationType", locking the request type to the direct broadcast that was received to minimize cross-talk.
        if (authFromNotificationType != PIN_ONLY_AUTH_REQUEST_FOR_PAUSE_CONSTANT_PIN) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "auth success. isPause: " + isPause + " cancelItem: " + item + " authFromNotificationType: " + authFromNotificationType);
            }
            if (authFromNotificationType == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_LOCK_RETRIES) {
                jobCancelClearNotif cancelClear = new jobCancelClearNotif(getApplicationContext(), TAG);
                cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                cancelClear.cancelJobClearNotif();
            } else if (authFromNotificationType == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_TASKER_TASK) {
                jobCancelClearNotif cancelClear = new jobCancelClearNotif(getApplicationContext(), TAG);
                cancelClear.setJobAndNotifParams(TASKER_TASK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_TASK, TASKER_JOB_SCHEDULED);
                cancelClear.cancelJobClearNotif();
            } else if (authFromNotificationType == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_CONSTANT_PIN) {
                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());
                constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
                constHelp.setTempAlarmVarToCancelConstantPIN(getApplicationContext(), iqd);
            }
        } else {
            pauseAction();
        }

        unregisterAuthReceiver();

        finishAndRemoveTask();

    }

//    private void authFail(boolean canDoBiometrics, boolean pinPassOnly) {
//        failCount++;
//        unregisterAuthReceiver();
//        if (failCount > 5 && canDoBiometrics && !pinPassOnly) {
//            //if using biometricprompt, should close the whole thing, only if they fail too many times, not just for a single failure
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "closed after too many biometricprompt auth fails");
//            }
//            failCount = 0;//in case it's kept around... OEMs and weird behaviours, am I right?
//            finishAndRemoveTask();
//        } else if (!canDoBiometrics || pinPassOnly){
//            //if returning from pinpassonly failure, close the whole thing
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "closed after returning from pinpassonly failure");
//            }
//            failCount = 0;//in case it's kept around... OEMs and weird behaviours, am I right?
//            finishAndRemoveTask();
//        } else {
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "canDoBiometrics: " + canDoBiometrics + " pinPassOnly: " + pinPassOnly + " authfail count: " + failCount);
//            }
//        }
//        //finishAndRemoveTask();
//    }


    private void unregisterAuthReceiver(){
        try {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "unregistering authreceiver");
            }
            unregisterReceiver(authReceiver);
        }catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Couldn't unregister authReceiver for unknown reasons");
            }
        }
    }

    //for non-biometricprompt version (PIN-only mode). Maybe this has to be in bioPinAuthentication, though.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_TASKER_TASK || requestCode == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_LOCK_RETRIES
        || requestCode == PIN_ONLY_AUTH_REQUEST_FOR_CANCEL_CONSTANT_PIN || requestCode == PIN_ONLY_AUTH_REQUEST_FOR_PAUSE_CONSTANT_PIN) {
            if (resultCode == RESULT_OK) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user used PIN-only for auth, handling result via onActivityResult. isAuthenticated: true");
                }
                authSuccess(requestCode);
                }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user used PIN-only for auth. isAuthenticated: false");
            }
            //authFail(false, true);//just force the vars for this situation
        }

    }

    //for biometricprompt version
    private final BroadcastReceiver authReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                //user successfully authenticated, so perform action via receiver sent from biometric auth.
                //...
                //...I know. But it's super easy to do it this way, and I'm not going to start using Kotlin just for coroutines at this moment.
                boolean isAuthenticated = intent.getBooleanExtra("success", false);
                boolean canDoBiometrics = intent.getBooleanExtra("canDoBiometrics", false);
                boolean pinPassOnly = intent.getBooleanExtra("pinPassOnly", false);
                int authFromNotificationType = intent.getIntExtra("authFromNotificationType", 0);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "authReceiver's isAuthenticated: " + isAuthenticated);
                }
                if (isAuthenticated) {
                    authSuccess(authFromNotificationType);
                } else {
                    //authFail(canDoBiometrics, pinPassOnly);
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "intent was null. Probably cancelled request. Didn't action.");
                }
                //authFail(true, false);//just force the vars for this situation, but allow 5 tries for this one.
            }
        }
    };



}
