/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;

import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.merrycachemiss.rl.receivers.DeviceAdmin;
import com.merrycachemiss.rl.util.bioPinAuthentication;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public final class aboutAppActivity extends AppCompatActivity implements View.OnClickListener{
    private static String TAG;
    private Button disableDeviceAdminButton;
    private DevicePolicyManager devicePolicyManager;
    private ComponentName deviceAdminComponentName;
    private bioPinAuthentication auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
        if (BuildConfig.DEBUG){
            TAG = "aboutAppActivity";
        } else{
            TAG = "q";
        }

        disableDeviceAdminButton = findViewById(R.id.disableDeviceAdminButton);
        Toolbar myToolbar = findViewById(R.id.aboutToolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        final TextView eulaPpDisclaimerText = findViewById(R.id.eulaPpDisclaimerText);
        final TextView MCMtwitterLink = findViewById(R.id.MCMtwitterLink);
        final TextView MCMwebLink = findViewById(R.id.MCMwebLink);
        final TextView MCMemailLink = findViewById(R.id.MCMemailLink);
        final TextView tipsText = findViewById(R.id.tipsText);

        eulaPpDisclaimerText.setOnClickListener(this);
        disableDeviceAdminButton.setOnClickListener(this);

        Linkify.addLinks(MCMwebLink, Linkify.WEB_URLS);
        Linkify.addLinks(MCMemailLink, Linkify.EMAIL_ADDRESSES);
        Linkify.addLinks(tipsText, Linkify.WEB_URLS);
        MCMtwitterLink.setMovementMethod(LinkMovementMethod.getInstance());

        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(this, DeviceAdmin.class);

        auth = new bioPinAuthentication(this, TAG);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.eulaPpDisclaimerText) {

            Intent intent2 = new Intent(this, eulaPpActivity.class);
            startActivity(intent2);
        } else if (v.getId() == R.id.disableDeviceAdminButton){
            if (devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
                
                Log.d(TAG, "try biometric/auth before disable device admin");
                try {
                    authThenDisableAdmin();
                } catch (Exception e){
                    Log.e(TAG, "fail biometric/auth before disable device admin");
                }
            } else {
                try{
                    Toast.makeText(this,
                            "Admin already disabled.",
                            Toast.LENGTH_LONG).show();
                } catch (Exception e){
                    //Log.d(TAG, "failed to make toast");
                }
            }

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //actionbar's "back" should not kill activities.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void authThenDisableAdmin() {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "launching biometric auth");
            }
            
            Executor biometricExecutor = Executors.newSingleThreadExecutor();

            final String biometricTitle = getApplicationContext().getResources().getString(R.string.please_authenticate);
            BiometricPrompt.PromptInfo rlBiometricPrompt;
            rlBiometricPrompt = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle(biometricTitle)
                    //.setNegativeButtonText(biometricExit)
                    //.setDeviceCredentialAllowed(true)
                    .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG | BiometricManager.Authenticators.BIOMETRIC_WEAK | BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                    .build();

            final BiometricPrompt myBiometricPrompt = new BiometricPrompt(this, biometricExecutor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                    if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "user cancelled biometric prompt");
                        }
                    } else {
                        try {
                            runOnUiThread(() -> Toast.makeText(getApplicationContext(),
                                    "Too many failed attempts, no authentication set up, or other error.",
                                    Toast.LENGTH_LONG).show());
                        } catch (Exception e){
                            if (BuildConfig.DEBUG) {
                                Log.e(TAG, "failed to toast", e);
                            }
                        }
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "unrecoverable biometric prompt error - too many failures, or user left activity");
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "showing toast of fail");
                    }

                    try {
                        runOnUiThread(() -> Toast.makeText(getApplicationContext(),
                                "Disable Device Admin via your phone Settings instead.",
                                Toast.LENGTH_LONG).show());
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to toast", e);
                        }
                    }
                }

                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "biometric auth success - removing device admin");
                    }
                    

                    try{
                        deviceAdminComponentName = new ComponentName(getApplicationContext(), DeviceAdmin.class);
                        if(devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
                            devicePolicyManager.removeActiveAdmin(deviceAdminComponentName);
                        }
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to remove device admin", e);
                        }
                        
                    }
                }

                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "biometric auth fail");
                    }
                    try {
                        runOnUiThread(() -> Toast.makeText(getApplicationContext(),
                                "Authentication error",
                                Toast.LENGTH_LONG).show());
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to toast", e);
                        }
                    }
                }


            });
            myBiometricPrompt.authenticate(rlBiometricPrompt);
    }


    @Override
    protected void onResume() {
        super.onResume();
        auth.adjustTimeoutForPresence(false);
        disableDeviceAdminButton.setEnabled(devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName));
    }

    @Override
    protected void onPause() {
        super.onPause();
        auth.adjustTimeoutForPresence(true);

    }


    @Override
    protected void onDestroy() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onDestroy was called");
        }
        super.onDestroy();
    }
}
