/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REQUIRE_BIOMETRIC_AUTH_USE_APP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK_JOB_ID;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_TASK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CONSTANT_PIN_MODE;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class notifHandlerEmptyActivity extends AppCompatActivity {

    final String TAG = "notifHandlerEmptyAct";
    private rldbInsertQueryDelete iqd;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "handling notification event (pause, cancel, etc) via onNewIntent");
        }
        handleIntents(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Transparent);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_handler_empty);
        Toolbar myToolbar = findViewById(R.id.emptyActivityToolbar);
        setSupportActionBar(myToolbar);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "handling notification event (pause, cancel, etc) via onCreate");
        }
        handleIntents(null);


    }


    private void handleIntents(Intent intent){

        throwShade(this);//clear notif shade
        Bundle extras;
        String action;
        if (intent == null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "intent is null");
            }
            extras = getIntent().getExtras();
            action = getIntent().getAction();
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "intent is NOT null");
            }
            extras = intent.getExtras();
            action = intent.getAction();
        }

        if (extras != null && action != null) {//only perform actions if intents were actually sent

            iqd = new rldbInsertQueryDelete(this);


            if (Objects.equals(action, "cancelAction")) {
                int cancelItem = extras.getInt("cancelButtonItem", 0);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user pressed CANCEL on a notif, id: " + cancelItem + " action: " + action);
                }
                KeyguardManager keyguardManager = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);


                if (keyguardManager != null && !keyguardManager.isKeyguardLocked()) {
                    //screen is unlocked, check if user is using biometrics. if not, just cancel the scheduled item.
                    if (iqd.getSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP) == 1) {
                        //user is using biometrics, so launch biometrics activity
                        Intent i = new Intent(this, cancelPauseAuthEmptyActivity.class);
                        i.putExtra("item", cancelItem);
                        i.putExtra("isPause", false);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

                        try {
                            this.startActivity(i);
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.e(TAG, "failed to launch biometric prompt, cancelling notif if screen isn't locked", e);
                            }
                            cancelItemIfScreenUnlocked(this, cancelItem);
                        }


                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "user isn't using biometrics. screen is unlocked, cancelled id: " + cancelItem);
                        }
                        cancelItemIfScreenUnlocked(this, cancelItem);


                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "screen is locked. didn't perform action on id: " + cancelItem);
                    }
                }

            } else if (Objects.equals(action, "pauseAction")) {
                int pauseItem = extras.getInt("pauseButtonItem", 0);

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user pressed PAUSE on a notif, id: " + pauseItem + " action: " + action);
                }
                KeyguardManager keyguardManager = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);


                if (keyguardManager != null && !keyguardManager.isKeyguardLocked()) {
                    //screen is unlocked, check if user is using biometrics. if not, just cancel the scheduled item.
                    if (iqd.getSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP) == 1) {
                        //user is using biometrics, so launch biometrics activity
                        Intent i = new Intent(this, cancelPauseAuthEmptyActivity.class);
                        i.putExtra("item", pauseItem);
                        i.putExtra("isPause", true);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

                        throwShade(this);//clear notif shade

                        try {
                            this.startActivity(i);

                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.e(TAG, "failed to launch biometric prompt, pausing if screen isn't locked", e);
                            }
                            pauseItemIfScreenUnlocked(this);
                        }


                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "user isn't using biometrics. screen is unlocked, pause id: " + pauseItem);
                        }
                        pauseItemIfScreenUnlocked(this);


                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "screen is locked. didn't perform pause action on id: " + pauseItem);
                    }
                    finishAndRemoveTask();
                }
            }
        }
    }



    private void pauseItemIfScreenUnlocked(Context context){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "screen is unlocked, performing pause action");
        }
        //900k is 15 min
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, SystemClock.elapsedRealtime()+900000);
        if (BuildConfig.DEBUG) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
            String pausedTimeFormatted = sdf.format(new Date( System.currentTimeMillis()+900000));
            Log.d(TAG, "paused until " + pausedTimeFormatted);
        }
        try {
            Toast.makeText(context.getApplicationContext(),
                    "Paused constant PIN requirement for 15 minutes.",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to show paused constant PIN toast");
            }
        }
        constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
        constHelp.manageConstantPIN15minPauseJob(context, true);
        finishAndRemoveTask();
    }

    private void cancelItemIfScreenUnlocked(Context context, int cancelItem){

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "screen is unlocked, performing cancel action on id: " + cancelItem);
        }
        if (cancelItem == NOTIF_ID_CANCEL_SCHEDULED_LOCK) {
            jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
            cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
            cancelClear.cancelJobClearNotif();
        } else if (cancelItem == NOTIF_ID_CANCEL_SCHEDULED_TASK) {
            jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
            cancelClear.setJobAndNotifParams(TASKER_TASK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_TASK, TASKER_JOB_SCHEDULED);
            cancelClear.cancelJobClearNotif();
        } else if (cancelItem == NOTIF_ID_CONSTANT_PIN_MODE) {

            constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
            constHelp.setTempAlarmVarToCancelConstantPIN(context, iqd);

        }
        finishAndRemoveTask();
    }


    private void throwShade(Context context){
        if (Build.VERSION.SDK_INT < 31)
            try{
                Intent throwShade = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(throwShade);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to clear notification shade", e);
                }
            }
        else{
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "didn't clear notification shade, since user is on S+. Does it automatically already on S+... finally, something actually convenient about this API level.");
            }
        }
    }



}
