/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.jobs;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.constantPinHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;


public final class constantPinChargingTriggerJob extends JobService {
    private static final String TAG = "constantPinChargTriJob";

    private boolean isWorking = false;
    private boolean jobCancelled = false;


    //As of 2022-11, we now have "plugPowerReceiver" in the main service, but this exists in case
    //the user plugs in while the service was killed by the system (yay OEMs and other dumb reasons)
    //It will still start constant PIN mode in that case.

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job started");
        }
        isWorking = true;

        
        startWorkOnNewThread(jobParameters);

        return isWorking;
    }

    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(() -> doWork(jobParameters)).start();
    }

    private void doWork(JobParameters jobParameters) {
        
        if (jobCancelled) {
            return;
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "start constant PIN service, if within valid parameters");
        }

        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());

        //if bedtime mode setting is 1 AND is within 8HR window
        //OR BT watch disconnect setting is 1 AND something has disconnected
        //then lock the screen if it is off.
        //also - if service not started then start the service
        constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
        if (constHelp.startBedtimeModeHeartbeatAfterChargeValid(iqd) || constHelp.watchMissingAndConstantPinEnabledOrIsManuallyEnabled(iqd)) {

            if (System.currentTimeMillis() >= iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END)) {
                iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);

                //2022-11-03 when service was replaced.
                constHelp.activateConstantPINServiceIfNeeded(getApplicationContext(), iqd);

//                Intent constantPINServiceIntent = new Intent(this, constantPINservice.class);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    this.startForegroundService(constantPINServiceIntent);
//                } else {
//                    this.startService(constantPINServiceIntent);
//                }

            } else if (BuildConfig.DEBUG) {
                Log.d(TAG, "constantPIN heartbeat was running in the past 15 min and the cooldown wasn't reached yet.");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                String timeFormatted = sdf.format(new Date(iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END)));
                Log.d(TAG, "heartbeat cooldown end: " + timeFormatted);
            }

        } else if (BuildConfig.DEBUG) {
            Log.d(TAG, "bedtime protection mode is not enabled, is cancelled/done for the night, or there are no missing wearables. didn't start service");
        }


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job complete/service started. Rescheduling is ON.");
        }
        isWorking = false;

        jobFinished(jobParameters, true);

    }


    
    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job cancelled altogether, no rescheduling");
        }
        jobCancelled = true;
        boolean needsReschedule = false;
        jobFinished(jobParameters, false);
        return needsReschedule;
    }

}