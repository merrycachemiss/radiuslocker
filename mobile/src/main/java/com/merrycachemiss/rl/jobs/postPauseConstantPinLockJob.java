/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.jobs;

import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.util.constantPinHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public final class postPauseConstantPinLockJob extends JobService {
    private static final String TAG = "postPauseConstPinLockJb";

    private boolean isWorking = false;
    private boolean jobCancelled = false;


    //what this does is perform a lock after the 15 minutes of pausing constant PIN mode expires, if the screen is off.
    //otherwise, it is paused and is likely not to actually lock the screen unless it's turned off again, which could be never.
    //it's a one-time trigger, and the job ends itself after execution. no need to keep redoing it every 15 minutes, as it
    //only needs to happen after user pauses constant PIN lock and the pause is done with.

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job started");
        }
        isWorking = true;

        
        startWorkOnNewThread(jobParameters);

        return isWorking;
    }

    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(() -> doWork(jobParameters)).start();
    }

    private void doWork(JobParameters jobParameters) {
        
        if (jobCancelled) {
            return;
        }
        if (BuildConfig.DEBUG){
            Log.d("lockOnceAfterConstPAlrm", "received scheduled alarm from constant PIN pause, locking if screen is off");
        }
        Context context = this.getApplicationContext();
        DisplayManager dm = (DisplayManager) this.getSystemService(Context.DISPLAY_SERVICE);
        if (dm != null) {
            for (Display display : dm.getDisplays()) {
                if (display.getState() != Display.STATE_ON) {
                    if (BuildConfig.DEBUG) {
                        Log.d("lockOnceAfterConstPAlrm", "display is off. lock.");
                    }
                    immediateLocker lock = new immediateLocker(context, false, true, "lockOnceAfterConstPAlrm");
                    lock.lockNOW();
                } else if (BuildConfig.DEBUG) {
                    Log.d("lockOnceAfterConstPAlrm", "display was on, didn't lock");
                }
            }
        }
        //add a small delay before locking again, in case user keeps turning screen on and causing it to lock again and again.
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, SystemClock.elapsedRealtime()+2000);

        constantPinHelper constHelp = new constantPinHelper(context);
        constHelp.manageConstantPIN15minPauseJob(context, false);
        iqd = null;
        dm = null;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job complete. Rescheduling is OFF.");
        }
        isWorking = false;

        jobFinished(jobParameters, false);

    }


    
    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job cancelled altogether, no rescheduling");
        }
        jobCancelled = true;
        boolean needsReschedule = false;
        jobFinished(jobParameters, false);
        return needsReschedule;
    }

}