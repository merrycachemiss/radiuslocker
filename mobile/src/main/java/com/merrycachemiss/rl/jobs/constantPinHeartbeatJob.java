/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.jobs;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.util.constantPinHelper;


import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;

public final class constantPinHeartbeatJob extends JobService {
    private static final String TAG = "constantPinHeartbeatJob";

    private boolean isWorking = false;
    private boolean jobCancelled = false;


    //purpose:
    //when user plugs in, but bedtime mode is not yet valid (8hr within alarm time), this job runs to retry activating bedtime mode
    //every 15 minutes. it will end itself when within the 8hr window


    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job started");
        }
        isWorking = true;
        startWorkOnNewThread(jobParameters);

        return isWorking;
    }

    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(() -> doWork(jobParameters)).start();
    }

    private void doWork(JobParameters jobParameters) {
        if (jobCancelled) {
            return;
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "start constant PIN service, if not already");
        }
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());




        //if bedtime mode settting is 1 AND is within 8HR window
        //OR BT watch disconnect setting is 1 AND something has disconnected
        //then lock the screen if it is off.
        //also - if service not started then start the service

        constantPinHelper constHelp = new constantPinHelper(this);
        if (constHelp.bedtimeModeAndWithin8hrLockingIsValid(iqd) || constHelp.watchMissingAndConstantPinEnabledOrIsManuallyEnabled(iqd)) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "is within 8HR of alarm time");
            }

            constHelp.constantPinModeActivateNow(false);
//            if (!isConstantPINInstanceCreated()) {
//                if (BuildConfig.DEBUG) {
//                    Log.d(TAG, "service was dead, so starting it");
//                }
//                Intent constantPINServiceIntent = new Intent(this, constantPINservice.class);
//
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    this.startForegroundService(constantPINServiceIntent);
//                } else {
//                    this.startService(constantPINServiceIntent);
//                }
//            } else if (BuildConfig.DEBUG) {
//                Log.d(TAG, "service was already running");
//            }

            //if currently within active locking scenarios:
            //if user paused locking for the 15 minutes, this heartbeat job that keeps the service alive can also serve
            //as the trigger to lock the display again after pausing -- IF THE SCREEN IS OFF.
            //if the screen is on, the timer gets set to 0 since so will lock when the screen is turned off
            if (SystemClock.elapsedRealtime() > iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END)) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Job detected that screen pausing period has expired. Checking if display is off so it can lock. also setting pause timestamp to 0.");
                }
                iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, 0);
                DisplayManager dm = (DisplayManager) this.getSystemService(Context.DISPLAY_SERVICE);
                if (dm != null) {
                    for (Display display : dm.getDisplays()) {
                        if (display.getState() != Display.STATE_ON) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "display is off. lock.");
                            }
                            immediateLocker lock = new immediateLocker(this, false, true, TAG);
                            lock.lockNOW();
                        }
                    }
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "pause period is still in effect, so didn't try to lock if screen off");
                }
            }

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Job complete/service started. Rescheduling is ON. Upped heartbeat cooldown time.");
            }
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, System.currentTimeMillis()+900000);
            isWorking = false;
            jobFinished(jobParameters, true);


        } else if (constHelp.stopConstantPinIfNecessary(this, iqd, true) == 1){

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Job considered expired -- is not charging, not within 8hr period, and no disconnected locking wearables are relevant. Rescheduling is OFF, now. Cleared heartbeat cooldown time.");
            }

            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);
            isWorking = false;
            jobFinished(jobParameters, false);
        } else if (constHelp.stopConstantPinIfNecessary(this, iqd, true) == 2){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "is still charging, no watches are missing, and no longer within 8hr of alarm. keeping heartbeat, but stopped service. Upped heartbeat cooldown time.");
            }
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, System.currentTimeMillis()+900000);
            isWorking = false;
            jobFinished(jobParameters, true);

        }

    }


    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Job cancelled altogether, no rescheduling. Cleared heartbeat cooldown time.");
        }
        jobCancelled = true;
        boolean needsReschedule = false;
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(getApplicationContext());
        iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);
        jobFinished(jobParameters, false);
        return needsReschedule;
    }

}