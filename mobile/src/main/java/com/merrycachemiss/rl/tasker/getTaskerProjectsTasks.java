/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.tasker;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;


import java.util.Objects;

import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK_JOB_ID;


import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_TASK;

import androidx.core.content.ContextCompat;

public final class getTaskerProjectsTasks {

    private static final String TAG = "getTaskerProjectsTasks";
    private final ContentResolver db;
    private final Uri tasksURI = Uri.parse("content://net.dinglisch.android.tasker/tasks");
    private final Uri prefsURI = Uri.parse("content://net.dinglisch.android.tasker/prefs");
    public static String[] taskStringArray;
    private final Context context;
    public static int checkedItem = 0;
    private boolean doesTaskMatchInDB = false;
    private boolean taskerFunctional = false;
    private BroadcastReceiver br;
    public getTaskerProjectsTasks(Context context){
        this.db = context.getContentResolver();
        this.context = context;

    }

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    public void executeTask(final String taskName, final boolean thisIsOnlyATest){
        testTaskerCapability();
        if (taskerFunctional && !Objects.equals(taskName, "") && !Objects.equals(taskName, "RL: NONE") && taskName != null) {
            TaskerIntent i = new TaskerIntent(taskName);

            br = new BroadcastReceiver() {
                public void onReceive(Context context, Intent recIntent) {
                    if (recIntent.getBooleanExtra(TaskerIntent.EXTRA_SUCCESS_FLAG, false)) {
                        // success, do something
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Tasker reports success for executing " + taskName);
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Tasker reports failure for executing " + taskName);
                        }
                    }

                    if (!thisIsOnlyATest) {
                        try {
                            jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                            cancelClear.setJobAndNotifParams(TASKER_TASK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_TASK, TASKER_JOB_SCHEDULED);
                            cancelClear.cancelJobClearNotif();

                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "failed to update DB status to task scheduled = 0");
                            }
                            
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "tested " + taskName + ", didn't cancel related job since it was only a test");
                        }
                    }

                    context.unregisterReceiver(br);
                }
            };

            // You probably want to unregister this if the user leaves your app e.g. in onPause
            // You may want to set a timeout in case e.g. Tasker's queue is full
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ContextCompat.registerReceiver(context, br, i.getCompletionFilter(), ContextCompat.RECEIVER_EXPORTED);
            } else {
                context.registerReceiver(br, i.getCompletionFilter());
            }


            if (BuildConfig.DEBUG) {
                Log.d(TAG, "sending tasker task execution broadcast");
            }
            context.sendBroadcast(i);

            Runnable unregTaskerRecAfterDelay = () -> {
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to sleep before unregistering tasker receiver");
                    }
                }
                try {
                    context.unregisterReceiver(br);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to unregister tasker receiver after delay -- possibly already dead");
                    }
                }

            };

            new Thread(unregTaskerRecAfterDelay).start();

        } else{
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "tasker isn't configured properly, or task name is blank");
            }
        }


    }

    public void unregisterRec(){
        context.unregisterReceiver(br);
    }

    public String testTaskerCapability() {
        TaskerIntent.Status status = TaskerIntent.testStatus(context);

        switch ( status ) {
            case OK:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Tasker is on and accessible by RL");
                }
                taskerFunctional = true;
                return "RL";
            case NotInstalled:
                taskerFunctional = false;
                return context.getString(R.string.tasker_error_not_installed);
            case NoPermission:
                taskerFunctional = false;
                return context.getString(R.string.tasker_error_installed_after_rl);
            case NotEnabled:
                taskerFunctional = false;
                return context.getString(R.string.tasker_error_not_enabled);
            case AccessBlocked:
                taskerFunctional = false;
                return context.getString(R.string.tasker_error_no_access);
            case NoReceiver:
                taskerFunctional = false;
                return context.getString(R.string.tasker_error_not_installed);
        default:
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Tasker failure status: " + status);
            }
            taskerFunctional = false;
            break;
        }
        taskerFunctional = false;
        return "";
    }

    public void getTasks(String existingTask){

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getTasks() get tasker projects/tasks");
        }
        
        try {
            Cursor c = db.query(
                    tasksURI,
                    null,
                    null,
                    null,
                    null);

            if (c != null){

                if (c.getCount() != 0 && !c.isAfterLast()) {
                    int projName = c.getColumnIndex("project_name");
                    int taskName = c.getColumnIndex("name");
                    taskStringArray = new String[c.getCount() + 1];
                    taskStringArray[0] = "RL: NONE";
                    while (c.moveToNext()) {
                        taskStringArray[c.getPosition() + 1] = c.getString(taskName);
                    }

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "task count: " + c.getCount() + " first: " + taskStringArray[1] + " last: " + taskStringArray[c.getCount()]);
                    }

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "loop thru task array from Tasker to find previously chosen task, set preselect position for dialog popup");
                    }

                    /*preselect the previously found task's pos in the dialog's list*/
                    for (int i = 1; i < taskStringArray.length; i++) {
                        if (taskStringArray[i].equals(existingTask)) {
                            //found that previously chosen item in DB matches current found one from tasker API return
                            checkedItem = i;
                            doesTaskMatchInDB = true;
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "ITEM FROM TASKER \"" + taskStringArray[i] + "\" MATCHES DB ENTRY \"" + existingTask + "\"");
                            }
                            break;
                        } else {
                            checkedItem = 0;

                        }
                    }

                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Tasker is EMPTY. Show only one entry in dialog - the default RL: NONE one");
                    }
                    //Tasker is EMPTY. Show only one entry in dialog
                    doesTaskMatchInDB = false;
                    taskStringArray = new String[1];
                    taskStringArray[0] = "RL: NONE";


                }
                c.close();
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to get tasker tasks, cursor was null");
                }
            }


        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to get tasker tasks", e);
            }
            
        }
    }

    public boolean doesTaskMatchInDB(){
        return doesTaskMatchInDB;
    }
    private boolean getInstalledStatus(){
        return TaskerIntent.taskerInstalled(context);
    }

// --Commented out by Inspection START (2019-11-16 2:02 PM):
//    public boolean getTaskerExtAccessState(){
//
//        if (getInstalledStatus()) {
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "get tasker settings state");
//            }
//            
//
//            try {
//                Cursor c = db.query(
//                        prefsURI,
//                        null,
//                        null,
//                        null,
//                        null);
//
//                assert c != null;
//                if (c.getCount() != 0) {
//
//                    int enabledSetting = c.getColumnIndex("enabled");
//                    int extAccessSetting = c.getColumnIndex("ext_access");
//                    if (c.moveToFirst()) {
//                        if (BuildConfig.DEBUG) {
//                            Log.d(TAG, c.getString(enabledSetting) + ", " + c.getString(extAccessSetting));
//                        }
//                        if (Objects.equals(c.getString(extAccessSetting), "true")) {
//                            //ext access is enabled, stop operation and return TRUE
//                            c.close();
//                            return true;
//                        }
//                    }
//                }
//                c.close();
//            } catch (Exception e) {
//                if (BuildConfig.DEBUG) {
//                    Log.d(TAG, "failed to get tasker settings state");
//                }
//                
//            }
//            return false;
//        }
//        return false;
//    }
// --Commented out by Inspection STOP (2019-11-16 2:02 PM)


}
