/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.Context;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.Manifest;

import com.google.android.material.snackbar.Snackbar;
import com.merrycachemiss.rl.adapters.wifiDeviceLVAdapter;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.bioPinAuthentication;
import com.merrycachemiss.rl.util.getSetCurrentWIFISSID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public final class wifiSafeListPickerActivity extends AppCompatActivity implements View.OnClickListener {

    public static boolean[] WIFIDeviceListCheckedState;
    private ArrayList<String> displayedWifiDeviceArrayList;
    private WifiManager wifiManager;
    private ListView wifiDeviceListview;
    private TextView wifiSSIDchangeDisclaimer;
    //private deviceDbHandler dbHandler;
    private TextView noWIFIDevicesFoundText;
    private ScrollView locationPermissionScrollview;
    //private TextView locPermissionReasonsURLText;
    private TextView android81WifiLocPermission;
    private Button turnOnLocServicesButton;
    private Button requestPermissionButton;
    private Button addWIFIbutton;
    private Button refreshWIFIbutton;
    private final int LOCATION_PERMISSION_REQUEST = 99;
    private static String TAG;
    private boolean locServicesEnabled = false;
    private static ArrayList<String> filteredScanResultsDistinct;
    //private boolean locPermissionEnabled = false;
    private bioPinAuthentication auth;
    private rldbInsertQueryDelete iqd;
    private long lastRefresh;
    AlertDialog addSSIDdialog;
    AlertDialog bgPermsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_picker);
        Toolbar myToolbar = findViewById(R.id.wifiToolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        lastRefresh = 0;
        if (BuildConfig.DEBUG) {
            TAG = "wifiSafeListPickerActi";
        } else {
            TAG = "f";
        }
        iqd = new rldbInsertQueryDelete(this);


        wifiDeviceListview = findViewById(R.id.wifiDeviceListview);
        wifiSSIDchangeDisclaimer = findViewById(R.id.wifiSSIDchangeDisclaimer);
        noWIFIDevicesFoundText = findViewById(R.id.noWIFIDevicesFoundText);
        locationPermissionScrollview = findViewById(R.id.locationPermissionScrollview);
        android81WifiLocPermission = findViewById(R.id.android81WifiLocPermissionText);
        turnOnLocServicesButton = findViewById(R.id.turnOnLocServicesButton);
        requestPermissionButton = findViewById(R.id.requestPermissionButton);
        addWIFIbutton = findViewById(R.id.addWIFIbutton);
        refreshWIFIbutton = findViewById(R.id.refreshWIFIbutton);
        //locPermissionReasonsURLText = findViewById(R.id.locPermissionReasonsURLText);

        turnOnLocServicesButton.setOnClickListener(this);
        requestPermissionButton.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= 29) {
            addWIFIbutton.setOnClickListener(this);
            refreshWIFIbutton.setOnClickListener(this);
        } else {
            addWIFIbutton.setEnabled(false);
            addWIFIbutton.setVisibility(View.GONE);
            refreshWIFIbutton.setEnabled(false);
            refreshWIFIbutton.setVisibility(View.GONE);
        }
        //locPermissionReasonsURLText.setMovementMethod(LinkMovementMethod.getInstance());
        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        wifiDeviceListview.setOnItemClickListener((parent, view, position, id) -> {
            //get actual bt device info from second array (the indexes match) - just shows in toast for now
            //later you'll want to store the info in sql lite db or something when they check the item
            final String WIFIdevice = displayedWifiDeviceArrayList.get(position);

            //toggle checkbox indirectly for row chosen
            CheckBox wifiDeviceNameCheckBox = view.findViewById(R.id.wifiDeviceNameCheckBox);
            wifiDeviceNameCheckBox.toggle();
            //store checkbox states for scrolling
            if (wifiDeviceNameCheckBox.isChecked()) {
                WIFIDeviceListCheckedState[position] = true;

                //store into DB
                //dbHandler.addSafeWIFIDevice(WIFIdevice);

                Runnable insertThread = () -> {
                    iqd.safeWIFIInsert(WIFIdevice);
                    Log.d(TAG, "inserted " + WIFIdevice);
                };
                new Thread(insertThread).start();
            } else {
                WIFIDeviceListCheckedState[position] = false;

                Runnable deleteThread = () -> {
                    iqd.safeWIFIDelete(WIFIdevice);
                    Log.d(TAG, "deleted " + WIFIdevice);
                };
                new Thread(deleteThread).start();
            }
        });
        auth = new bioPinAuthentication(this, TAG);
    }

    protected void onResume() {
        super.onResume();
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        auth.adjustTimeoutForPresence(false);

        addWIFIbutton.setVisibility(View.GONE);
        refreshWIFIbutton.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= 29) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            this.registerReceiver(wifiScanReceiver, intentFilter);
        }
        wifiDeviceListview.setVisibility(View.GONE);
        noWIFIDevicesFoundText.setVisibility(View.VISIBLE);

        //Android O 8.1 and above require that location permissions be granted in order to check WIFI in background.
        //only request these permissions on those versions.
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            locationPermissionScrollview.setVisibility(View.VISIBLE);
            osLocationServicesEnabled();
            locationPermissionEnabled(true);//avoid recursive rescan, will do it manually here
            if (locationPermissionEnabledCheck(true)) {//avoid recursive rescan, will do it manually here
                if (android.os.Build.VERSION.SDK_INT >= 29 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "lost wifi bg perm and user is on >=29, showing dialog in onresume.");
                    }
                    getBackgroundLocPermissions();
                } else if (android.os.Build.VERSION.SDK_INT >= 29 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "already had wifi bg perm or is on <29, avoiding showing dialog in onresume.");
                    }
                    if (bgPermsDialog != null) {
                        bgPermsDialog.dismiss();
                    }
                    generateWIFIDeviceListQ(null, false);
                } else if (android.os.Build.VERSION.SDK_INT < 29) {
                    try {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "begin wifi list generate - use has permissions for location and is < 29/Q.");
                        }

                        generateWIFIDeviceList();
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "fail wifi list generate", e);
                        }


                        Toast.makeText(wifiSafeListPickerActivity.this,
                                "Can't get WIFI list",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
            //Log.d("safewifipicker","locServicesEnabled:" + locServicesEnabled + " locPermissionEnabled: " + locPermissionEnabled);
        } else {//user running < 8.1 or has location permissions

            try {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "begin wifi list generate");
                }

                generateWIFIDeviceList();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "fail wifi list generate", e);
                }


                Toast.makeText(wifiSafeListPickerActivity.this,
                        "Can't get WIFI list",
                        Toast.LENGTH_SHORT).show();
            }

        }


    }

    private void generateWIFIDeviceListQ(ArrayList<String> scannedNearbyWifiList, boolean fromScan) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "generateWIFIDeviceListQ: user is on Q or above. generating the annoying version of the list, because Google");
        }
        if (wifiManager.isWifiEnabled() && locServicesEnabled) {

            locationPermissionScrollview.setVisibility(View.GONE);

            if (locationPermissionEnabledCheck(true)) {
                try {
                    //@SuppressLint("MissingPermission") List<WifiConfiguration> storedWIFIDevices = wifiManager.getConfiguredNetworks();

                    //displayedWifiDeviceArrayList = new ArrayList<>();//reset arraylist

                    displayedWifiDeviceArrayList = iqd.getAllSafeWifiAsList();
                    int listSize = 0;

                    if (displayedWifiDeviceArrayList != null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "size of all saved wifi list: " + displayedWifiDeviceArrayList.size());
                        }
                        Collections.sort(displayedWifiDeviceArrayList, String.CASE_INSENSITIVE_ORDER);
                        listSize = displayedWifiDeviceArrayList.size();
                    }
                    int scannedListSize = 0;
                    //int scannedListAmountTrimmed = 0;
                    if (scannedNearbyWifiList != null) {
                        //add scanned list to end of main list
                        String scannedWifiNetworkName;
                        //remove items in list that belong in the exising saved wifi list of user
                        for (int i = 0; i < scannedNearbyWifiList.size(); i++) {
                            scannedWifiNetworkName = scannedNearbyWifiList.get(i);
                            //only add unique names to list
                            if (displayedWifiDeviceArrayList != null) {
                                for (String s : displayedWifiDeviceArrayList) {
                                    if (s.equals(scannedWifiNetworkName)) {
                                        scannedNearbyWifiList.remove(i);
                                        i--;
                                    }
                                }
                            }

                        }
                        scannedListSize = scannedNearbyWifiList.size();
                        for (int i = 0; i < scannedListSize; i++) {
                            if (displayedWifiDeviceArrayList == null) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "displayedWifiDeviceArrayList is null, as no safe wifi existed before now, init as new");
                                }
                                displayedWifiDeviceArrayList = new ArrayList<>();
                            }
                            displayedWifiDeviceArrayList.add(scannedNearbyWifiList.get(i));
                        }
                        if (displayedWifiDeviceArrayList != null) {
                            listSize = displayedWifiDeviceArrayList.size();
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "scannedNearbyWifiList is null");
                        }

                    }

                    //if (displayedWifiDeviceArrayList != null) {
                    if (listSize > 0) {
                        //FIRST ARRAY FOR DISPLAYING THE HUMAN-READABLE wifi DEVICE INFO IN LISTVIEW

                        //*todo*
                        //you must get both the saved list and the scanned list. when (re)scan, this WIFIDeviceListCheckedState must be flushed and recreated.
                        //it must be equal to the saved list plus scanned list, minus the shared names.

                        WIFIDeviceListCheckedState = new boolean[displayedWifiDeviceArrayList.size()];
                        for (int i = 0; i < displayedWifiDeviceArrayList.size() - scannedListSize; i++) {
                            //only default-checkmark the items within the size of the original non-scanned list
                            WIFIDeviceListCheckedState[i] = true;
                        }


                        noWIFIDevicesFoundText.setVisibility(View.GONE);
                        wifiDeviceListview.setVisibility(View.VISIBLE);
                        wifiSSIDchangeDisclaimer.setVisibility(View.VISIBLE);
                        ListAdapter storedWIFIDeviceAdapter = new wifiDeviceLVAdapter(this, displayedWifiDeviceArrayList);
                        wifiDeviceListview.setAdapter(storedWIFIDeviceAdapter);
                    } else {
                        wifiDeviceListview.setVisibility(View.GONE);
                        wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                        noWIFIDevicesFoundText.setVisibility(View.VISIBLE);
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "starting wifi scan");
                    }
                    if (!fromScan) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "showing snackbar for wifi refresh, then start scan");
                        }
                        try {
                            if ((Build.VERSION.SDK_INT < 29 || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) && SystemClock.elapsedRealtime() - 12000 > lastRefresh) {
                                try {
                                    CoordinatorLayout coordinatorLayout = findViewById(R.id.wifiSafeListPickerActivityCoordinatorLayout);
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, R.string.refreshing_wifi_connections, 2500);
                                    snackbar.show();
                                } catch (Exception e) {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(TAG, "failed to show snackbar for wifi refresh");
                                    }

                                }
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, SystemClock.elapsedRealtime() - lastRefresh + " gap existed between refresh cooldown");
                                }
                                wifiManager.startScan();
                                lastRefresh = SystemClock.elapsedRealtime();
                            } else {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "User is on < 29 or has BG permission granted, but a refresh was already done within 13s - " + (SystemClock.elapsedRealtime() - lastRefresh) + " gap existed between refresh cooldown");
                                }
                            }
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.e(TAG, "failed wifi scan", e);
                            }

                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Q wifi list was already generated from a scan. avoided doing recursive scan");
                        }
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to gen wifi list", e);
                    }

                    wifiDeviceListview.setVisibility(View.GONE);
                    wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                    locationPermissionScrollview.setVisibility(View.VISIBLE);
                    osLocationServicesEnabled();
                    locationPermissionEnabled(true);
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "permission not granted in additional check before list generate");
                }
                wifiDeviceListview.setVisibility(View.GONE);
                wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                locationPermissionScrollview.setVisibility(View.VISIBLE);
                osLocationServicesEnabled();
                locationPermissionEnabled(true);

            }
        }
    }


    final BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            @SuppressLint("InlinedApi") boolean success = intent.getBooleanExtra(
                    WifiManager.EXTRA_RESULTS_UPDATED, false);
            if (success) {
                //one last check for location permissions
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    lastRefresh = SystemClock.elapsedRealtime();

                    List<ScanResult> rawScanResults = wifiManager.getScanResults();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "size of nearby scanned networks: " + rawScanResults.size());
                    }
                    if (rawScanResults != null) {
                        if (!rawScanResults.isEmpty()) {
                            String ssidNoQuotes;
                            ArrayList<String> filteredScanResults = new ArrayList<>();
                            for (ScanResult config : rawScanResults) {
                                ssidNoQuotes = config.SSID;
                                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length() - 1);
                                }
                                //the device name gets passed to temp list if doesn't exist in main list already
                                if (!Objects.equals(ssidNoQuotes, "")) {
                                    filteredScanResults.add(ssidNoQuotes);
                                }
                            }

                            Collections.sort(filteredScanResults, String.CASE_INSENSITIVE_ORDER);
                            filteredScanResultsDistinct = new ArrayList<>();
                            String previousSSID = "";
                            for (String ssidName : filteredScanResults) {
                                if (!Objects.equals(previousSSID, ssidName)) {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(TAG, "item " + ssidName + " doesn't equal previous element " + previousSSID + " in filtereredScanResultarraylist, adding to new distinct arraylist");
                                    }
                                    filteredScanResultsDistinct.add(ssidName);
                                } else if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "item " + ssidName + " is equal to previous element " + previousSSID + " in filteredScanResults arraylist, didn't add to new distinct arraylist");
                                }
                                previousSSID = ssidName;
                            }
                            //will later be looped through again to ensure items from filteredScanResultsDistinct don't appear if they are within the past saved wifi items list
                            generateWIFIDeviceListQ(filteredScanResultsDistinct, true);
                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "wifi scan finished, but there were no networks nearby. no need to refresh list");
                            }
                            //generateWIFIDeviceListQ(null, true);
                            Toast.makeText(wifiSafeListPickerActivity.this,
                                    "Scanned for networks, but found none nearby. You can still add an SSID manually.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to get nearby wifi. no need to refresh lists.");
                    }

                    Toast.makeText(wifiSafeListPickerActivity.this,
                            "The OS has limited your WIFI refreshing, or there was a general failure. Try again in 30-60 seconds.",
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(wifiSafeListPickerActivity.this,
                        "Missing location permissions",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

    private void generateWIFIDeviceList() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "generateWIFIDeviceList(): user is < Q. Generating the user-friendly version of the list");
        }
        if (wifiManager.isWifiEnabled()) {

            locationPermissionScrollview.setVisibility(View.GONE);
//            if (Build.VERSION.SDK_INT >= 29) {
//                List<WifiNetworkSuggestion> storedWIFIDevices = wifiManager.addNetworkSuggestions();
//            } else {

            if (locationPermissionEnabledCheck(true) || Build.VERSION.SDK_INT < 27) {
                try {
                    @SuppressLint("MissingPermission") List<WifiConfiguration> storedWIFIDevices = wifiManager.getConfiguredNetworks();

                    // }
                    if (!storedWIFIDevices.isEmpty()) {
                        //FIRST ARRAY FOR DISPLAYING THE HUMAN-READABLE wifi DEVICE INFO IN LISTVIEW
                        displayedWifiDeviceArrayList = new ArrayList<>();

                        int pos = 0;
                        WIFIDeviceListCheckedState = new boolean[storedWIFIDevices.size()];//creates/stores checkmark states based on DB result. listview adapter sets checks.

                        for (WifiConfiguration config : storedWIFIDevices) {
                            String ssidNoQuotes = config.SSID;
                            if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                                ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length() - 1);
                            }
//                ssidNoQuotes = ssidNoQuotes.replace("\"","");
//                ssidNoQuotes = ssidNoQuotes.replace("\'","");
//                ssidNoQuotes = ssidNoQuotes.replace("\\","");
                            //the device name gets passed to listview adapter:
                            displayedWifiDeviceArrayList.add(ssidNoQuotes);

                        }
                        Collections.sort(displayedWifiDeviceArrayList, String.CASE_INSENSITIVE_ORDER);

                        //now that the list is sorted, iterate through it again (...I know) to mark checkbox state if in db.
                        for (String ssidNoQuotes : displayedWifiDeviceArrayList) {
                            //creates/stores checkmark states based on DB result. listview adapter sets checks.
                            final String[] selectionArgs = {ssidNoQuotes};
                            WIFIDeviceListCheckedState[pos] = iqd.genericSingleItemExistsQuery(rldbContract.SafeWIFIQuery.CONTENT_URI, rldbContract.SafeWIFIQuery.PROJECTION_SSID,
                                    selectionArgs);
                            //dbHandler.isOnSafeWIFI(ssidNoQuotes);
                            pos++;
                        }

                        noWIFIDevicesFoundText.setVisibility(View.GONE);
                        wifiDeviceListview.setVisibility(View.VISIBLE);
                        wifiSSIDchangeDisclaimer.setVisibility(View.VISIBLE);
                        ListAdapter storedWIFIDeviceAdapter = new wifiDeviceLVAdapter(this, displayedWifiDeviceArrayList);
                        wifiDeviceListview.setAdapter(storedWIFIDeviceAdapter);
                    } else {
                        wifiDeviceListview.setVisibility(View.GONE);
                        wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                        noWIFIDevicesFoundText.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e) {
                        
                        wifiDeviceListview.setVisibility(View.GONE);
                        wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                        locationPermissionScrollview.setVisibility(View.VISIBLE);
                        osLocationServicesEnabled();
                        locationPermissionEnabled(false);
                    }
            } else{
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "permission not granted in additional check before list generate");
                }
                wifiDeviceListview.setVisibility(View.GONE);
                wifiSSIDchangeDisclaimer.setVisibility(View.GONE);
                locationPermissionScrollview.setVisibility(View.VISIBLE);
                osLocationServicesEnabled();
                locationPermissionEnabled(false);

            }
        }
    }

    private void osLocationServicesEnabled(){
        LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (lm != null) {
            try {

                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception e) {

                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to get location services status", e);
                }
            }

            try {

                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception e) {

                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to get network status", e);
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to get network status - LocationManager is null");
            }
        }

        if(!gps_enabled && !network_enabled) {
            osLocationServicesAddRemoveButtons(false);
            return;
        }
        osLocationServicesAddRemoveButtons(true);
    }
    private void osLocationServicesAddRemoveButtons(Boolean isEnabled){
        if (!isEnabled){
            turnOnLocServicesButton.setVisibility(View.VISIBLE);
            //locPermissionReasonsURLText.setVisibility(View.VISIBLE);
            android81WifiLocPermission.setVisibility(View.VISIBLE);
            requestPermissionButton.setVisibility(View.VISIBLE);
            requestPermissionButton.setEnabled(false);
            locServicesEnabled = false;
        } else{
            locServicesEnabled = true;
            turnOnLocServicesButton.setVisibility(View.GONE);
            requestPermissionButton.setEnabled(true);
        }

    }


    private boolean locationPermissionEnabledCheck(boolean fromWifiListGenerate){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "locationPermissionEnabledCheck(fromWifiListGenerate: " + fromWifiListGenerate + ")");
        }
        //Q TARGET STUFF:
        if (Build.VERSION.SDK_INT >= 29 &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED)//for Q target
        ) {
            locationPermissionAddRemoveButtonsGetlist(false, fromWifiListGenerate);
            return false;
        } else if (Build.VERSION.SDK_INT < 29 &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            locationPermissionAddRemoveButtonsGetlist(false, fromWifiListGenerate);
            return false;
        }
        return true;
    }


    private void locationPermissionEnabled(boolean fromWifiListGenerate){
        //split perm check to the above locationPermissionEnabledCheck, since some parts want to do the check without doing the later locationPermissionAddRemoveButtonsGetlist()
        //this should be cleaned up and removed later, this method is useless now but u g0t n0 time

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "locationPermissionEnabled(fromWifiListGenerate: " + fromWifiListGenerate + ")");
        }
        locationPermissionAddRemoveButtonsGetlist(locationPermissionEnabledCheck(fromWifiListGenerate), fromWifiListGenerate);

    }

    private void locationPermissionAddRemoveButtonsGetlist(boolean permissionGranted, boolean fromWifiListGenerate){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "locationPermissionAddRemoveButtonsGetlist(permissionGranted: " + permissionGranted + ", fromWifiListGenerate: " + fromWifiListGenerate + ")");
        }
        if (!permissionGranted || !locServicesEnabled){
            //also add a check for location services, and then a way to turn it on https://stackoverflow.com/a/10311891
            noWIFIDevicesFoundText.setVisibility(View.GONE);
            //locPermissionReasonsURLText.setVisibility(View.VISIBLE);
            android81WifiLocPermission.setVisibility(View.VISIBLE);
            requestPermissionButton.setVisibility(View.VISIBLE);
            addWIFIbutton.setVisibility(View.GONE);
            refreshWIFIbutton.setVisibility(View.GONE);

        } else {
            noWIFIDevicesFoundText.setVisibility(View.VISIBLE);
            //locPermissionReasonsURLText.setVisibility(View.GONE);
            android81WifiLocPermission.setVisibility(View.GONE);
            requestPermissionButton.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= 29) {
                addWIFIbutton.setVisibility(View.VISIBLE);
                refreshWIFIbutton.setVisibility(View.VISIBLE);
            }
            
            try {
                if (!fromWifiListGenerate) {

                    if (Build.VERSION.SDK_INT >= 29) {
                        generateWIFIDeviceListQ(null, false);

                    } else {
                        generateWIFIDeviceList();
                    }
                } else{
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "locationPermissionAddRemoveButtonsGetlist called from wifi list generate. avoided recursively calling it because of your legacy spaghetti code!");
                    }
                }
            } catch (Exception e) {

                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "fail wifi list generate", e);
                }
                
                
                Toast.makeText(wifiSafeListPickerActivity.this,
                        "Can't get WIFI list",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (permissions.length == 1 &&
                    Objects.equals(permissions[0], Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "begin wifi list generate after getting fine loc perm");
                    }
                    if (Build.VERSION.SDK_INT >= 29) {
                        getBackgroundLocPermissions();
                    } else {
                        android81WifiLocPermission.setVisibility(View.GONE);
                        requestPermissionButton.setVisibility(View.GONE);
                        generateWIFIDeviceList();
                    }

                } catch (Exception e) {
                    
                    
                    Toast.makeText(wifiSafeListPickerActivity.this,
                            "Can't get WIFI list",
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "permissions result: not granted");
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //actionbar's "back" should not kill activities.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void getBackgroundLocPermissions() {//f my life. I didn't fully get when the show/not show rationale thing happens, because Google.
        if (Build.VERSION.SDK_INT >= 29){
            //if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getBackgroundLocPermissions() - should show rationale");
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(wifiSafeListPickerActivity.this, R.style.AppTheme));
                    builder.setTitle(this.getApplicationContext().getResources().getString(R.string.wifi_perm_title));
                    builder.setMessage(this.getApplicationContext().getResources().getString(R.string.wifi_perm_explainer));
                            //"Background WIFI detection needs background location permission to know when you are on your chosen SSID.");
                    //builder.setPositiveButton(android.R.string.ok, null);
                    //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, LOCATION_PERMISSION_REQUEST);
                    builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {

                        dialog.dismiss();
                        try {
                            dialog.dismiss();
                        } catch (Exception e){
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "getBackgroundLocPermissions() - failed to pre-dismiss when user hit OK");
                            }
                        }
                        ActivityCompat.requestPermissions(wifiSafeListPickerActivity.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, LOCATION_PERMISSION_REQUEST);

                    });
                    builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                        dialog.cancel();
                        if (ContextCompat.checkSelfPermission(wifiSafeListPickerActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            finish();
                        }
                    });
//                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            ActivityCompat.requestPermissions(wifiSafeListPickerActivity.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, LOCATION_PERMISSION_REQUEST);
//                        }
//                    });

                    bgPermsDialog = builder.create();
                    bgPermsDialog.show();
                    builder.show();
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getBackgroundLocPermissions() - do not show rationale");
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(wifiSafeListPickerActivity.this, R.style.AppTheme));
                    builder.setTitle(this.getApplicationContext().getResources().getString(R.string.wifi_perm_title));
                    builder.setMessage(this.getApplicationContext().getResources().getString(R.string.wifi_perm_explainer));
                    builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
//                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                            Uri uri = Uri.fromParts("package", "com.merrycachemiss.rl", null);
//                            intent.setData(uri);
//                            startActivity(intent);

                        dialog.dismiss();
                        try {
                            dialog.dismiss();
                        } catch (Exception e){
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "getBackgroundLocPermissions() - failed to pre-dismiss when user hit OK");
                            }
                        }
                        ActivityCompat.requestPermissions(wifiSafeListPickerActivity.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, LOCATION_PERMISSION_REQUEST);

                    });
                    builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                        dialog.cancel();
                        if (ContextCompat.checkSelfPermission(wifiSafeListPickerActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            finish();
                        }
                    });

                    bgPermsDialog = builder.create();
                    bgPermsDialog.show();
                }
            } else{
                
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "bg permission was granted");
                }
                android81WifiLocPermission.setVisibility(View.GONE);
                requestPermissionButton.setVisibility(View.GONE);
                generateWIFIDeviceListQ(null, false);
                addWIFIbutton.setVisibility(View.VISIBLE);
                refreshWIFIbutton.setVisibility(View.VISIBLE);
            }
//            } else {
//                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
//                    requestPermissions(
//                            arrayOf(
//                                    Manifest.permission.ACCESS_FINE_LOCATION
//                                    /*Manifest.permission.ACCESS_BACKGROUND_LOCATION*/
//                            ),
//                            PERMISSION_REQUEST_FINE_LOCATION
//                    );
//                } else {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(wifiSafeListPickerActivity.this, R.style.AppTheme));
//                    builder.setTitle("Functionality limited");
//                    builder.setMessage("To function properly, this feature requires background location access at all times. Go to Settings >> Applications >> Permissions to give background location access permissions to the app at all times.");
//                    builder.setPositiveButton(android.R.string.ok, null);
//                    builder.setOnDismissListener {
//                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        val uri:Uri = Uri.fromParts("package", packageName, null);
//                        intent.data = uri;
//                        startActivity(intent);
//                    }
//                    builder.show();
//                }
//            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.requestPermissionButton) {
            //Q TARGET STUFF:
            if (Build.VERSION.SDK_INT == 29) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.ACCESS_BACKGROUND_LOCATION//for Q target
                }, LOCATION_PERMISSION_REQUEST);
            } else if (Build.VERSION.SDK_INT >= 30) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_PERMISSION_REQUEST);
            }
        }
        else if (v.getId() == R.id.turnOnLocServicesButton) {
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(myIntent);
        }
        else if (v.getId() == R.id.addWIFIbutton) {
            showAlertDialog();
        }
        else if (v.getId() == R.id.refreshWIFIbutton) {
            if (SystemClock.elapsedRealtime() > lastRefresh + 15000) {

                lastRefresh = SystemClock.elapsedRealtime();
                Log.d(TAG, "wifi scan triggered via button");
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "showing snackbar for wifi refresh, then start scan");
                }
                try {
                    try {
                        CoordinatorLayout coordinatorLayout = findViewById(R.id.wifiSafeListPickerActivityCoordinatorLayout);
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, R.string.refreshing_wifi_connections, 2500);
                        snackbar.show();
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to show snackbar for wifi refresh");
                        }
                        
                    }
                    wifiManager.startScan();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed wifi scan", e);
                    }
                    
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "wifi already scanned within 15 sec ago");
                }
                try {
                    CoordinatorLayout coordinatorLayout = findViewById(R.id.wifiSafeListPickerActivityCoordinatorLayout);
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, R.string.refreshing_wifi_connections_too_soon, 4500);
                    snackbar.show();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to show snackbar for wifi refresh");
                    }
                    
                }
            }

        }
    }


    private void showAlertDialog(){
        try {
        //https://stackoverflow.com/questions/10903754/input-text-dialog-android
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(wifiSafeListPickerActivity.this, R.style.AppTheme));
        builder.setTitle("Manually add an SSID");
        View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.ssid_input_alertdialog, null);
        final EditText input = viewInflated.findViewById(R.id.ssidInput);
        builder.setView(viewInflated);


// Set up the buttons
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            if (!TextUtils.isEmpty(input.getText().toString())) {
                iqd.safeWIFIInsert(input.getText().toString());
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "inserted " + input.getText().toString());
                }
            } else {
                Toast.makeText(wifiSafeListPickerActivity.this,
                        "Empty input",
                        Toast.LENGTH_SHORT).show();
            }
            //refresh whole list to show new items, re-use previously scanned and filtered list to tack onto end again
            if (filteredScanResultsDistinct != null) {
                if (!filteredScanResultsDistinct.isEmpty()) {
                    generateWIFIDeviceListQ(filteredScanResultsDistinct, false);
                } else {
                    generateWIFIDeviceListQ(null, false);
                }
            } else {
                generateWIFIDeviceListQ(null, false);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
            addSSIDdialog = builder.create();
            addSSIDdialog.show();
            //builder.show();
        } catch (Exception e){
            
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to show popup", e);
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onpause");
        }
        auth.adjustTimeoutForPresence(true);
        if (bgPermsDialog != null) {
            bgPermsDialog.dismiss();
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "bgPermsDialog was dismissed in onpause");
            }
        }
        if (Build.VERSION.SDK_INT >= 29) {
            try {
                this.unregisterReceiver(wifiScanReceiver);
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "failed to unregister wifiscanreceiver in onpause", e);
                }
            }
        }
        try {
            setCurrentWIFI(this);
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to store current wifi network ssid on onPause()", e);
            }

        }
    }


    private void setCurrentWIFI(Context context) {
        //store wifi info in case they turned on the constant PIN activation BEFORE giving the app loc permission and choosing a network,
        //thus it didn't know what network it is currently on when trying to set it up.
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
        getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(context, TAG);
        //set current ssid info, but only after comparison of before vs after network change.
        wifiSSIDGetSet.setCurrentWIFISSID(iqd, wifiSSIDGetSet.getCurrentWIFISSID());

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onStop");
        }
        if (bgPermsDialog != null) {
            bgPermsDialog.dismiss();
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "bgPermsDialog was dismissed in onStop");
            }
        }

    }



        @Override
    protected void onDestroy() {
        if (addSSIDdialog != null) {
            addSSIDdialog.dismiss();
            addSSIDdialog = null;
        }
        if (bgPermsDialog != null) {
            bgPermsDialog.dismiss();
            bgPermsDialog = null;
        }
        super.onDestroy();

    }
}
