/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.dataStorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public final class shPrefsGetterSetter {
    private static final String TAG = "shPrefsGetterSetter";
    private SharedPreferences sharedPreferences;

    private final String SHAREDPREFS_FILENAME = "WDL_SHAREPREFS_FILE";
    private Context storageContext;

    public shPrefsGetterSetter(Context context){

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            try{
                // All N devices have split storage areas, but we may need to
                // move the existing preferences to the new device protected
                // storage area, which is where the data lives from now on.
                final Context deviceContext = context.createDeviceProtectedStorageContext();

                storageContext = deviceContext;//used in the rest of the activity

                int deviceProtectedStorageIsAlreadyMigrated = 0;
                sharedPreferences = storageContext.getSharedPreferences(SHAREDPREFS_FILENAME, Context.MODE_PRIVATE);
                try{
                    deviceProtectedStorageIsAlreadyMigrated = getSharedPrefsInt("storage_migrated_direct_boot");
                } catch (Exception e){
                    Log.d(TAG, "sharedprefs: failed to get value storage_migrated_direct_boot to see if migrated before");
                }
                if (deviceProtectedStorageIsAlreadyMigrated == 0) {
                    try {
                        deviceContext.moveSharedPreferencesFrom(context, SHAREDPREFS_FILENAME);
                        //deviceContext.moveDatabaseFrom(context, DATABASE_NAME);
                        Log.d(TAG, "sharedprefs now migrated to device protected storage");
                        //deviceProtectedStorageIsAlreadyMigrated = 1;
                        setSharedPrefsInt("storage_migrated_direct_boot", 1);
                    } catch (Exception e) {
                        Log.d(TAG, "Failed to migrate shared preferencesto device protected storage. Possibly first boot with it.");
                    }
                } else{
                    Log.d(TAG, "sharedprefs storage was ALREADY migrated to device protected storage");
                    setSharedPrefsInt("storage_migrated_direct_boot", 1);
                }
            } catch (Exception e){//migration failed, so just use pre-N method
                Log.d(TAG, "FAILED MIGRATION to device protected storage. Default method is used.");
                storageContext = context;
                sharedPreferences = storageContext.getSharedPreferences(SHAREDPREFS_FILENAME, Context.MODE_PRIVATE);
            }

        } else {
            storageContext = context;
            sharedPreferences = storageContext.getSharedPreferences(SHAREDPREFS_FILENAME, Context.MODE_PRIVATE);
        }

    }

    //set ints
    public void setSharedPrefsInt(String property, int value) {
        SharedPreferences.Editor editor = storageContext.getSharedPreferences(SHAREDPREFS_FILENAME, Context.MODE_PRIVATE).edit();
        editor.putInt(property, value);
        editor.apply();
    }
    //get ints
    public int getSharedPrefsInt(String property) {
        try {
            return sharedPreferences.getInt(property, 0);
        } catch (Exception e){
            Log.e(TAG, " value " + property + " doesn't exist in sharedprefs.", e);

            return 0;
        }
    }


}
