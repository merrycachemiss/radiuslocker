/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Modifications Copyright © 2024 merry cache miss technologies, Inc.
 */

package com.merrycachemiss.rl.dataStorage;


import android.provider.BaseColumns;

interface rldbSchema {

    String DB_NAME = "rldb.db";

    String TBL_MONITORED_BT = "monitored_bt";
    String TBL_SAFE_BT = "safe_bt";
    String TBL_CONNECTED_BT = "connected_bt";
    String TBL_SAFE_WIFI = "safe_wifi";
    String TBL_SAFE_WIFI_MAC = "safe_wifi_mac";
    String TBL_SETTINGS_TEMP = "settings_temp";

    String COL_ID = BaseColumns._ID;
    String COL_SERIAL = "serial";
    String COL_NAME = "name";
    String COL_CONNECTED = "connected";
    String COL_UPDT_DT_TM = "updt_dt_tm";
    String COL_SSID = "ssid";
    String COL_MAC = "mac";
    String COL_SNOOZER = "snoozer";
    String COL_SETTING_TEMPVAL_NAME = "setting_tempval_name";
    String COL_STRING_VAL = "string_val";
    String COL_LONG_VAL = "long_val";

    String DDL_CREATE_TBL_MONITORED_BT =
            "CREATE TABLE IF NOT EXISTS " + TBL_MONITORED_BT + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SERIAL + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE, " +
                    COL_NAME + " TEXT " +
            ")";

    String DDL_CREATE_TBL_SAFE_BT =
            "CREATE TABLE IF NOT EXISTS " + TBL_SAFE_BT + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SERIAL + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE, " +
                    COL_NAME + " TEXT " +
            ")";

    String DDL_CREATE_TBL_CONNECTED_BT =
            "CREATE TABLE IF NOT EXISTS " + TBL_CONNECTED_BT + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SERIAL + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE, " +
                    COL_CONNECTED + " INTEGER NOT NULL, " +
                    COL_UPDT_DT_TM + " LONG " +
            ")";

    String DDL_CREATE_TBL_SAFE_WIFI =
            "CREATE TABLE IF NOT EXISTS " + TBL_SAFE_WIFI + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SSID + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE" +
            ")";

    String DDL_CREATE_TBL_SAFE_WIFI_MAC =
            "CREATE TABLE IF NOT EXISTS " + TBL_SAFE_WIFI_MAC + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SSID + " TEXT, " +
                    COL_MAC + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE, " +
                    COL_SNOOZER + " INTEGER NOT NULL" +
            ")";

    String DDL_CREATE_TBL_SETTINGS_TEMP =
            "CREATE TABLE IF NOT EXISTS " + TBL_SETTINGS_TEMP + "(" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_SETTING_TEMPVAL_NAME + " INT NOT NULL UNIQUE ON CONFLICT REPLACE, " +
                    COL_STRING_VAL + " TEXT DEFAULT '', " +
                    COL_LONG_VAL + " LONG DEFAULT 0" +
            ")";

    String DML_WHERE_ID_CLAUSE = "_id = ?";



    /*ALL QUERIES WITH FIXED ARGUMENTS LIVE HERE*/

    //is on ANY safe BT:
    String JOIN_CONNECTED_SAFE_BT_STATEMENT = TBL_CONNECTED_BT + " JOIN " + TBL_SAFE_BT + " ON " +
            TBL_CONNECTED_BT + "." + COL_SERIAL + " = " + TBL_SAFE_BT + "." + COL_SERIAL
            + " WHERE (" + TBL_CONNECTED_BT + "." + COL_CONNECTED + " = 1) LIMIT 1";

    //is on ANY monitored BT:
    String JOIN_CONNECTED_MONITORED_BT_STATEMENT = TBL_CONNECTED_BT + " JOIN " + TBL_MONITORED_BT + " ON " +
            TBL_CONNECTED_BT + "." + COL_SERIAL + " = " + TBL_MONITORED_BT + "." + COL_SERIAL
            + " WHERE (" + TBL_CONNECTED_BT + "." + COL_CONNECTED + " = 1) LIMIT 1";//remove the limit for multi-watch monitoring?

    //is on ANY monitored BT:
    String JOIN_ALL_CONNECTED_MONITORED_BT_STATEMENT = TBL_CONNECTED_BT + " JOIN " + TBL_MONITORED_BT + " ON " +
            TBL_CONNECTED_BT + "." + COL_SERIAL + " = " + TBL_MONITORED_BT + "." + COL_SERIAL
            + " WHERE (" + TBL_CONNECTED_BT + "." + COL_CONNECTED + " = 1)";

//    String JOIN_DISCONNECTED_MONITORED_BT_STATEMENT = TBL_CONNECTED_BT + " JOIN " + TBL_MONITORED_BT + " ON " +
//            TBL_CONNECTED_BT + "." + COL_SERIAL + " = " + TBL_MONITORED_BT + "." + COL_SERIAL
//            + " WHERE " + TBL_CONNECTED_BT + "." + COL_CONNECTED + " =0";

    String HAS_MONITORED_BT_STORED_SELECT_STATEMENT = TBL_MONITORED_BT + " LIMIT 1";

    String HAS_SAFE_BT_STORED_SELECT_STATEMENT = TBL_SAFE_BT + " LIMIT 1";

    String HAS_SAFE_WIFI_STORED_SELECT_STATEMENT = TBL_SAFE_WIFI + " LIMIT 1";


    String HAS_SAFE_WIFI_MAC_STORED_SELECT_STATEMENT = TBL_SAFE_WIFI_MAC + " LIMIT 1";

    //String ALL_SETTINGS_TEMP_VAR_VALUES_STATEMENT = "SELECT * FROM " + TBL_SETTINGS_TEMP;

    String INSERT_MONITORED_BT_STATEMENT = "INSERT INTO " + TBL_MONITORED_BT + " (" + COL_SERIAL + ", " + COL_NAME + ") VALUES (?, ?)";
    String INSERT_SAFE_BT_STATEMENT = "INSERT INTO " + TBL_SAFE_BT + " (" + COL_SERIAL + ", " + COL_NAME + ") VALUES (?, ?)";
    String INSERT_CONNECTED_BT_STATEMENT = "INSERT INTO " + TBL_CONNECTED_BT + " (" + COL_SERIAL + ", " + COL_CONNECTED + ", " + COL_UPDT_DT_TM + ") VALUES (?, ?, ?)";
    String INSERT_SAFE_WIFI_STATEMENT = "INSERT INTO " + TBL_SAFE_WIFI + " (" + COL_SSID + ") VALUES (?)";
    String INSERT_SETTINGS_TEMP_STATEMENT = "INSERT INTO " + TBL_SETTINGS_TEMP + " (" + COL_SETTING_TEMPVAL_NAME + ", " + COL_STRING_VAL + ", " + COL_LONG_VAL + ") VALUES (?, ?, ?)";

    //String DELETE_ROW_FROM_TABLE = "DELETE FROM ? WHERE ? =?";



    //String DEFAULT_TBL_ITEMS_SORT_ORDER = "name ASC";

   // String LEFT_OUTER_JOIN_STATEMENT = TBL_ITEMS + " LEFT OUTER JOIN " + TBL_PHOTOS + " ON(items._id = photos.items_id)";


}

