/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.dataStorage;

public final class dataConstants {

    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */


    public final static int CRASH_REPORTING_ENABLED = 1;

    public final static int PHONE_LOCKING_ENABLED = 3;

    public final static int DONT_LOCK_TWICE_WITHIN = 4;

    public final static int DONT_LOCK_IF_MAPS_OPEN = 5;

    public final static int DONT_LOCK_IF_CHARGING = 6;

    public final static int SNOOZE_STOP_TIME = 7;

    public final static int BT_HAS_BEEN_REFRESHED_BEFORE = 8;

    public final static int LOCK_IF_BT_OFF = 9;

    public final static int DEVICE_ID = 11;

    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */

    public final static int DONT_LOCK_IF_DRIVING = 12;

    public final static int USER_IS_DRIVING = 13;

    public final static int POST_SNOOZE_LOCK_CHECK = 14;

    public final static int RELOCK_JOB_SCHEDULED = 15;

    public final static int DRIVING_CHECK_COOLDOWN_FINISH_TIME = 16;

    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */

    //public final static int DRIVING_LAST_CHECK_TIME = 17;

    public final static int LOCK_IF_SIM_REMOVED = 18;

    public final static int REQUIRE_BIOMETRIC_AUTH_USE_APP = 20;

    public final static int REAUTH_TRIGGER_TIME = 21;

    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */

    public final static int TASKER_TASK = 22;

    public final static int TASKER_JOB_DELAY = 24;

    public final static int TASKER_JOB_SCHEDULED = 25;



    //these are IDs for various scheduled jobs
    public final static int RELOCK_JOB_ID = 26;

    public final static int PURCHASE_VERIF_JOB_ID = 27;

    public final static int TASKER_TASK_JOB_ID = 28;


    /*
    *
    * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
    *
    *
    * */



    public final static int DELAY_BEFORE_LOCK = 29;

    public final static int DELAY_BEFORE_LOCK_EXPIRY = 30;


    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */


    public final static int BEDTIME_PROTECTION_SETTING = 42;

    public final static int CONSTANTLY_REQUIRE_PIN_UPON_LOCKING_WATCH_DISCONNECT_SETTING = 43;

    //for tracking if a watch disconnected, and if some other part (job, service, etc) needs to know this.
    public final static int CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED = 44;

    public final static int CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID = 45;

    public final static int CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID = 46;

    //new cancel button behaviour: if they hit cancel, any jobs won't do anything until the next alarm time mismatches this stored one.
    //so if they hit cancel tonight, the trigger job check if the times match (this stored var vs next alarm time)
    //if match, then this night is to be skipped
    public final static int CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP = 47;

    public final static int CONSTANT_REQUIRE_PIN_PAUSE_TIME_END = 48;

    //ON HEARTBEAT START: SET THIS TIME +15 MIN INTO FUTURE. IF < THAN THAT TIME, THEN THE TRIGGER JOB WON'T TRY TO RUN THE SERVICE.
    //ON HEARTBEAT END, CONSTANT PIN END, OR ON DEVICE RESTART: SET THIS TIME TO 0.
    public final static int CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END = 49;

    //for when a user wants to force constant PIN mode through a quick tile or other means
    public final static int CONSTANT_REQUIRE_PIN_BY_CHOICE = 50;


    public final static int MULTI_DEVICE_MODE = 51;
    public final static int MULTI_DEVICE_MODE_QUEUE_LATER_LOCK = 52;


    public final static int USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP = 53;


    public final static int ONLY_USE_PIN_PASS_FOR_RL_AUTH = 54;


    public final static int BT_REFRESHER_QUEUED = 55;


    public final static int LOCK_IF_CABLE_REMOVED = 56;


    public final static int START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE = 57;

    public final static int LATEST_CONNECTED_WIFI_SSID = 58;


    /*
     *
     * THESE EXISTING VALUES CAN ----NEVER---- BE CHANGED TO ANOTHER NUMBER
     *
     *
     * */



    public final static int USE_WEAR_APIS = 59;


    public final static int APP_VERSION = 60;


    public final static int CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID = 61;


    private dataConstants(){

    }
}
