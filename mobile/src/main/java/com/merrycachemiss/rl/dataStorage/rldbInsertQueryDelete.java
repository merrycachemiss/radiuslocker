/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.dataStorage;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;


import java.util.ArrayList;
import java.util.Arrays;


public final class rldbInsertQueryDelete {

    private static final String TAG = "settingsTempInsDel";
    private final ContentResolver db;
    private Uri dbURI;
    public rldbInsertQueryDelete(Context context){
        db = context.getContentResolver();
    }

    public boolean genericSingleItemExistsQuery(Uri URI, String[] projection, String[] selectionArgs){
        boolean exists = false;
        try {
            Cursor c = db.query(
                    URI,
                    projection,
                    null,
                    selectionArgs,
                    null);

            if (c != null) {
                if (c.getCount() != 0) {
                    c.moveToFirst();
                    exists = true;
                    String item = c.getString(0);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "genericSingleItemExistsQuery: queried " + URI + " and found value: " + item + " with count " + c.getCount());
                    }

                }
                c.close();
            }
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "genericSingleItemExistsQuery: FAILED to query " + URI);//after rebooting content provider");
                }
                

            }
        //}
        return exists;
    }

    public boolean isConnectedBTDeviceQuery(Uri URI, String[] selectionArgs){
        boolean exists = false;
        try {
            Cursor c = db.query(
                    URI,
                    rldbContract.ConnectedBTQuery.SERIAL_PROJECTION,
                    null,
                    selectionArgs,
                    null);

            if (c != null) {
                if (c.getCount() != 0) {
                    c.moveToFirst();
                    exists = true;
                    String item = c.getString(0);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "isConnectedBTDeviceQuery: queried " + URI + " and found value: " + item);
                    }

                }
                c.close();
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "isConnectedBTDeviceQuery: FAILED to query " + URI);
            }
            

        }
        //}
        return exists;
    }
    public long connectedBTDeviceQueryUpdateTime(Uri URI, String[] selectionArgs){
        long time = 0;
        try {
            Cursor c = db.query(
                    URI,
                    rldbContract.ConnectedBTQuery.PROJECTION_UPDT_DT_TM,
                    null,
                    selectionArgs,
                    null);

            if (c != null) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "connectedBTDeviceQueryUpdateTime c.getCount(): " + c.getCount() + " c.getColumnCount() " + c.getColumnCount() + " c.getColumnNames(): " + Arrays.toString(c.getColumnNames()));
                }
                if (c.getCount() != 0) {
                    c.moveToFirst();
                    time = c.getLong(0);

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "connectedBTDeviceQueryUpdateTime: queried " + URI + " and found values: " + c.getLong(0));
                    }

                }
                c.close();
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "connectedBTDeviceQueryUpdateTime: FAILED to query " + URI, e);
            }
        }
        //}
        return time;
    }

//    public Pair<Integer, Integer> connectedBTDeviceQueryStats(Uri URI, String[] selectionArgs){
//        Pair<Integer, Integer> pair = new Pair<>(0, 0);
//        try {
//            Cursor c = db.query(
//                    URI,
//                    rldbContract.ConnectedBTQuery.PROJECTION_SERIAL_CONNECTEDSTATE_CONNECTUPDATETIME,
//                    null,
//                    selectionArgs,
//                    null);
//
//            if (c != null) {
//                if (c.getCount() != 0) {
//                    c.moveToFirst();
//                    pair = new Pair<>(c.getInt(1), c.getInt(2));
//                    if (BuildConfig.DEBUG) {
//                        Log.d(TAG, "connectedBTDeviceQueryStats: queried " + URI + " and found values: " + c.getInt(0) +
//                                " and " + c.getInt(1) + " and " + c.getInt(2));
//                    }
//
//                }
//                c.close();
//            }
//        } catch (Exception e){
//            if (BuildConfig.DEBUG) {
//                Log.d(TAG, "connectedBTDeviceQueryStats: FAILED to query " + URI);
//            }
//        }
//        //}
//        return pair;
//    }


    public void BTConnStatusInsert(String serial, int connected){
        Uri btURI = rldbContract.ConnectedBTQuery.CONTENT_URI;
        ContentValues dbValues = new ContentValues();
        dbValues.put(rldbContract.ConnectedBTQuery.SERIAL, serial);
        dbValues.put(rldbContract.ConnectedBTQuery.CONNECTED, connected);
        if (connected == 1) {
            dbValues.put(rldbContract.ConnectedBTQuery.UPDT_DT_TM, SystemClock.elapsedRealtime());
        } else {
            dbValues.put(rldbContract.ConnectedBTQuery.UPDT_DT_TM, 0);//we actually don't care about a disconnected device's update time. Only care about connected time.
        }
        //^ and v changed with 202410 update, due to replacement of delayed lock process with another thing
        //dbValues.put(rldbContract.ConnectedBTQuery.UPDT_DT_TM, System.currentTimeMillis())

        //
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "BTConnStatusInsert: try to insert " + btURI + " into db");
        }
        try {
        dbURI = db.insert(btURI, dbValues);
        } catch (Exception e) {
            Log.d(TAG, "BTConnStatusInsert: FAILED to insert " + btURI + " into db");
            
        }
        dbValues.clear();
    }
    public void BTConnStatusDelete(String serial){
        Uri btURI = rldbContract.ConnectedBTQuery.CONTENT_URI;
        final String[] btDeviceDeleted = {serial};
        
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "BTConnStatusDelete: try to delete " + btURI + " from db");
        }

        try {
            db.delete(btURI, null, btDeviceDeleted);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "BTConnStatusDelete: FAILED to delete " + btURI + " from db");
                }
                
        }
    }
    public void updateAllBTConnStatusToDisconn() {
        Uri btURI = rldbContract.ConnectedBTQuery.CONTENT_URI;
        ContentValues vals = new ContentValues();
        vals.put(rldbContract.ConnectedBTQuery.CONNECTED, 0);
        vals.put(rldbContract.ConnectedBTQuery.UPDT_DT_TM, 0);//System.currentTimeMillis());
        String where = rldbContract.ConnectedBTQuery.CONNECTED + "= ?";
        final String[] selectArgs = {"1"};
        try {
            
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "resetting connected device states to 0");
            }
            db.update(btURI, vals, where, selectArgs);
            vals.clear();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "updateAllBTConnStatusToDisconn: FAILED to update " + btURI + " in db");
                }
                
            }
       // }
    }

    public void updateAllBTConnStatusTimesOnceForFirstTransitionAwayFromAlarmmanager() {
        Uri btURI = rldbContract.ConnectedBTQuery.CONTENT_URI;
        ContentValues vals = new ContentValues();
        vals.put(rldbContract.ConnectedBTQuery.UPDT_DT_TM, 0);//System.currentTimeMillis());
        String where = rldbContract.ConnectedBTQuery.CONNECTED + "= ?";
        final String[] selectArgs = {"1"};
        try {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "updateAllBTConnStatusTimesOnceForFirstTransitionAwayFromAlarmmanager resetting connected device times to 0");
            }
            db.update(btURI, vals, where, selectArgs);
            vals.clear();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "updateAllBTConnStatusTimesOnceForFirstTransitionAwayFromAlarmmanager: FAILED to update " + btURI + " in db");
            }

        }
        // }
    }


    public void monitoredSafeBTInsert(Uri btURI, String serial, String name){
        ContentValues dbValues = new ContentValues();
        dbValues.put(rldbContract.MonitoredBTQuery.SERIAL, serial);
        dbValues.put(rldbContract.MonitoredBTQuery.NAME, name);

        
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "monitoredSafeBTInsert: try to insert " + btURI + " into db");
        }

        try {
            dbURI = db.insert(btURI, dbValues);

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "monitoredSafeBTInsert: FAILED to insert " + btURI + " into db");
                }
                
            }
        //}
        dbValues.clear();
    }

    public void monitoredSafeBTDelete(Uri btURI, String serial){
        final String[] btDeviceDeleted = {serial};
        try {
            db.delete(btURI, null, btDeviceDeleted);

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "monitoredSafeBTDelete: FAILED to delete " + btURI + " into db");
                }
                
            }
//        }
    }

    public ArrayList<String> getAllSafeWifiAsList(){

        ArrayList<String> chosenWifiList = null;
        try {
            Cursor c = db.query(
                    rldbContract.AllSafeWIFIQuery.CONTENT_URI,
                    rldbContract.AllSafeWIFIQuery.PROJECTION_SSID,
                    null,
                    null,
                    rldbContract.AllSafeWIFIQuery.SORT_ORDER_DEFAULT);

            if (c != null) {
                if (c.getCount() != 0) {
                    chosenWifiList = new ArrayList<>(c.getCount());
                    while (c.moveToNext()) {
                        chosenWifiList.add(c.getString(0));
                        String item = c.getString(0);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "genericSingleItemExistsQuery: queried " + rldbContract.AllSafeWIFIQuery.CONTENT_URI + " and found value: " + item);
                        }
                    }

                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "genericSingleItemExistsQuery: queried " + rldbContract.AllSafeWIFIQuery.CONTENT_URI + " and found NO values");
                    }
                }
                c.close();
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "genericSingleItemExistsQuery: FAILED to get " + rldbContract.AllSafeWIFIQuery.CONTENT_URI + " from db", e);
            }
            

        }
        //}
        return chosenWifiList;
    }


    public ArrayList<String> getAllMonitoredDevices(){
        ArrayList<String> monitoredDevices = null;
        try {
            Cursor c = db.query(
                    rldbContract.AllMonitoredBTQuery.CONTENT_URI,
                    rldbContract.AllMonitoredBTQuery.PROJECTION_SERIAL,
                    null,
                    null,
                    rldbContract.AllMonitoredBTQuery.SORT_ORDER_DEFAULT);

            if (c != null) {
                if (c.getCount() != 0) {
                    monitoredDevices = new ArrayList<>(c.getCount());
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getAllMonitoredDevices(): count " + c.getCount());
                    }
                    while (c.moveToNext()) {
                        monitoredDevices.add(c.getString(0));
                        String item = c.getString(0);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "getAllMonitoredDevices: queried " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " and found value: " + item);
                        }
                    }

                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getAllMonitoredDevices(): queried " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " and found NO values");
                    }
                }
                c.close();
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "getAllMonitoredDevices(): FAILED to get " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " from db", e);
            }


        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getAllMonitoredDevices(): " + monitoredDevices);
        }

        //}
        return monitoredDevices;
    }

//    public ArrayList<Pair<String, Boolean>> getAllMonitoredDevicesAsPairs(){
//        ArrayList<Pair<String, Boolean>> monitoredDevices = null;
//        try {
//            Cursor c = db.query(
//                    rldbContract.AllMonitoredBTQuery.CONTENT_URI,
//                    rldbContract.AllMonitoredBTQuery.PROJECTION_SERIAL,
//                    null,
//                    null,
//                    rldbContract.AllMonitoredBTQuery.SORT_ORDER_DEFAULT);
//
//            if (c != null) {
//                if (c.getCount() != 0) {
//                    monitoredDevices = new ArrayList<>(c.getCount());
//                    if (BuildConfig.DEBUG) {
//                        Log.d(TAG, "getAllMonitoredDevicesAsPairs(): count " + c.getCount());
//                    }
//                    while (c.moveToNext()) {
//                        monitoredDevices.add(new Pair(c.getString(0), false));
//                        if (BuildConfig.DEBUG) {
//                            String item = c.getString(0);
//                            Log.d(TAG, "getAllMonitoredDevicesAsPairs: queried " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " and found value: " + item);
//                        }
//                    }
//
//                } else {
//                    if (BuildConfig.DEBUG) {
//                        Log.d(TAG, "getAllMonitoredDevicesAsPairs(): queried " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " and found NO values");
//                    }
//                }
//                c.close();
//            }
//        } catch (Exception e){
//            if (BuildConfig.DEBUG) {
//                Log.e(TAG, "getAllMonitoredDevicesAsPairs(): FAILED to get " + rldbContract.AllMonitoredBTQuery.CONTENT_URI + " from db", e);
//            }
//
//
//        }
//
//        if (BuildConfig.DEBUG) {
//            Log.d(TAG, "getAllMonitoredDevicesAsPairs(): " + monitoredDevices);
//        }
//
//        //}
//        return monitoredDevices;
//    }

    public ArrayList<String> getAllConnectedMonitoredDevices(){
        ArrayList<String> connectedMonitoredDevices = null;
        try {
            Cursor c = db.query(
                    rldbContract.AllConnectedMonitoredBTQuery.CONTENT_URI,
                    rldbContract.AllConnectedMonitoredBTQuery.PROJECTION_SERIAL,
                    null,
                    null,
                    rldbContract.AllConnectedMonitoredBTQuery.SORT_ORDER_DEFAULT);

            if (c != null) {
                if (c.getCount() != 0) {
                    connectedMonitoredDevices = new ArrayList<>(c.getCount());
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getAllConnectedMonitoredDevices(): count " + c.getCount());
                    }
                    while (c.moveToNext()) {
                        connectedMonitoredDevices.add(c.getString(0));
                        String item = c.getString(0);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "getAllConnectedMonitoredDevices: queried " + rldbContract.AllConnectedMonitoredBTQuery.CONTENT_URI + " and found value: " + item);
                        }
                    }

                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getAllConnectedMonitoredDevices(): queried " + rldbContract.AllConnectedMonitoredBTQuery.CONTENT_URI + " and found NO values");
                    }
                }
                c.close();
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "getAllConnectedMonitoredDevices(): FAILED to get " + rldbContract.AllConnectedMonitoredBTQuery.CONTENT_URI + " from db", e);
            }


        }


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getAllConnectedMonitoredDevices(): " + connectedMonitoredDevices);
        }

        //}
        return connectedMonitoredDevices;
    }


    public void safeWIFIInsert(String ssid){
        ContentValues dbValues = new ContentValues();
        dbValues.put(rldbContract.SafeWIFIQuery.SSID, ssid);
        Uri URI = rldbContract.SafeWIFIQuery.CONTENT_URI;
        
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "safeWIFIInsert: try to insert " + URI + " into db");
        }

        try {
            dbURI = db.insert(URI, dbValues);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "safeWIFIInsert: FAILED to insert " + URI + " into db");
                }
                
            }
//        }
        dbValues.clear();
    }

    public void safeWIFIDelete(String ssid){
        final String[] wifiDeviceDeleted = {ssid};

        Uri URI = rldbContract.SafeWIFIQuery.CONTENT_URI;
        try {
            db.delete(URI, null, wifiDeviceDeleted);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "safeWIFIDelete: FAILED to delete " + URI + " into db");
                }
                
            }
//        }
    }


    public long getSettingsTempLongVal(int setting){
        //
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getSettingsTempLongVal: try to retrieve getSettingsTempLongVal " + setting);
        }
        long value = 0;
        final String[] selectionArgs = {String.valueOf(setting)};
        try {
            Cursor c = db.query(
                    rldbContract.SettingsTempValsQuery.CONTENT_URI,
                    rldbContract.SettingsTempValsQuery.SETTING_LONG_VAL_PROJECTION,
                    null,
                    selectionArgs,
                    null);


            if (c != null) {
                if (c.getCount() != 0) {
                    c.moveToFirst();
                    value = c.getLong(0);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, setting + " value: " + value + " count: " + c.getCount());
                    }
                    //
                }
                c.close();
            }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getSettingsTempLongVal: FAILED to retrieve getSettingsTempLongVal " + setting);
                }
                
            }
//        }
        return value;
    }

    public String getSettingsTempStringVal(int setting) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getSettingsTempStringVal: try to retrieve " + setting);
        }
        //
        String value = "";
        final String[] selectionArgs = {String.valueOf(setting)};

        try {
            Cursor c = db.query(
                    rldbContract.SettingsTempValsQuery.CONTENT_URI,
                    rldbContract.SettingsTempValsQuery.SETTING_STRING_VAL_PROJECTION,
                    null,
                    selectionArgs,
                    null);
            if (c != null) {
                if (c.getCount() != 0) {
                    c.moveToFirst();
                    value = c.getString(0);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, setting + " value: " + value + " count: " + c.getCount());
                    }
                }
                c.close();
            }
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getSettingsTempStringVal: FAILED to retrieve " + setting);
                }
                
            }
//        }

        return value;
    }

    public void setSettingsTempLongVal(int setting, long value){
        
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setSettingsTempLongVal: try to insert " + setting + " into db");
        }
        ContentValues dbValues = new ContentValues();
        dbValues.put(rldbContract.SettingsTempValsQuery.TEMPVAL_NAME, setting);
        dbValues.put(rldbContract.SettingsTempValsQuery.LONG_VAL, value);

        try {
            dbURI = db.insert(rldbContract.SettingsTempValsQuery.CONTENT_URI, dbValues);

            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "setSettingsTempLongVal: FAILED to insert " + setting + " into db");
                }
                
            }
//        }

        dbValues.clear();
    }

    public void setSettingsTempStringVal(int setting, String value){
        
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setSettingsTempStringVal: try to insert " + setting + " into db");
        }
        ContentValues dbValues = new ContentValues();
        dbValues.put(rldbContract.SettingsTempValsQuery.TEMPVAL_NAME, setting);
        dbValues.put(rldbContract.SettingsTempValsQuery.STRING_VAL, value);

        try {
            dbURI = db.insert(rldbContract.SettingsTempValsQuery.CONTENT_URI, dbValues);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "setSettingsTempStringVal: FAILED to insert " + setting + " into db");
                }
                
            }
//        }

        dbValues.clear();
    }




}
