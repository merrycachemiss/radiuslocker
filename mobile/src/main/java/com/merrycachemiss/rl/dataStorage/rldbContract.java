/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Modifications Copyright © 2024 merry cache miss technologies, Inc.
 */

package com.merrycachemiss.rl.dataStorage;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;


import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_CONNECTED;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_LONG_VAL;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_MAC;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_NAME;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_SERIAL;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_SETTING_TEMPVAL_NAME;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_SNOOZER;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_SSID;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_STRING_VAL;
import static com.merrycachemiss.rl.dataStorage.rldbSchema.COL_UPDT_DT_TM;


public final class rldbContract {


    static final String AUTHORITY = "com.merrycachemiss.rl.rldb";
    /*
      The content URI for the top-level
      rldb authority.
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    /**
     * A selection clause for ID based queries.
     */
    public static final String SELECTION_ID_BASED = BaseColumns._ID + " = ? ";


    public static final class MonitoredBTQuery implements CommonColumns, SerialProjection, SerialSelections {
        /**
         * The content URI for this table.
         */
        public static final Uri CONTENT_URI =  Uri.withAppendedPath(rldbContract.CONTENT_URI, "monitored_bt");
        /**
         * The mime type of a directory of items.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_monitored_bt";
        /**
         * The mime type of a single item.
         */
        public static final String CONTENT_MONITORED_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_monitored_bt";

        public static final String[] PROJECTION_ALL = {_ID, SERIAL, NAME};

        public static final String[] PROJECTION_ID = {_ID};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SERIAL + " ASC";
    }

    public static final class SafeBTQuery implements CommonColumns, SerialProjection, SerialSelections {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "safe_bt");
        /**
         * The mime type of a directory of photos.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_safe_bt";
        /**
         * The mime type of a single photo.
         */
        public static final String CONTENT_SAFE_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_safe_bt";

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {_ID, SERIAL, NAME};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SERIAL + " ASC";
    }

    /**
     * Constants for a joined view of Items and Photos. The _id of this
     * joined view is the _id of the Items table.
     */
    public static final class ConnectedBTQuery implements CommonColumns, SerialProjection, SerialSelections {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "connected_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_connected_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_CONNECTED_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_connected_bt";


        public static final String UPDT_DT_TM = COL_UPDT_DT_TM;

        public static final String[] PROJECTION_UPDT_DT_TM = {COL_UPDT_DT_TM};

        public static final String CONNECTED_SERIAL_SELECTION = SERIAL + " = ? AND " + CONNECTED + " = ?";

        public static final String[] PROJECTION_SERIAL_CONNECTEDSTATE_CONNECTUPDATETIME= {SERIAL, CONNECTED, COL_UPDT_DT_TM};

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {_ID, SERIAL, CONNECTED, UPDT_DT_TM};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SERIAL + " ASC";
    }

    public static final class SafeWIFIQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "safe_wifi");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_safe_wifi";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_SAFE_WIFI_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_safe_wifi";

        public static final String SSID = COL_SSID;

        public static final String SSID_SELECTION = SSID + " = ?";
        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {_ID, SSID};
        public static final String[] PROJECTION_SSID = {SSID};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SSID + " ASC";
    }


    public static final class AllSafeWIFIQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "all_safe_wifi");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_all_safe_wifi";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_SAFE_WIFI_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_all_safe_wifi";

        public static final String SSID = COL_SSID;

        public static final String SSID_SELECTION = SSID + " = ?";


        public static final String[] PROJECTION_ALL = {_ID, SSID};
        public static final String[] PROJECTION_SSID = {SSID};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SSID + " COLLATE NOCASE ASC";
    }


    public static final class AllMonitoredBTQuery implements CommonColumns {
        /**
         * The content URI for this table.
         */
        public static final Uri CONTENT_URI =  Uri.withAppendedPath(rldbContract.CONTENT_URI, "all_monitored_bt");
        /**
         * The mime type of a directory of items.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_all_monitored_bt";
        /**
         * The mime type of a single item.
         */
        public static final String CONTENT_MONITORED_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_all_monitored_bt";
        /**
         * A projection of all columns in the items table.
         */
        //public static final String[] PROJECTION_ALL = {_ID, SERIAL, NAME};

        public static final String[] PROJECTION_SERIAL = {SERIAL};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SERIAL + " COLLATE NOCASE ASC";
    }




    public static final class SafeWIFIMACQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "safe_wifi_mac");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_safe_wifi_mac";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_SAFE_WIFI_MAC_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_safe_wifi_mac";

        public static final String SSID = COL_SSID;

        public static final String MAC = COL_MAC;

        static final String SNOOZER = COL_SNOOZER;

        public static final String MAC_SELECTION = MAC + " = ?";

        public static final String SSID_SELECTION = SSID + " = ?";

        public static final String SNOOZER_SELECTION = SNOOZER + " = ?";

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {_ID, SSID, MAC};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SSID + " ASC";
    }



    public static final class SettingsTempValsQuery implements BaseColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "settings_temp");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_settings_temp";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_SETTINGS_TEMP_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_settings_temp";

        public static final String TEMPVAL_NAME = COL_SETTING_TEMPVAL_NAME;

        public static final String STRING_VAL = COL_STRING_VAL;

        public static final String LONG_VAL = COL_LONG_VAL;

        public static final String SETTING_TEMPVAL_NAME_SELECTION = TEMPVAL_NAME + " = ?";


        public static final String[] SETTING_STRING_VAL_PROJECTION = {STRING_VAL};

        public static final String[] SETTING_LONG_VAL_PROJECTION = {LONG_VAL};

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {_ID, TEMPVAL_NAME, STRING_VAL, LONG_VAL};
        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = TEMPVAL_NAME + " ASC";
    }



    public static final class ConnectedSafeBTQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "connected_safe_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_connected_safe_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_CONNECTED_SAFE_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_connected_safe_bt";

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {rldbSchema.TBL_CONNECTED_BT + "." +_ID, SERIAL, CONNECTED};


        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = SERIAL + " ASC";
    }



    public static final class ConnectedMonitoredBTQuery implements CommonColumns, SerialProjection {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "connected_monitored_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_connected_monitored_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_CONNECTED_MONITORED_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_connected_monitored_bt";

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {rldbSchema.TBL_CONNECTED_BT + "." +_ID, SERIAL, CONNECTED};
        public static final String[] PROJECTION_SERIAL = {SERIAL};

        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = rldbSchema.TBL_CONNECTED_BT + "." + SERIAL + " ASC";
    }


    public static final class AllConnectedMonitoredBTQuery implements CommonColumns, SerialProjection {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "all_connected_monitored_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_all_connected_monitored_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_CONNECTED_MONITORED_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_all_connected_monitored_bt";

        /**
         * A projection of all columns in the photos table.
         */
        public static final String[] PROJECTION_ALL = {rldbSchema.TBL_CONNECTED_BT + "." +_ID, SERIAL, CONNECTED};
        public static final String[] PROJECTION_SERIAL = {SERIAL};

        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = rldbSchema.TBL_CONNECTED_BT + "." + SERIAL + " ASC";
    }


    public static final class HasAnyMonitoredBTQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "has_monitored_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_has_monitored_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_HAS_MONITORED_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_has_monitored_bt";

        public static final String[] PROJECTION_ANY_ID = {rldbSchema.TBL_MONITORED_BT + "." +_ID};

    }

    public static final class HasAnySafeBTQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "has_safe_bt");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_has_safe_bt";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_HAS_SAFE_BT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_has_safe_bt";

        public static final String[] PROJECTION_ANY_ID = {rldbSchema.TBL_SAFE_BT + "." +_ID};
    }

    public static final class HasAnySafeWIFIQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "has_safe_wifi");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_has_safe_wifi";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_HAS_WIFI_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_has_safe_wifi";

        public static final String[] PROJECTION_ANY_ID = {rldbSchema.TBL_SAFE_WIFI + "." +_ID};

    }

    private static final class HasAnySafeWIFIMacQuery implements CommonColumns {
        /**
         * The Content URI for this table.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(rldbContract.CONTENT_URI, "has_safe_wifi_mac");
        /**
         * The mime type of a directory of joined entities.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/rldb_has_safe_wifi_mac";
        /**
         * The mime type of a single joined entity.
         */
        public static final String CONTENT_HAS_WIFI_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/rldb_has_safe_wifi_mac";

        public static final String[] PROJECTION_ANY_ID = {rldbSchema.TBL_SAFE_WIFI_MAC + "." +_ID};

    }



    /**
     * This interface defines common columns found in multiple tables.
     */
    interface CommonColumns extends BaseColumns {
        /**
         * The name of the item.
         */
        String SERIAL = COL_SERIAL;
        /**
         * The borrower of the item.
         */
        String NAME = COL_NAME;

        String CONNECTED = COL_CONNECTED;
    }

    public interface SerialProjection {

        String[] SERIAL_PROJECTION = {COL_SERIAL};

        String[] PROJECTION_CONNECTED_SERIAL = {rldbSchema.TBL_CONNECTED_BT + "." + COL_SERIAL};

        //public final String[] SERIAL_CONNECTED_STATE_PROJECTION = {COL_SERIAL, COL_CONNECTED};

    }


    /**
     * This interface defines common columns found in multiple tables.
     */
    private interface SerialSelections {

        String SERIAL_SELECTION = COL_SERIAL + " = ?";
    }



}
