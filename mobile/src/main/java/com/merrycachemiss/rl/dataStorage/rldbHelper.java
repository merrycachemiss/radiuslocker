/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Modifications Copyright © 2024 merry cache miss technologies, Inc.
 */

package com.merrycachemiss.rl.dataStorage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class rldbHelper extends SQLiteOpenHelper {

    private static final String TAG = "rldbHelper";
    private static final rldbHelper mInstance = null;
    private static final String NAME = rldbSchema.DB_NAME;
    private static final int VERSION = 1;

    rldbHelper(Context context) {
        super(context, NAME, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
            db.execSQL(rldbSchema.DDL_CREATE_TBL_MONITORED_BT);
            db.execSQL(rldbSchema.DDL_CREATE_TBL_SAFE_BT);
            db.execSQL(rldbSchema.DDL_CREATE_TBL_CONNECTED_BT);
            db.execSQL(rldbSchema.DDL_CREATE_TBL_SAFE_WIFI);
            db.execSQL(rldbSchema.DDL_CREATE_TBL_SAFE_WIFI_MAC);
            db.execSQL(rldbSchema.DDL_CREATE_TBL_SETTINGS_TEMP);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

//NOTE FOR SDK 30+ IF YOU DO A MIGRATION
//https://www.alecstrong.com/2020/07/sqlite-sdk-30/
//    TLDR: Android 30 upgrades SQLite from 3.22.0 -> 3.28.0, this introduces new alter table behavior which will potentially cause runtime exceptions when ALTER TABLE statements are ran on tables which are used in a view. To preserve old behavior turn on PRAGMA legacy_alter_table=ON before running your migrations.
//so:
// db.execSQL("PRAGMA legacy_alter_table=ON;");
    @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
