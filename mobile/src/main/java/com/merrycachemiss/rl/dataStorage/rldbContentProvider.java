/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Modifications Copyright © 2024 merry cache miss technologies, Inc.
 */


package com.merrycachemiss.rl.dataStorage;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;

import java.util.ArrayList;
import java.util.Objects;

import static com.merrycachemiss.rl.dataStorage.rldbContract.AUTHORITY;
import static com.merrycachemiss.rl.dataStorage.rldbContract.MonitoredBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SafeBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SafeWIFIQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SettingsTempValsQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.ConnectedBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.ConnectedMonitoredBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.ConnectedSafeBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.HasAnyMonitoredBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.HasAnySafeBTQuery;
import static com.merrycachemiss.rl.dataStorage.rldbContract.HasAnySafeWIFIQuery;

public final class rldbContentProvider extends ContentProvider {

    private static final String TAG = "rlContentProvider";
    // helper constants for use with the UriMatcher
    private static final int MONITORED_BT_LIST = 1;
    private static final int MONITORED_BT_ID = 2;
    private static final int SAFE_BT_LIST = 3;
    private static final int SAFE_BT_ID = 4;
    private static final int CONNECTED_BT_LIST = 5;
    private static final int CONNECTED_BT_ID = 6;
    private static final int SAFE_WIFI_LIST = 7;
    private static final int SAFE_WIFI_ID = 8;
    private static final int SETTINGS_TEMP_LIST = 9;
    private static final int SETTINGS_TEMP_ID = 10;
    private static final int CONNECTED_MONITORED_BT_LIST = 11;
    private static final int CONNECTED_MONITORED_BT_ID = 12;
    private static final int CONNECTED_SAFE_BT_LIST = 13;
    private static final int CONNECTED_SAFE_BT_ID = 14;
    private static final int HAS_MONITORED_BT_LIST = 15;
    private static final int HAS_MONITORED_BT_ID = 16;
    private static final int HAS_SAFE_BT_LIST = 17;
    private static final int HAS_SAFE_BT_ID = 18;
    private static final int HAS_SAFE_WIFI_LIST = 19;
    private static final int HAS_SAFE_WIFI_ID = 20;
    private static final int SAFE_WIFI_MAC_LIST = 21;
    private static final int SAFE_WIFI_MAC_ID = 22;
    private static final int ALL_SAFE_WIFI_SSID_LIST = 23;
    private static final int ALL_SAFE_WIFI_SSID_ID = 24;
    private static final int ALL_MONITORED_BT_LIST = 25;
    private static final int ALL_MONITORED_BT_ID = 26;
    private static final int ALL_CONNECTED_MONITORED_BT_LIST = 27;
    private static final int ALL_CONNECTED_MONITORED_BT_ID = 28;
    private static final UriMatcher URI_MATCHER;

    // prepare the UriMatcher
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, "monitored_bt", MONITORED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "monitored_bt/#", MONITORED_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "safe_bt", SAFE_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "safe_bt/#", SAFE_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "connected_bt", CONNECTED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "connected_bt/#", CONNECTED_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "safe_wifi", SAFE_WIFI_LIST);
        URI_MATCHER.addURI(AUTHORITY, "safe_wifi/#", SAFE_WIFI_ID);
        URI_MATCHER.addURI(AUTHORITY, "safe_wifi_mac", SAFE_WIFI_MAC_LIST);
        URI_MATCHER.addURI(AUTHORITY, "safe_wifi_mac/#", SAFE_WIFI_MAC_ID);
        URI_MATCHER.addURI(AUTHORITY, "settings_temp", SETTINGS_TEMP_LIST);
        URI_MATCHER.addURI(AUTHORITY, "settings_temp/#", SETTINGS_TEMP_ID);
        URI_MATCHER.addURI(AUTHORITY, "connected_safe_bt", CONNECTED_SAFE_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "connected_safe_bt/#", CONNECTED_SAFE_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "connected_monitored_bt", CONNECTED_MONITORED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "connected_monitored_bt/#", CONNECTED_MONITORED_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "has_monitored_bt", HAS_MONITORED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "has_monitored_bt/#", HAS_MONITORED_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "has_safe_bt", HAS_SAFE_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "has_safe_bt/#", HAS_SAFE_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "has_safe_wifi", HAS_SAFE_WIFI_LIST);
        URI_MATCHER.addURI(AUTHORITY, "has_safe_wifi/#", HAS_SAFE_WIFI_ID);
        URI_MATCHER.addURI(AUTHORITY, "all_safe_wifi/", ALL_SAFE_WIFI_SSID_LIST);
        URI_MATCHER.addURI(AUTHORITY, "all_safe_wifi/#", ALL_SAFE_WIFI_SSID_ID);
        URI_MATCHER.addURI(AUTHORITY, "all_monitored_bt/", ALL_MONITORED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "all_monitored_bt/#", ALL_MONITORED_BT_ID);
        URI_MATCHER.addURI(AUTHORITY, "all_connected_monitored_bt/", ALL_CONNECTED_MONITORED_BT_LIST);
        URI_MATCHER.addURI(AUTHORITY, "all_connected_monitored_bt/#", ALL_CONNECTED_MONITORED_BT_ID);
    }

    private rldbHelper dbHelper = null;

    private final ThreadLocal<Boolean> mIsInBatchMode = new ThreadLocal<>();

    @Override
    public boolean onCreate() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "contentprovider onCreate called. Try to init DB singleton from contentprovider");
        }
        Context storageContext;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            try {
                storageContext = Objects.requireNonNull(getContext()).createDeviceProtectedStorageContext();//used in the rest of the activity
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Initiated sql in device protected storage");
                }
                //crashLog(TAG + " Initiated sql in device protected storage", context);//not sure if need context or storagecontext
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to initiate sql in device protected storage, falling back to normal storage context");
                }
                storageContext = getContext();
            }

        } else {
            storageContext = getContext();
        }
        dbHelper = new rldbHelper(storageContext);//.getInstance(getContext());
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "DB now init from contentprovider");
        }
        return true;
    }


    @Override
    public String getType(@NonNull Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case MONITORED_BT_LIST:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned rldbContract.MonitoredBTQuery.CONTENT_TYPE");
                }
                return rldbContract.MonitoredBTQuery.CONTENT_TYPE;
            case MONITORED_BT_ID:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned rldbContract.MonitoredBTQuery.CONTENT_MONITORED_BT_TYPE");
                }
                return rldbContract.MonitoredBTQuery.CONTENT_MONITORED_BT_TYPE;

            case SAFE_BT_LIST:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned SafeBTQuery.CONTENT_TYPE");
                }
                return SafeBTQuery.CONTENT_TYPE;
            case SAFE_BT_ID:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned SafeBTQuery.CONTENT_SAFE_BT_TYPE");
                }
                return SafeBTQuery.CONTENT_SAFE_BT_TYPE;


            case CONNECTED_BT_LIST:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned ConnectedBTQuery.CONTENT_TYPE");
                }
                return ConnectedBTQuery.CONTENT_TYPE;
            case CONNECTED_BT_ID:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType returned ConnectedBTQuery.CONTENT_CONNECTED_BT_TYPE");
                }
                return rldbContract.ConnectedBTQuery.CONTENT_CONNECTED_BT_TYPE;


            case SAFE_WIFI_LIST:
                return SafeWIFIQuery.CONTENT_TYPE;
            case SAFE_WIFI_ID:
                return SafeWIFIQuery.CONTENT_SAFE_WIFI_TYPE;

            case ALL_SAFE_WIFI_SSID_LIST:
                return rldbContract.AllSafeWIFIQuery.CONTENT_TYPE;
            case ALL_SAFE_WIFI_SSID_ID:
                return rldbContract.AllSafeWIFIQuery.CONTENT_SAFE_WIFI_TYPE;

            case ALL_MONITORED_BT_LIST:
                return rldbContract.AllMonitoredBTQuery.CONTENT_TYPE;
            case ALL_MONITORED_BT_ID:
                return rldbContract.AllMonitoredBTQuery.CONTENT_MONITORED_BT_TYPE;

            case SAFE_WIFI_MAC_LIST:
                return rldbContract.SafeWIFIMACQuery.CONTENT_TYPE;
            case SAFE_WIFI_MAC_ID:
                return rldbContract.SafeWIFIMACQuery.CONTENT_SAFE_WIFI_MAC_TYPE;

            case SETTINGS_TEMP_LIST:
                return rldbContract.SettingsTempValsQuery.CONTENT_TYPE;
            case SETTINGS_TEMP_ID:
                return SettingsTempValsQuery.CONTENT_SETTINGS_TEMP_TYPE;


            case CONNECTED_SAFE_BT_LIST:
                return rldbContract.ConnectedSafeBTQuery.CONTENT_TYPE;
            case CONNECTED_SAFE_BT_ID:
                return ConnectedSafeBTQuery.CONTENT_CONNECTED_SAFE_BT_TYPE;


            case CONNECTED_MONITORED_BT_LIST:
                return rldbContract.ConnectedMonitoredBTQuery.CONTENT_TYPE;
            case CONNECTED_MONITORED_BT_ID:
                return ConnectedMonitoredBTQuery.CONTENT_CONNECTED_MONITORED_TYPE;



            case HAS_MONITORED_BT_LIST:
                return HasAnyMonitoredBTQuery.CONTENT_TYPE;
            case HAS_MONITORED_BT_ID:
                return HasAnyMonitoredBTQuery.CONTENT_HAS_MONITORED_BT_TYPE;

            case HAS_SAFE_BT_LIST:
                return HasAnySafeBTQuery.CONTENT_TYPE;
            case HAS_SAFE_BT_ID:
                return HasAnySafeBTQuery.CONTENT_HAS_SAFE_BT_TYPE;


            case HAS_SAFE_WIFI_LIST:
                return HasAnySafeWIFIQuery.CONTENT_TYPE;
            case HAS_SAFE_WIFI_ID:
                return HasAnySafeWIFIQuery.CONTENT_HAS_WIFI_TYPE;
            default:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getType(uri) - Unsupported URI: " + uri);
                }
                return null;
        }
    }

    /*
    * activities will be like:
    *   Uri mNewUri;
    *   Defines an object to contain the new values to insert
        ContentValues mNewValues = new ContentValues();

        mNewValues.put(UserDictionary.Words.APP_ID, "example.user");
        mNewValues.put(UserDictionary.Words.LOCALE, "en_US");
        mNewValues.put(UserDictionary.Words.WORD, "insert");
        mNewValues.put(UserDictionary.Words.FREQUENCY, "100");

        mNewUri = getContentResolver().insert(
                UserDictionary.Word.CONTENT_URI,   // the user dictionary content URI
                mNewValues                          // the values to insert
    );
    * */
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Insert to DB: " + uri);
        }
        if (URI_MATCHER.match(uri) != MONITORED_BT_LIST
                && URI_MATCHER.match(uri) != SAFE_BT_LIST
                && URI_MATCHER.match(uri) != CONNECTED_BT_LIST
                && URI_MATCHER.match(uri) != SAFE_WIFI_LIST
                && URI_MATCHER.match(uri) != SETTINGS_TEMP_LIST) {
            throw new IllegalArgumentException(
                    "insert() - Unsupported URI for insertion: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case MONITORED_BT_LIST: {

                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_MONITORED_BT,
                                null,
                                values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
            case SAFE_BT_LIST: {
                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_SAFE_BT,
                                null,
                                values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
            case CONNECTED_BT_LIST: {
                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_CONNECTED_BT,
                                null,
                                values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
            case SAFE_WIFI_LIST: {
                ContentValues vals = new ContentValues();

                String ssidNoQuotes = values.getAsString(SafeWIFIQuery.SSID);
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length() - 1);
                }
                vals.put(SafeWIFIQuery.SSID, ssidNoQuotes);
                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_SAFE_WIFI,
                                null,
                                vals,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
            case SAFE_WIFI_MAC_LIST: {
                ContentValues vals = new ContentValues();

                String ssidNoQuotes = values.getAsString(rldbContract.SafeWIFIMACQuery.SSID);
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length() - 1);
                }
                vals.put(rldbContract.SafeWIFIMACQuery.SSID, ssidNoQuotes);
                vals.put(rldbContract.SafeWIFIMACQuery.MAC, ssidNoQuotes);
                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_SAFE_WIFI_MAC,
                                null,
                                vals,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
            case SETTINGS_TEMP_LIST: {
                long id =
                        db.insertWithOnConflict(
                                rldbSchema.TBL_SETTINGS_TEMP,
                                null,
                                values,
                                SQLiteDatabase.CONFLICT_REPLACE);
                return getUriForId(id, uri);
            }
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "insert() - Unsupported URI: " + uri);
        }
        throw new IllegalArgumentException("insert() - Unsupported URI: " + uri);
    }


    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        boolean useAuthorityUri = false;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "query DB: " + uri);
        }
        String[] proj = projection;
        String ssidNoQuotes;
        switch (URI_MATCHER.match(uri)) {
//            case MONITORED_BT_LIST:
//                builder.setTables(rldbSchema.TBL_MONITORED_BT);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = MonitoredBTQuery.SORT_ORDER_DEFAULT;
//                }
//                break;
            case MONITORED_BT_LIST://get monitored BT by serial
                builder.setTables(rldbSchema.TBL_MONITORED_BT);
                // limit query to one row at most:
                builder.appendWhere(rldbContract.MonitoredBTQuery.SERIAL_SELECTION);
                break;
            case MONITORED_BT_ID://get monitored BT by serial
                builder.setTables(rldbSchema.TBL_MONITORED_BT);
                // limit query to one row at most:
                builder.appendWhere(rldbContract.MonitoredBTQuery.SERIAL_SELECTION);
                break;
//            case SAFE_BT_LIST:
//                builder.setTables(rldbSchema.TBL_SAFE_BT);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = SafeBTQuery.SORT_ORDER_DEFAULT;
//                }
//                break;
            case SAFE_BT_ID://get safe BT by serial
                builder.setTables(rldbSchema.TBL_SAFE_BT);
                // limit query to one row at most:
                builder.appendWhere(SafeBTQuery.SERIAL_SELECTION);
                break;
            case SAFE_BT_LIST://get safe BT by serial
                builder.setTables(rldbSchema.TBL_SAFE_BT);
                // limit query to one row at most:
                builder.appendWhere(SafeBTQuery.SERIAL_SELECTION);
                break;
//            case CONNECTED_BT_LIST:
//                builder.setTables(rldbSchema.TBL_CONNECTED_BT);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = ConnectedBTQuery.SORT_ORDER_DEFAULT;
//                }
//                useAuthorityUri = true;
//                break;
            case CONNECTED_BT_ID://get connected BT by serial
                //proj = ConnectedBTQuery.PROJECTION_SERIAL_AND_UPDT_DT_TM;
                builder.setTables(rldbSchema.TBL_CONNECTED_BT);
                // limit query to one row at most:
                builder.appendWhere(ConnectedBTQuery.CONNECTED_SERIAL_SELECTION);
                break;
            case CONNECTED_BT_LIST://get connected BT by serial
                //proj = ConnectedBTQuery.PROJECTION_SERIAL_AND_UPDT_DT_TM;
                builder.setTables(rldbSchema.TBL_CONNECTED_BT);
                // limit query to one row at most:
                builder.appendWhere(ConnectedBTQuery.CONNECTED_SERIAL_SELECTION);
                break;
//            case SAFE_WIFI_LIST:
//                builder.setTables(rldbSchema.TBL_SAFE_WIFI);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = SafeWIFIQuery.SORT_ORDER_DEFAULT;
//                }
//                useAuthorityUri = true;
//                break;
            case SAFE_WIFI_ID://get safe wifi by SSID
                ssidNoQuotes = selectionArgs[0];
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length()-1);
                }
                selectionArgs[0] = ssidNoQuotes;
                builder.setTables(rldbSchema.TBL_SAFE_WIFI);
                // limit query to one row at most:
                builder.appendWhere(SafeWIFIQuery.SSID_SELECTION);
                break;
            case SAFE_WIFI_LIST://get safe wifi by SSID
                ssidNoQuotes = selectionArgs[0];
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length()-1);
                }
                selectionArgs[0] = ssidNoQuotes;
                builder.setTables(rldbSchema.TBL_SAFE_WIFI);
                // limit query to one row at most:
                builder.appendWhere(SafeWIFIQuery.SSID_SELECTION);
                break;

            case SAFE_WIFI_MAC_ID://get safe wifi by MAC (ALT TABLE)

                builder.setTables(rldbSchema.TBL_SAFE_WIFI_MAC);
                // limit query to one row at most:
                builder.appendWhere(rldbContract.SafeWIFIMACQuery.MAC_SELECTION);
                break;
            case SAFE_WIFI_MAC_LIST://get safe wifi by  MAC (ALT TABLE)
                builder.setTables(rldbSchema.TBL_SAFE_WIFI_MAC);
                // limit query to one row at most:
                builder.appendWhere(rldbContract.SafeWIFIMACQuery.MAC_SELECTION);
                break;

            case ALL_SAFE_WIFI_SSID_ID://get all safe wifi ssid
                builder.setTables(rldbSchema.TBL_SAFE_WIFI);
                break;
            case ALL_SAFE_WIFI_SSID_LIST://get all safe wifi ssid
                builder.setTables(rldbSchema.TBL_SAFE_WIFI);
                break;

            case ALL_MONITORED_BT_ID://get all safe wifi ssid
                builder.setTables(rldbSchema.TBL_MONITORED_BT);
                break;
            case ALL_MONITORED_BT_LIST://get all safe wifi ssid
                builder.setTables(rldbSchema.TBL_MONITORED_BT);
                break;

//            case SETTINGS_TEMP_LIST:
//                builder.setTables(rldbSchema.TBL_SETTINGS_TEMP);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = SettingsTempValsQuery.SORT_ORDER_DEFAULT;
//                }
//                useAuthorityUri = true;
//                break;
            case SETTINGS_TEMP_ID://remember that the projection passed here determines what gets returned, string or long val
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "query received: SETTINGS_TEMP_ID");
                }
                builder.setTables(rldbSchema.TBL_SETTINGS_TEMP);
                // limit query to one row at most:
                builder.appendWhere(SettingsTempValsQuery.SETTING_TEMPVAL_NAME_SELECTION);
                break;
            case SETTINGS_TEMP_LIST://remember that the projection passed here determines what gets returned, string or long val
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "query received: SETTINGS_TEMP_LIST");
                }
                builder.setTables(rldbSchema.TBL_SETTINGS_TEMP);
                // limit query to one row at most:
                builder.appendWhere(SettingsTempValsQuery.SETTING_TEMPVAL_NAME_SELECTION);
                break;
//            case CONNECTED_SAFE_BT_LIST:
//                builder.setTables(rldbSchema.JOIN_CONNECTED_SAFE_BT_STATEMENT);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = ConnectedSafeBTQuery.SORT_ORDER_DEFAULT;
//                }
//                useAuthorityUri = true;
//                break;
            case CONNECTED_SAFE_BT_LIST:
                proj = rldbContract.SerialProjection.PROJECTION_CONNECTED_SERIAL;
                builder.setTables(rldbSchema.JOIN_CONNECTED_SAFE_BT_STATEMENT);
                //builder.appendWhere(rldbSchema.TBL_CONNECTED_BT + "." + ConnectedBTQuery.CONNECTED + " = 1");
                useAuthorityUri = true;
                break;
//            case CONNECTED_MONITORED_BT_LIST:
//                builder.setTables(rldbSchema.JOIN_CONNECTED_MONITORED_BT_STATEMENT);
//                if (TextUtils.isEmpty(sortOrder)) {
//                    sortOrder = ConnectedMonitoredBTQuery.SORT_ORDER_DEFAULT;
//                }
//                useAuthorityUri = true;
//                break;
            case CONNECTED_MONITORED_BT_LIST:
                proj = rldbContract.SerialProjection.PROJECTION_CONNECTED_SERIAL;
                builder.setTables(rldbSchema.JOIN_CONNECTED_MONITORED_BT_STATEMENT);

                useAuthorityUri = true;
                break;

            case ALL_CONNECTED_MONITORED_BT_LIST:
                proj = rldbContract.SerialProjection.PROJECTION_CONNECTED_SERIAL;
                builder.setTables(rldbSchema.JOIN_ALL_CONNECTED_MONITORED_BT_STATEMENT);

                useAuthorityUri = true;
                break;


            case HAS_MONITORED_BT_LIST:
                builder.setTables(rldbSchema.HAS_MONITORED_BT_STORED_SELECT_STATEMENT);
                proj = HasAnyMonitoredBTQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            case HAS_SAFE_BT_LIST:
                builder.setTables(rldbSchema.HAS_SAFE_BT_STORED_SELECT_STATEMENT);
                proj = HasAnySafeBTQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            case HAS_SAFE_WIFI_LIST:
                builder.setTables(rldbSchema.HAS_SAFE_WIFI_STORED_SELECT_STATEMENT);
                proj = HasAnySafeWIFIQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            case HAS_MONITORED_BT_ID:
                builder.setTables(rldbSchema.HAS_MONITORED_BT_STORED_SELECT_STATEMENT);
                proj = HasAnyMonitoredBTQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            case HAS_SAFE_BT_ID:
                builder.setTables(rldbSchema.HAS_SAFE_BT_STORED_SELECT_STATEMENT);
                proj = HasAnySafeBTQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            case HAS_SAFE_WIFI_ID:
                builder.setTables(rldbSchema.HAS_SAFE_WIFI_STORED_SELECT_STATEMENT);
                proj = HasAnySafeWIFIQuery.PROJECTION_ANY_ID;
                useAuthorityUri = true;
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        //builder.setStrict(true);//requires projectionmap, but allows greater security <------------------------------------- try after things are working

        // https://developer.android.com/reference/android/database/sqlite/SQLiteQueryBuilder.html#setStrict(boolean)
        Cursor cursor = builder.query(db, proj, selection, selectionArgs,
                null, null, sortOrder);
        // if we want to be notified of any changes:
        if (useAuthorityUri) {
            cursor.setNotificationUri(Objects.requireNonNull(getContext()).getContentResolver(), rldbContract.CONTENT_URI);
        } else {
            cursor.setNotificationUri(Objects.requireNonNull(getContext()).getContentResolver(), uri);
        }
        return cursor;


        //Log.d(TAG, "Unsupported URI: " + uri);
        //throw new IllegalArgumentException("Unsupported URI: " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        int updateCount = 0;
        switch (URI_MATCHER.match(uri)) {
            case CONNECTED_BT_ID:
//                ContentValues vals = new ContentValues();
//                vals.put(ConnectedBTQuery.CONNECTED, 0);
//                vals.put(ConnectedBTQuery.UPDT_DT_TM, System.currentTimeMillis());
//                String where = ConnectedBTQuery.CONNECTED + "= ?";
//                final String[] selectArgs = {"1"};

                updateCount = db.update(rldbSchema.TBL_CONNECTED_BT,
                        values,
                        selection,
                        selectionArgs);
                //vals.clear();
                break;

            case CONNECTED_BT_LIST://update whole table to connected=0
//                ContentValues vals2 = new ContentValues();
//                vals2.put(ConnectedBTQuery.CONNECTED, 0);
//                vals2.put(ConnectedBTQuery.UPDT_DT_TM, System.currentTimeMillis());
//                String where2 = ConnectedBTQuery.CONNECTED + "= ?";
//                final String[] selectArgs2 = {"1"};

                updateCount = db.update(rldbSchema.TBL_CONNECTED_BT,
                        values,
                        selection,
                        selectionArgs);

                //vals2.clear();
                break;

            default:
                // no support for updating photos or entities!
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Unsupported URI: " + uri);
                }
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (updateCount > 0 && !isInBatchMode()) {
                Objects.requireNonNull(getContext()).getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "delete from DB: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int delCount = 0;
        String ssidNoQuotes;
        switch (URI_MATCHER.match(uri)) {
            case MONITORED_BT_ID:
                delCount = db.delete(rldbSchema.TBL_MONITORED_BT, MonitoredBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case MONITORED_BT_LIST:
                delCount = db.delete(rldbSchema.TBL_MONITORED_BT, MonitoredBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case SAFE_BT_ID:
                delCount = db.delete(rldbSchema.TBL_SAFE_BT, SafeBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case SAFE_BT_LIST:
                delCount = db.delete(rldbSchema.TBL_SAFE_BT, SafeBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case CONNECTED_BT_ID:
                delCount = db.delete(rldbSchema.TBL_CONNECTED_BT, ConnectedBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case CONNECTED_BT_LIST:
                delCount = db.delete(rldbSchema.TBL_CONNECTED_BT, ConnectedBTQuery.SERIAL_SELECTION, selectionArgs);
                break;
            case SAFE_WIFI_ID:
                ssidNoQuotes = selectionArgs[0];
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length()-1);
                }
                selectionArgs[0] = ssidNoQuotes;
                delCount = db.delete(rldbSchema.TBL_SAFE_WIFI, SafeWIFIQuery.SSID_SELECTION, selectionArgs);
                break;
            case SAFE_WIFI_LIST:
                ssidNoQuotes = selectionArgs[0];
                if (ssidNoQuotes.startsWith("\"") && ssidNoQuotes.endsWith("\"")) {
                    ssidNoQuotes = ssidNoQuotes.substring(1, ssidNoQuotes.length()-1);
                }
                selectionArgs[0] = ssidNoQuotes;
                delCount = db.delete(rldbSchema.TBL_SAFE_WIFI, SafeWIFIQuery.SSID_SELECTION, selectionArgs);
                break;
            case SAFE_WIFI_MAC_ID:
                delCount = db.delete(rldbSchema.TBL_SAFE_WIFI_MAC, rldbContract.SafeWIFIMACQuery.MAC_SELECTION, selectionArgs);
                break;
            case SAFE_WIFI_MAC_LIST:
                delCount = db.delete(rldbSchema.TBL_SAFE_WIFI_MAC, rldbContract.SafeWIFIMACQuery.MAC_SELECTION, selectionArgs);
                break;
            case SETTINGS_TEMP_ID:
                delCount = db.delete(rldbSchema.TBL_SETTINGS_TEMP, SettingsTempValsQuery.SETTING_TEMPVAL_NAME_SELECTION, selectionArgs);
                break;
            case SETTINGS_TEMP_LIST:
                delCount = db.delete(rldbSchema.TBL_SETTINGS_TEMP, SettingsTempValsQuery.SETTING_TEMPVAL_NAME_SELECTION, selectionArgs);
                break;

            default:
                // no support for deleting photos or entities -
                // photos are deleted by a trigger when the item is deleted
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Unsupported URI: " + uri);
                }
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (delCount > 0 && !isInBatchMode()) {
            Objects.requireNonNull(getContext()).getContentResolver().notifyChange(uri, null);
        }
        return delCount;

    }


    private Uri getUriForId(long id, Uri uri) {
        if (id > 0) {
            Uri itemUri = ContentUris.withAppendedId(uri, id);
            if (!isInBatchMode()) {
                // notify all listeners of changes:
                Objects.requireNonNull(getContext()).
                        getContentResolver().
                        notifyChange(itemUri, null);
            }
            return itemUri;
        }
        // s.th. went wrong:
        throw new SQLException(
                "Problem while inserting into uri: " + uri);
    }

    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(
            @NonNull ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        mIsInBatchMode.set(true);
        // the next line works because SQLiteDatabase
        // uses a thread local SQLiteSession object for
        // all manipulations
        db.beginTransaction();
        try {
            final ContentProviderResult[] retResult = super.applyBatch(operations);
            db.setTransactionSuccessful();
            Objects.requireNonNull(getContext()).getContentResolver().notifyChange(rldbContract.CONTENT_URI, null);
            return retResult;
        }
        finally {
            mIsInBatchMode.remove();
            db.endTransaction();
        }
    }

    private boolean isInBatchMode() {

        return mIsInBatchMode.get() != null && mIsInBatchMode.get();
    }





    /*
    * The dummyQuery method is only used to show a sample query.
    * It's not used within this sample app.
    this would be put in activities/services
    @SuppressWarnings("unused")
    private void dummyQuery() {

        String itemId = "10";
        String selection = LentItemsContract.SELECTION_ID_BASED; // BaseColumns._ID
        // + " = ? "
        String[] selectionArgs = {itemId};
        ContentResolver resolver = getActivity().getContentResolver();
        Cursor c = resolver.query(
                Items.CONTENT_URI,          // die URI
                Items.PROJECTION_ALL,       // optionale Angabe der gewünschten Spalten
                selection,                  // optionale WHERE Klausel (ohne Keyword)
                selectionArgs,              // optionale Wildcard Ersetzungen
                Items.SORT_ORDER_DEFAULT);  // optionale ORDER BY Klausel (ohne Keyword)

        if (c != null && c.moveToFirst()) {
            // int idx = c.getColumnIndex(Items.NAME);
            String name = c.getString(1);
            String borrower = c.getString(2);
        }

    }
    * */
}
