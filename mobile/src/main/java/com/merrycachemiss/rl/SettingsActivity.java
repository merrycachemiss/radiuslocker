/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.admin.DevicePolicyManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SwitchCompat;
import androidx.biometric.BiometricManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.jobs.constantPinChargingTriggerJob;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.receivers.DeviceAdmin;
import com.merrycachemiss.rl.tasker.getTaskerProjectsTasks;
import com.merrycachemiss.rl.util.bioPinAuthentication;
import com.merrycachemiss.rl.util.getSetCurrentWIFISSID;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.merrycachemiss.rl.MainActivity.delayLockSavedVal;
import static com.merrycachemiss.rl.MainActivity.dontLockIfCharging;
import static com.merrycachemiss.rl.MainActivity.dontLockTwiceSavedVal;
import static com.merrycachemiss.rl.MainActivity.delayTaskSpinnerDelayMultiplierSavedVal;
import static com.merrycachemiss.rl.MainActivity.lockIfCableRemoved;
import static com.merrycachemiss.rl.MainActivity.lockIfSIMRemoved;
import static com.merrycachemiss.rl.MainActivity.multiWatchMode;
import static com.merrycachemiss.rl.MainActivity.buttonRemap;
import static com.merrycachemiss.rl.MainActivity.postSnoozeLockCheck;
import static com.merrycachemiss.rl.MainActivity.requireBiometricAuth;
import static com.merrycachemiss.rl.MainActivity.bedtimeMode;
import static com.merrycachemiss.rl.MainActivity.requirePinPassOnlyForAuth;
import static com.merrycachemiss.rl.MainActivity.retrievedSettingsVars;

import static com.merrycachemiss.rl.MainActivity.startConstantPinIfLeaveSnoozeWifi;
import static com.merrycachemiss.rl.MainActivity.useWearApis;
import static com.merrycachemiss.rl.dataStorage.dataConstants.BEDTIME_PROTECTION_SETTING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_BY_CHOICE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DELAY_BEFORE_LOCK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_IF_CHARGING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_TWICE_WITHIN;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_CABLE_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_SIM_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.POST_SNOOZE_LOCK_CHECK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REAUTH_TRIGGER_TIME;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.REQUIRE_BIOMETRIC_AUTH_USE_APP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.ONLY_USE_PIN_PASS_FOR_RL_AUTH;
import static com.merrycachemiss.rl.dataStorage.dataConstants.START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_DELAY;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP;


import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_WEAR_APIS;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.tasker.TaskerIntent.getExternalAccessPrefsIntent;
import static com.merrycachemiss.rl.tasker.getTaskerProjectsTasks.checkedItem;
import static com.merrycachemiss.rl.tasker.getTaskerProjectsTasks.taskStringArray;
import static com.merrycachemiss.rl.util.bioPinAuthentication.PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY;

public final class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    private Button chooseWIFIbutton;
    private Button chooseSafeBTbutton;
    private Button chooseTasksButton;
    private Spinner delayTaskSpinner;
    private Spinner delayLockSpinner;
    private String delayLockSelectedItem;
    private SwitchCompat dontLockIfChargingSwitch;
    private SwitchCompat postSnoozeLockCheckSwitch;
    private SwitchCompat lockIfSIMRemovedSwitch;
    private SwitchCompat lockIfCableRemovedSwitch;
    private SwitchCompat startConstantPinIfLeaveSnoozeWifiSwitch;
    private SwitchCompat requireBiometricAuthSwitch;
    private SwitchCompat wearApiSwitch;
    private AppCompatCheckBox requirePinPassAuthOnlyCheckbox;
    private String dontLockTwiceSelectedItem;
    private String delayTaskSelectedItem;
    private Spinner dontLockTwiceSpinner;
    private TextView taskerErrorText;
    private static int delayTaskSpinnerPosSelection;
    private static int delayLockSpinnerPosSelection;
    private SwitchCompat bedtimeModeSwitch;
    private TextView alarmTimeText;
    private SwitchCompat multiWatchModeSwitch;
    private SwitchCompat buttonRemapSwitch;
    private bioPinAuthentication auth;
    private boolean isAuthenticating = false;
    private boolean isOnboarding = false;

    private final boolean isDebuggable = BuildConfig.DEBUG;
    private static final String TAG = "SettingsActivity";

    private rldbInsertQueryDelete iqd;

    private getTaskerProjectsTasks tasker;

    private static final int DEVICE_ADMIN_ACTIVATION_REQUEST = 47; //Request ID for Device Administrator

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar myToolbar = findViewById(R.id.settingsToolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }


        iqd = new rldbInsertQueryDelete(this);

        chooseWIFIbutton = findViewById(R.id.chooseWIFIbutton);
        chooseSafeBTbutton = findViewById(R.id.chooseSafeBTbutton);
        chooseTasksButton = findViewById(R.id.chooseTasksButton);
        dontLockIfChargingSwitch = findViewById(R.id.dontLockIfChargingSwitch);
        postSnoozeLockCheckSwitch = findViewById(R.id.postSnoozeLockCheckSwitch);
        lockIfSIMRemovedSwitch = findViewById(R.id.lockIfSIMRemovedSwitch);
        lockIfCableRemovedSwitch = findViewById(R.id.lockIfCableRemovedSwitch);
        startConstantPinIfLeaveSnoozeWifiSwitch = findViewById(R.id.startConstantPinIfLeaveSnoozeWifiSwitch);
        requireBiometricAuthSwitch = findViewById(R.id.requireBiometricAuthSwitch);
        delayTaskSpinner = findViewById(R.id.delayTaskSpinner);
        taskerErrorText = findViewById(R.id.taskerErrorText);
        delayLockSpinner = findViewById(R.id.delayLockSpinner);
        dontLockTwiceSpinner = findViewById(R.id.dontLockTwiceSpinner);
        bedtimeModeSwitch = findViewById(R.id.bedtimeModeSwitch);
        alarmTimeText = findViewById(R.id.alarmTimeText);
        multiWatchModeSwitch = findViewById(R.id.multiWatchModeSwitch);
        buttonRemapSwitch = findViewById(R.id.buttonRemapSwitch);
        requirePinPassAuthOnlyCheckbox = findViewById(R.id.requirePinPassAuthOnlyCheckbox);
        wearApiSwitch = findViewById(R.id.wearApiSwitch);
        chooseWIFIbutton.setOnClickListener(this);
        chooseSafeBTbutton.setOnClickListener(this);
        chooseTasksButton.setOnClickListener(this);
        requirePinPassAuthOnlyCheckbox.setOnClickListener(this);

        CompoundButton.OnCheckedChangeListener switchListener = (v, isChecked) -> {
            if (v.getId() == R.id.dontLockIfChargingSwitch) {
                dontLockIfChargingSwitch();
            } else if (v.getId() == R.id.postSnoozeLockCheckSwitch) {
                postSnoozeLockCheckSwitch();
            } else if (v.getId() == R.id.lockIfSIMRemovedSwitch) {
                lockIfSIMRemovedSwitch();
            } else if (v.getId() == R.id.lockIfCableRemovedSwitch) {
                lockIfCableRemovedSwitch();
            } else if (v.getId() == R.id.startConstantPinIfLeaveSnoozeWifiSwitch) {
                startConstantPinIfLeaveSnoozeWifiSwitchSwitch();
            } else if (v.getId() == R.id.requireBiometricAuthSwitch) {
                requireBiometricAuthSwitch();
            } else if (v.getId() == R.id.bedtimeModeSwitch) {
                bedtimeModeSetup();
            } else if (v.getId() == R.id.multiWatchModeSwitch) {
                multiWatchModeSwitch();
            } else if (v.getId() == R.id.buttonRemapSwitch) {
                buttonRemapSwitch();
            } else if (v.getId() == R.id.wearApiSwitch){
                wearApiSwitch();
            }
        };
        dontLockIfChargingSwitch.setOnCheckedChangeListener(switchListener);
        postSnoozeLockCheckSwitch.setOnCheckedChangeListener(switchListener);
        lockIfSIMRemovedSwitch.setOnCheckedChangeListener(switchListener);
        lockIfCableRemovedSwitch.setOnCheckedChangeListener(switchListener);
        startConstantPinIfLeaveSnoozeWifiSwitch.setOnCheckedChangeListener(switchListener);
        requireBiometricAuthSwitch.setOnCheckedChangeListener(switchListener);
        bedtimeModeSwitch.setOnCheckedChangeListener(switchListener);
        multiWatchModeSwitch.setOnCheckedChangeListener(switchListener);
        buttonRemapSwitch.setOnCheckedChangeListener(switchListener);
        wearApiSwitch.setOnCheckedChangeListener(switchListener);

        setUpDontLockTwiceSpinner();
        setUpDelayTaskSpinner();
        setUpDelayLockSpinner();

        taskerErrorText.setVisibility(View.GONE);

        if (requireBiometricAuth == 1){
            requirePinPassAuthOnlyCheckbox.setEnabled(true);
        }
        if (requirePinPassOnlyForAuth == 1){
            requirePinPassAuthOnlyCheckbox.setChecked(true);
        }
        auth = new bioPinAuthentication(this, TAG);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.chooseWIFIbutton) {
            launchOtherActivity(wifiSafeListPickerActivity.class);
        } else if (v.getId() == R.id.chooseSafeBTbutton) {
            launchOtherActivity(btSafeListPickerActivity.class);
        } else if (v.getId() == R.id.chooseTasksButton) {
            try {
                chooseTasksButton();
            } catch (Exception e) {
                if (isDebuggable) {
                    Log.e(TAG, "failed to get tasker popup", e);
                }
            }
        } else if (v.getId() == R.id.requirePinPassAuthOnlyCheckbox){
            if (requirePinPassAuthOnlyCheckbox.isChecked()) {
                requirePinPassOnlyForAuth = 1;
            } else {
                requirePinPassOnlyForAuth = 0;
            }
            iqd.setSettingsTempLongVal(ONLY_USE_PIN_PASS_FOR_RL_AUTH, requirePinPassOnlyForAuth);
        }
    }


    private void setUpDontLockTwiceSpinner(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        
        Log.d(TAG,"create spinner array adapter - dont lock twice");
        ArrayAdapter<CharSequence> dontLockTwiceSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.dontLockTwiceVals, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        
        Log.d(TAG,"set up generic android simple spinner layout - dont lock twice");
        dontLockTwiceSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        
        Log.d(TAG,"apply adapter to spinner - dont lock twice");
        dontLockTwiceSpinner.setAdapter(dontLockTwiceSpinnerAdapter);

        
        Log.d(TAG,"dont_lock_twice_within set to prev used val: " + dontLockTwiceSavedVal);

        dontLockTwiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l)
            {
                dontLockTwiceSelectedItem = parent.getItemAtPosition(position).toString();
                switch (dontLockTwiceSelectedItem) {
                    case "N/A":
                        
                        Log.d(TAG,"dont_lock_twice_within set to 0 (N/A)");
                        //dontLockTwiceDisclaimerText.setVisibility(View.GONE);
                        dontLockTwiceSavedVal = 0;
                        break;
                    case "5 sec":
                        
                        Log.d(TAG,"dont_lock_twice_within set to 1 (5 sec)");
                        //dontLockTwiceDisclaimerText.setVisibility(View.VISIBLE);
                        dontLockTwiceSavedVal = 1;
                        break;
                    case "10 sec":
                        
                        Log.d(TAG,"dont_lock_twice_within set to 2 (10 sec)");
                        //dontLockTwiceDisclaimerText.setVisibility(View.VISIBLE);
                        dontLockTwiceSavedVal = 2;
                        break;
                    case "15 sec":
                        
                        Log.d(TAG,"dont_lock_twice_within set to 3 (15 sec)");
                        //dontLockTwiceDisclaimerText.setVisibility(View.VISIBLE);
                        dontLockTwiceSavedVal = 3;
                        break;
                    case "20 sec":
                        
                        Log.d(TAG,"dont_lock_twice_within set to 4 (20 sec)");
                        //dontLockTwiceDisclaimerText.setVisibility(View.VISIBLE);
                        dontLockTwiceSavedVal = 4;
                        break;
                    default:
                        break;
                }

                iqd.setSettingsTempLongVal(DONT_LOCK_TWICE_WITHIN, dontLockTwiceSavedVal);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });

        //set spinner to default val previously selected.
        dontLockTwiceSpinner.setSelection(dontLockTwiceSavedVal);

    }

    private void setUpDelayTaskSpinner(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        
        Log.d(TAG,"create spinner array adapter - delay task");
        ArrayAdapter<CharSequence> delayTaskSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.delayTaskVals, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        
        Log.d(TAG,"set up generic android simple spinner layout - delay task");
        delayTaskSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        
        Log.d(TAG,"apply adapter to spinner - delay task");
        delayTaskSpinner.setAdapter(delayTaskSpinnerAdapter);

        
        Log.d(TAG,"delay_task set to prev used val: " + delayTaskSpinnerDelayMultiplierSavedVal);

        delayTaskSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l)
            {
                delayTaskSelectedItem = parent.getItemAtPosition(position).toString();
                switch (delayTaskSelectedItem) {
                    case "N/A":
                        
                        Log.d(TAG,"delay_task set to 0 (N/A)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 0;
                        delayTaskSpinnerPosSelection = 0;
                        break;
                    case "15 min":
                        
                        Log.d(TAG,"delay_task set to 1 (15 minutes)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 1;
                        delayTaskSpinnerPosSelection = 1;
                        break;
                    case "30 min":
                        
                        Log.d(TAG,"delay_task set to 2 (30 minutes)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 2;
                        delayTaskSpinnerPosSelection = 2;
                        break;
                    case "1 hr":
                        
                        Log.d(TAG,"delay_task set to 4 (1 hour)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 4;
                        delayTaskSpinnerPosSelection = 3;
                        break;
                    case "2 hr":
                        
                        Log.d(TAG,"delay_task set to 8 (2 hours)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 8;
                        delayTaskSpinnerPosSelection = 4;
                        break;
                    case "4 hr":
                        
                        Log.d(TAG,"delay_task set to 16 (4 hours)");
                        delayTaskSpinnerDelayMultiplierSavedVal = 16;
                        delayTaskSpinnerPosSelection = 5;
                        break;
                    default:
                        break;
                }

                iqd.setSettingsTempLongVal(TASKER_JOB_DELAY, delayTaskSpinnerDelayMultiplierSavedVal);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });

        //set spinner to default val previously selected.
        if (delayTaskSpinnerDelayMultiplierSavedVal >=0 && delayTaskSpinnerDelayMultiplierSavedVal <= 2){
            delayTaskSpinnerPosSelection = delayTaskSpinnerDelayMultiplierSavedVal;
        } else if (delayTaskSpinnerDelayMultiplierSavedVal == 4){
            delayTaskSpinnerPosSelection = 3;
        } else if (delayTaskSpinnerDelayMultiplierSavedVal == 8){
            delayTaskSpinnerPosSelection = 4;
        } else if (delayTaskSpinnerDelayMultiplierSavedVal == 16){
            delayTaskSpinnerPosSelection = 5;
        }
        delayTaskSpinner.setSelection(delayTaskSpinnerPosSelection);
    }



    private void setUpDelayLockSpinner(){
        // Create an ArrayAdapter using the string array and a default spinner layout
        
        Log.d(TAG,"create spinner array adapter - delay lock");
        ArrayAdapter<CharSequence> delayLockSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.delayLockVals, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        
        Log.d(TAG,"set up generic android simple spinner layout - delay lock");
        delayLockSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        
        Log.d(TAG,"apply adapter to spinner - delay lock");
        delayLockSpinner.setAdapter(delayLockSpinnerAdapter);

        
        Log.d(TAG,"DELAY_BEFORE_LOCK set to prev used val: " + delayLockSavedVal);


        delayLockSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l)
            {
                delayLockSelectedItem = parent.getItemAtPosition(position).toString();
                switch (delayLockSelectedItem) {
                    case "N/A":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 0 (N/A)");
                        delayLockSavedVal = 0;
                        delayLockSpinnerPosSelection = 0;
                        break;
                    case "15 sec":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 1 (15 sec)");
                        delayLockSavedVal = 1;
                        delayLockSpinnerPosSelection = 1;
                        break;
                    case "20 sec":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 2 (20 sec)");
                        delayLockSavedVal = 2;
                        delayLockSpinnerPosSelection = 2;
                        break;
                    case "25 sec":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 3 (25 sec)");
                        delayLockSavedVal = 3;
                        delayLockSpinnerPosSelection = 3;
                        break;
                    case "60 sec":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 10 (60 sec)");
                        delayLockSavedVal = 10;
                        delayLockSpinnerPosSelection = 4;
                        break;
                    case "90 sec":
                        
                        Log.d(TAG,"DELAY_BEFORE_LOCK set to 16 (90 sec)");
                        delayLockSavedVal = 16;
                        delayLockSpinnerPosSelection = 5;
                        break;
                    default:
                        break;
                }

                iqd.setSettingsTempLongVal(DELAY_BEFORE_LOCK, delayLockSavedVal);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
        //set spinner to default val previously selected.
        if (delayLockSavedVal >=0 && delayLockSavedVal <= 3){
            delayLockSpinnerPosSelection = delayLockSavedVal;
        } else if (delayLockSavedVal == 10){
            delayLockSpinnerPosSelection = 4;
        } else if (delayLockSavedVal == 16){
            delayLockSpinnerPosSelection = 5;
        }
        //set spinner to default val previously selected.
        delayLockSpinner.setSelection(delayLockSpinnerPosSelection);
    }


    private void chooseTasksButton(){

        if (tasker == null) {
            if (isDebuggable) {
                Log.d(TAG, "creating getTaskerProjectsTasks object, since it wasn't before");
            }
            tasker = new getTaskerProjectsTasks(this);
        } else{
            if (isDebuggable) {
                Log.d(TAG, "getTaskerProjectsTasks object was already created");
            }
        }
        if (taskerWorks(true)) {
            // setup the alert builder
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Tasker Tasks");
            builder.setCancelable(false);


            tasker.getTasks(iqd.getSettingsTempStringVal(TASKER_TASK));//retrieve task list, but also send in previously chosen tasker task if exists

            if (!tasker.doesTaskMatchInDB()) {
                Runnable deleteThread = () -> {
                    iqd.setSettingsTempStringVal(TASKER_TASK, null);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "!doesTaskMatchInDB, so removed existing tasker task because doesn't exist in tasker anymore");
                    }
                };
                new Thread(deleteThread).start();
            }
            //checkeditem exists in getTaskerProjectsTasks as static var
            builder.setSingleChoiceItems(taskStringArray, checkedItem, (dialog, choice) -> {
                if (isDebuggable) {
                    Log.d(TAG, "chose " + taskStringArray[choice]);
                    Log.d(TAG, "checked item before(?) was " + taskStringArray[checkedItem]);

                }

                if (choice != 0) {
                    Runnable insertThread = () -> {
                        iqd.setSettingsTempStringVal(TASKER_TASK, taskStringArray[choice]);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "inserted " + taskStringArray[choice]);
                        }
                    };
                    new Thread(insertThread).start();
                } else {
                    Runnable deleteThread = () -> {
                        iqd.setSettingsTempStringVal(TASKER_TASK, null);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "removed tasker task");
                        }
                    };
                    new Thread(deleteThread).start();
                }
            });

            // add CLOSE button
            builder.setNeutralButton(getApplicationContext().getResources().getString(R.string.tasker_dialog_test_choice_now), (dialog, choice) -> {
                String taskToTest = iqd.getSettingsTempStringVal(TASKER_TASK);
                if (isDebuggable) {
                    Log.d(TAG, "chose to TEST task: " + taskToTest);
                }
                if (!Objects.equals(taskToTest, "") && !Objects.equals(taskToTest, "RL: NONE") && taskToTest != null) {
                    try {
                        tasker.executeTask(taskToTest, true);
                    } catch (Exception e) {
                        if (isDebuggable) {
                            Log.e(TAG, "failed to test task", e);
                        }
                        
                    }
                } else {
                    if (isDebuggable) {
                        Log.d(TAG, "User tried test NULL task. Avoided it.");
                    }
                }
            });
            // add CLOSE button
            builder.setPositiveButton(getApplicationContext().getResources().getString(R.string.tasker_dialog_set_choice), (dialog, choice) -> {
                if (isDebuggable) {
                    Log.d(TAG, "chose SET");
                }
            });
            // create and show alert dialog
            AlertDialog dialog = builder.create();
            dialog.show();

        } else {
            if (isDebuggable) {
                Log.d(TAG, "button press -> chooseTasksButton() -> taskerWorks(frombutton TRUE): false");
            }
        }
    }

    private void postSnoozeLockCheckSwitch(){
        if (postSnoozeLockCheckSwitch.isChecked() && postSnoozeLockCheck == 0){
            postSnoozeLockCheck = 1;
            iqd.setSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK, postSnoozeLockCheck);
        } else if (!postSnoozeLockCheckSwitch.isChecked() && postSnoozeLockCheck == 1){
            postSnoozeLockCheck = 0;
            iqd.setSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK, postSnoozeLockCheck);
            jobCancelClearNotif cancelClear = new jobCancelClearNotif(this, TAG);
            cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
            cancelClear.cancelJobClearNotif();
        }

    }

    private void lockIfSIMRemovedSwitch(){
        if (lockIfSIMRemovedSwitch.isChecked() && lockIfSIMRemoved == 0){
            lockIfSIMRemoved = 1;
            iqd.setSettingsTempLongVal(LOCK_IF_SIM_REMOVED, lockIfSIMRemoved);
        } else if (!lockIfSIMRemovedSwitch.isChecked() && lockIfSIMRemoved == 1){
            lockIfSIMRemoved = 0;
            iqd.setSettingsTempLongVal(LOCK_IF_SIM_REMOVED, lockIfSIMRemoved);
        }

    }

    private void lockIfCableRemovedSwitch(){
        if (lockIfCableRemovedSwitch.isChecked() && lockIfCableRemoved == 0){
            lockIfCableRemoved = 1;
            iqd.setSettingsTempLongVal(LOCK_IF_CABLE_REMOVED, lockIfCableRemoved);
        } else if (!lockIfCableRemovedSwitch.isChecked() && lockIfCableRemoved == 1){
            lockIfCableRemoved = 0;
            iqd.setSettingsTempLongVal(LOCK_IF_CABLE_REMOVED, lockIfCableRemoved);
        }
    }


    private void startConstantPinIfLeaveSnoozeWifiSwitchSwitch() {
        if (startConstantPinIfLeaveSnoozeWifiSwitch.isChecked() && startConstantPinIfLeaveSnoozeWifi == 0){
            startConstantPinIfLeaveSnoozeWifi = 1;
            iqd.setSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE, startConstantPinIfLeaveSnoozeWifi);

            if (isDebuggable) {
                Log.d(TAG, "activated startConstantPinIfLeaveSnoozeWifiSwitchSwitch(), storing current WIFI so it's ready.");
            }
            getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(this, TAG);
            wifiSSIDGetSet.setCurrentWIFISSID(iqd, wifiSSIDGetSet.getCurrentWIFISSID());
        } else if (!startConstantPinIfLeaveSnoozeWifiSwitch.isChecked() && startConstantPinIfLeaveSnoozeWifi == 1){
            startConstantPinIfLeaveSnoozeWifi = 0;
            iqd.setSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE, startConstantPinIfLeaveSnoozeWifi);
        }
    }


    private void requireBiometricAuthSwitch(){
        if (requireBiometricAuthSwitch.isChecked() && requireBiometricAuth == 0){
            try {
                isOnboarding = true;
                isAuthenticating = true;
                auth.authenticate(true, isOnboarding, 0);
            } catch (Exception e){
                if (isDebuggable) {
                    Log.e(TAG, "failed to do biometric setup/confirmation", e);
                }
                requireBiometricAuth = 0;
                iqd.setSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP, requireBiometricAuth);
                requireBiometricAuthSwitch.setChecked(false);
                
            }
        } else if (!requireBiometricAuthSwitch.isChecked() && requireBiometricAuth == 1){
            requireBiometricAuth = 0;
            iqd.setSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP, requireBiometricAuth);
            requirePinPassAuthOnlyCheckbox.setEnabled(false);

        }


    }

    private void multiWatchModeSwitch(){
        if (multiWatchModeSwitch.isChecked() && multiWatchMode == 0){
            multiWatchMode = 1;
            iqd.setSettingsTempLongVal(MULTI_DEVICE_MODE, multiWatchMode);
        } else if (!multiWatchModeSwitch.isChecked() && multiWatchMode == 1){
            multiWatchMode = 0;
            iqd.setSettingsTempLongVal(MULTI_DEVICE_MODE, multiWatchMode);
        }

    }

    private void buttonRemapSwitch(){
        if (buttonRemapSwitch.isChecked() && buttonRemap == 0){
            buttonRemap = 1;
            iqd.setSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP, buttonRemap);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Attempt to use button remapping");
            }
            
            try {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.constantPINServiceAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.lockNowAlias"),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "Failed to enable activity aliases", e);
                }
            }
        } else if (!buttonRemapSwitch.isChecked() && buttonRemap == 1){
            buttonRemap = 0;
            iqd.setSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP, buttonRemap);
            getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.constantPINServiceAlias"),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            getPackageManager().setComponentEnabledSetting(
                    new ComponentName("com.merrycachemiss.rl", "com.merrycachemiss.rl.lockNowAlias"),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        }

    }


    private void dontLockIfChargingSwitch(){
        if (dontLockIfChargingSwitch.isChecked() && dontLockIfCharging == 0) {
            dontLockIfCharging = 1;
            iqd.setSettingsTempLongVal(DONT_LOCK_IF_CHARGING, dontLockIfCharging);
        } else if (!dontLockIfChargingSwitch.isChecked() && dontLockIfCharging == 1){
            dontLockIfCharging = 0;
            iqd.setSettingsTempLongVal(DONT_LOCK_IF_CHARGING, dontLockIfCharging);
        }
    }

    private void wearApiSwitch(){
        if (wearApiSwitch.isChecked() && useWearApis == 0) {
            useWearApis = 1;
            iqd.setSettingsTempLongVal(USE_WEAR_APIS, useWearApis);
        } else if (!wearApiSwitch.isChecked() && useWearApis == 1){
            useWearApis = 0;
            iqd.setSettingsTempLongVal(USE_WEAR_APIS, useWearApis);
        }
    }

    private void setUpUI() {//need conditions for the new DB

        dontLockIfChargingSwitch.setChecked(dontLockIfCharging == 1);

        postSnoozeLockCheckSwitch.setChecked(postSnoozeLockCheck == 1);

        lockIfSIMRemovedSwitch.setChecked(lockIfSIMRemoved == 1);

        lockIfCableRemovedSwitch.setChecked(lockIfCableRemoved == 1);

        startConstantPinIfLeaveSnoozeWifiSwitch.setChecked(startConstantPinIfLeaveSnoozeWifi == 1);

        requireBiometricAuthSwitch.setChecked(requireBiometricAuth == 1);

        buttonRemapSwitch.setChecked(buttonRemap == 1);

        wearApiSwitch.setChecked(useWearApis == 1);

        try {
            taskerWorks(false);
        } catch (Exception e){
            if (isDebuggable){
                Log.e(TAG, "error checking tasker status - setUpUI() -> taskerWorks()", e);
            }
            
        }

        bedtimeModeSwitch.setChecked(bedtimeMode == 1);

        multiWatchModeSwitch.setChecked(multiWatchMode == 1);


        //only enable checkbox if user has biometrics and the option is set up. Otherwise, they're just using PIN anyway.
        requirePinPassAuthOnlyCheckbox.setEnabled(requireBiometricAuth == 1 &&
                (BiometricManager.from(getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK) == BiometricManager.BIOMETRIC_SUCCESS
                        || BiometricManager.from(getApplicationContext()).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //actionbar's "back" should not kill activities.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean taskerWorks(boolean fromButton){
        if (tasker == null) {
            if (isDebuggable) {
                Log.d(TAG, "creating getTaskerProjectsTasks object, since it wasn't before");
            }
            tasker = new getTaskerProjectsTasks(this);
        } else{
            if (isDebuggable) {
                Log.d(TAG, "getTaskerProjectsTasks object was already created");
            }
        }

        if (isDebuggable) {
            Log.d(TAG, "try to set ensure tasker works when user presses button");
        }
        String taskerCapabilityResponse = "";
        try {
            taskerCapabilityResponse = tasker.testTaskerCapability();
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Failed to test tasker", e);
            }
        }

        if (!Objects.equals(taskerCapabilityResponse, "RL")){
            //show tasker error text to user
            if (fromButton){

                try {
                    if (isDebuggable) {
                        Log.d(TAG, "show tasker error toast to user");
                    }
                    Toast.makeText(this,
                            taskerCapabilityResponse,
                            Toast.LENGTH_LONG).show();
                } catch (Exception e){
                    if (isDebuggable) {
                        Log.e(TAG, "fail show tasker error toast to user", e);
                    }
                }
                if (taskerCapabilityResponse.equals(getApplicationContext().getResources().getString(R.string.tasker_error_no_access))){
                    if (isDebuggable) {
                        Log.d(TAG, "taskerConfigedExtAccess(): false, try to start external access enabler tasker intent");
                    }
                    
                    try {
                        startActivity(getExternalAccessPrefsIntent());
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "Failed to start external access enabler in tasker", e);
                        }
                    }
                }
            }

            taskerErrorText.setVisibility(View.VISIBLE);
            taskerErrorText.setText(taskerCapabilityResponse);

            delayTaskSpinner.setSelection(0);
            
            Log.d(TAG,"delay_task set to 0 (DISABLED) since tasker appears to be inaccessible");
            delayTaskSpinnerDelayMultiplierSavedVal = 0;

            Runnable saveVal = () -> {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "saving tasker delay val to db on new thread");
            }
            iqd.setSettingsTempLongVal(TASKER_JOB_DELAY, delayTaskSpinnerDelayMultiplierSavedVal);
            };
            new Thread(saveVal).start();

        } else{
            //hide tasker error text

            taskerErrorText.setVisibility(View.GONE);
            return true;
        }
        return false;
    }

    private void bedtimeModeSetup(){
        if (bedtimeModeSwitch.isChecked() && bedtimeMode == 0){
            DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            ComponentName deviceAdminComponentName = new ComponentName(this, DeviceAdmin.class);
            if (devicePolicyManager != null && !devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
                //Begin enabling device administrator
                Intent deviceAdminEnableIntent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);

                deviceAdminEnableIntent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdminComponentName);
                deviceAdminEnableIntent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                        "RadiusLocker needs admin privileges in order to automatically lock" +
                                " your screen when your chosen wearable disconnects or in other situations where you set it up to be locked.");
                startActivityForResult(deviceAdminEnableIntent, DEVICE_ADMIN_ACTIVATION_REQUEST);

            } else if (devicePolicyManager != null && devicePolicyManager.isAdminActive(deviceAdminComponentName)) {
                resetConstantPINVars();
                bedtimeMode = 1;
                iqd.setSettingsTempLongVal(BEDTIME_PROTECTION_SETTING, bedtimeMode);
                scheduleConstantPinChargingTriggerJob();
                constantPinHelper constHelp = new constantPinHelper(this);
                constHelp.activateConstantPINServiceIfNeeded(this, iqd);

            }
        } else if ((!bedtimeModeSwitch.isChecked() && bedtimeMode == 1)){
            resetConstantPINVars();
            bedtimeMode = 0;
            iqd.setSettingsTempLongVal(BEDTIME_PROTECTION_SETTING, bedtimeMode);
            cancelConstantPinItems();
        }
    }

    private void resetConstantPINVars(){
        iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP, 0);
        if (iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE) != 1) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "didn't alter pause time, due to manual mode being triggered");
            }
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, 0);
            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);
        }
    }
    private void scheduleConstantPinChargingTriggerJob(){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "try to set up constant PIN scheduleConstantPinChargingTriggerJob()");
        }

        ComponentName componentName = new ComponentName(this, constantPinChargingTriggerJob.class);
        JobInfo jobInfo = new JobInfo.Builder(CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID, componentName)
                .setRequiresCharging(true)
                .setRequiresDeviceIdle(false)
                .setPersisted(true)
                .build();
        JobScheduler jobScheduler = (JobScheduler)this.getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = 0;
        if (jobScheduler != null) {
            resultCode = jobScheduler.schedule(jobInfo);
            if (resultCode == JobScheduler.RESULT_SUCCESS) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Job schedule success: constant PIN scheduleConstantPinChargingTriggerJob()");
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Job schedule fail: constant PIN scheduleConstantPinChargingTriggerJob()");
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Job schedule fail: constant PIN scheduleConstantPinChargingTriggerJob(). Jobscheduler is null.");
            }
        }

    }



    private void cancelConstantPinItems(){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "cancelConstantPinItems()");
        }
        //They've disabled constant PIN bedtime mode, so disable the charging trigger.
        //but then the below part in the IF for manual only cancels heartbeat job if manual mode isn't on
        jobCancelClearNotif cancelClear = new jobCancelClearNotif(this, TAG);
        cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_CHARGING_TRIGGER_JOB_ID, 0, 0);
        cancelClear.cancelJobClearNotif();

        cancelClear.setJobAndNotifParams(CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID, 0, 0);
        cancelClear.cancelJobClearNotif();
        //NOT ENABLED MANUALLY AND NO SLEEPING DETECTED
        if (iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_BY_CHOICE) != 1){
            cancelClear.setJobAndNotifParams(CONSTANTLY_REQUIRE_PIN_HEARTBEAT_JOB_ID, 0, 0);
            cancelClear.cancelJobClearNotif();
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Stopping constant PIN service, since user turned off the feature from settings and manual mode is not enabled.");
            }

            constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
            constHelp.stopConstantPinIfNecessary(getApplicationContext(), iqd, false);
        } else if (BuildConfig.DEBUG) {
            Log.d(TAG, "Constant PIN service wasn't shut down via settings when user turned off bedtime mode, as manual mode is still on");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEVICE_ADMIN_ACTIVATION_REQUEST) {//device admin intent enable was requested
            if (resultCode != Activity.RESULT_OK) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "admin not granted, zero-ing bedtime protection mode");
                }
                resetConstantPINVars();
                bedtimeMode = 0;
                iqd.setSettingsTempLongVal(BEDTIME_PROTECTION_SETTING, bedtimeMode);
                cancelConstantPinItems();
                bedtimeModeSwitch.setChecked(false);

            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "admin granted, setting up bedtime protection mode");
                }
                resetConstantPINVars();
                bedtimeMode = 1;
                iqd.setSettingsTempLongVal(BEDTIME_PROTECTION_SETTING, bedtimeMode);
                scheduleConstantPinChargingTriggerJob();
                constantPinHelper constHelp = new constantPinHelper(this);
                constHelp.activateConstantPINServiceIfNeeded(this, iqd);
            }
        } else if (requestCode == PIN_ONLY_AUTH_REQUEST_FOR_ACTIVITY) {
            if (resultCode == RESULT_OK) {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user used PIN-only for auth. Success. isOnboarding: " + isOnboarding);
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "bumping auth timeout");
                }
                iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, SystemClock.elapsedRealtime() + 300000);
                if (isOnboarding) {
                    auth.authOnboardingResult(true, false);
                }
            } else {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user used PIN-only for auth. No auth success. isOnboarding: " + isOnboarding);
                }

                if (!isOnboarding) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "set auth grace to 0 after auth fail, ensures was not overridden elsewhere by this scatterbrain code");
                    }
                    iqd.setSettingsTempLongVal(REAUTH_TRIGGER_TIME, 0);
                    finishAffinity();
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "onboarding fail via old PIN-only mode");
                    }
                    auth.authOnboardingResult(false, false);
                }
            }
            //ensure vars are reset
            isOnboarding = false;
            isAuthenticating = false;
        }
    }


    private void launchOtherActivity(Class<?> chosenClass) {
        Intent intent = new Intent(this, chosenClass);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        auth.adjustTimeoutForPresence(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        isAuthenticating = false;
        auth.adjustTimeoutForPresence(false);
        if (retrievedSettingsVars == 0){
            //if the user changed permissions while on this screen, so the app was killed. Get the settings again, and have a merry cache miss.
            if (isDebuggable) {
                Log.d(TAG, "Static settings vars are empty, activities may have been destroyed to to permission alteration outside of this app (*shakes fist*). Getting the settings vals.");
            }
            dontLockTwiceSavedVal = (int) iqd.getSettingsTempLongVal(DONT_LOCK_TWICE_WITHIN);
            delayTaskSpinnerDelayMultiplierSavedVal = (int) iqd.getSettingsTempLongVal(TASKER_JOB_DELAY);
            dontLockIfCharging = (int) iqd.getSettingsTempLongVal(DONT_LOCK_IF_CHARGING);
            postSnoozeLockCheck = (int) iqd.getSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK);
            lockIfSIMRemoved = (int) iqd.getSettingsTempLongVal(LOCK_IF_SIM_REMOVED);
            lockIfCableRemoved = (int) iqd.getSettingsTempLongVal(LOCK_IF_CABLE_REMOVED);
            startConstantPinIfLeaveSnoozeWifi = (int) iqd.getSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE);
            requireBiometricAuth = (int) iqd.getSettingsTempLongVal(REQUIRE_BIOMETRIC_AUTH_USE_APP);
            delayLockSavedVal = (int) iqd.getSettingsTempLongVal(DELAY_BEFORE_LOCK);
            bedtimeMode = (int) iqd.getSettingsTempLongVal(BEDTIME_PROTECTION_SETTING);
            multiWatchMode = (int) iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE);
            buttonRemap = (int) iqd.getSettingsTempLongVal(USE_LAUNCHER_SHORTCUTS_FOR_BUTTON_REMAP);
            requirePinPassOnlyForAuth = (int) iqd.getSettingsTempLongVal(ONLY_USE_PIN_PASS_FOR_RL_AUTH);
            useWearApis = (int) iqd.getSettingsTempLongVal(USE_WEAR_APIS);
            retrievedSettingsVars = 1;
        }

        setUpUI();

        long alarmClockTime = 0;
        try {
            AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                AlarmManager.AlarmClockInfo alarmClockInfo = alarmManager.getNextAlarmClock();
                alarmClockTime = alarmClockInfo.getTriggerTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                String timeFormatted = sdf.format(new Date(alarmClockTime));
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "next alarm time: " + alarmClockTime + ", " + timeFormatted);
                }
                if (alarmClockTime != 0) {
                    alarmTimeText.setText(timeFormatted);
                } else{
                    alarmTimeText.setText(getApplicationContext().getResources().getString(R.string.unset_alarm_time));
                }
            } else{
                alarmTimeText.setText(getApplicationContext().getResources().getString(R.string.unset_alarm_time));
            }
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "no alarm time set, so it crashed the alarm time check");
            }
            alarmTimeText.setText(getApplicationContext().getResources().getString(R.string.unset_alarm_time));
        }

        authBiometricsIfEnabled();

    }

    private void authBiometricsIfEnabled() {
        if (requireBiometricAuth == 1 && SystemClock.elapsedRealtime() > iqd.getSettingsTempLongVal(REAUTH_TRIGGER_TIME)) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user must authenticate if < 5 minutes of reauth timeout");
            }
            auth.authenticate(true, false, 0);
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "user doesn't use biometrics or is within 5 minute auth grace period");
            }
        }
    }


}
