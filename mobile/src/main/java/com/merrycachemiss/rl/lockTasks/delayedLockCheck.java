/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.lockTasks;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;

public final class delayedLockCheck {

    final String TAG = "alarmDelayedLockCheck";
//    public static final int DELAYED_LOCK_ALARM_REQ_CODE = 8008;
//    @Override
//    public void onReceive(Context context, Intent alarmIntent) {
//        if (BuildConfig.DEBUG){
//            Log.d("delayedLockAlarmSetup", "received scheduled alarm, broadcasting com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE");
//        }
//
//        Intent lockCheckForService = new Intent("com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE");
//        lockCheckForService.putExtra("fromAlarm", 1);
//        context.sendBroadcast(lockCheckForService, "com.merrycachemiss.rl");
//    }


    boolean lockingDeviceHasReturnedWithinDelayTime(Context context, long lockDelay, String btDeviceSerial, long disconnectedTime) {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "queuing delayed lock process and starting a backup/redundant lock retry jobschedule");
        }
        //AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //Intent intent = new Intent(context, AlarmDelayedLockOption.class);
        //PendingIntent alarmIntent = PendingIntent.getBroadcast(context, DELAYED_LOCK_ALARM_REQ_CODE, intent, PendingIntent.FLAG_IMMUTABLE);

        /*
        *
        * if (connected = 1 AND updateTime > disconnectTime)
        *   return TRUE
        *
        *
        * */

        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
        final String[] connectedSelectionArgs = {btDeviceSerial, "1"};
        //get last connected time of particular BT device with connected=1
        //focusing on time, in addition to connected, as sometimes the wear API (maybe) makes it think that something's still connected, according to
        //some logic you have elsewhere to have a failsafe against erroneous disconnection lock.
        //Maybe - it's been a few years and running out of time for 2024 update window, investigate later.
        final long deviceLastConnectedTime = iqd.connectedBTDeviceQueryUpdateTime(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);

        if (BuildConfig.DEBUG){
            Log.d(TAG, "deviceLastConnectedTime: " + deviceLastConnectedTime + " disconnectedTime: " + disconnectedTime
            + " SystemClock.elapsedRealtime(): " + SystemClock.elapsedRealtime());
        }
        //the "&& deviceLastConnectedTime < SystemClock.elapsedRealtime()" also confirms that the device connected during an expected period.
        //this check is in case the phone shutdown unexpectedly or etc, and the app never got to reset connection states, and the app's service didn't
        //spawn in time at bootup, thus didn't capture the connected event.
        //new note in above: changed disconnect events to always set "updt_dt_tm" to "0" if disconnected. So, may not need to be so harsh but whatever, I'm in a rush to finish
        //this before the SDK deadline!
        return (deviceLastConnectedTime > disconnectedTime && deviceLastConnectedTime < SystemClock.elapsedRealtime());

//        long delayPeriod = SystemClock.elapsedRealtime() + ((10 + (5 * lockDelay)) * 1000);// ex- user chose 25 sec, so value of 3: 10 + (5 x 3) = 25.
//        long alarmExpiryDate = System.currentTimeMillis() + ((10 + (5 * lockDelay)) * 1000);
//        if (alarmExpiryDate >= iqd.getSettingsTempLongVal(DELAY_BEFORE_LOCK_EXPIRY)) {
//
//            try {
//                if (android.os.Build.VERSION.SDK_INT >= 23) {
//                    if (alarmManager != null) {
//                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                                delayPeriod, alarmIntent);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                            if (!alarmManager.canScheduleExactAlarms()) {
//                                permissionHelper perm = new permissionHelper(context);
//                                perm.surfaceNotificationForMissingPerms();
//                            }
//                        }
//                        if (BuildConfig.DEBUG) {
//                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
//                            String delayTimeFormatted = sdf.format(new Date(alarmExpiryDate));
//                            Log.d("delayedLockAlarmSetup", "delayed to: " + delayTimeFormatted);
//                        }
//                        success = true;
//                    } else {
//                        if (BuildConfig.DEBUG) {
//                            Log.d("delayedLockAlarmSetup", "failed to create alarm");
//                        }
//                        alarmExpiryDate = 0;
//                    }
//                } else {
//                    if (alarmManager != null) {
//                        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                                delayPeriod, alarmIntent);
//                        success = true;
//
//                    } else {
//                        if (BuildConfig.DEBUG) {
//                            Log.d("delayedLockAlarmSetup", "failed to create alarm");
//                        }
//                        alarmExpiryDate = 0;
//                    }
//                }
//            } catch (Exception e) {
//                if (BuildConfig.DEBUG) {
//                    Log.e("delayedLockAlarmSetup", "failed to create alarm", e);
//                }
//                alarmExpiryDate = 0;
//                success = false;
//            }
//
//
//            if (BuildConfig.DEBUG) {
//                Log.d("delayedLockAlarmSetup", "create a backup jobschedule in addition to alarm (or if alarm failed to create)");
//            }
//            try {
//                scheduleBackupLockJob(context);
//            } catch (Exception e){
//                if (BuildConfig.DEBUG) {
//                    Log.e("delayedLockAlarmSetup", "failed to create backup job in addition to alarm", e);
//                }
//            }
//
//
//            iqd.setSettingsTempLongVal(DELAY_BEFORE_LOCK_EXPIRY, alarmExpiryDate);
//        } else {
//            if (BuildConfig.DEBUG) {
//                Log.d("delayedLockAlarmSetup", "alarm was already scheduled. Didn't make a new one, and won't lock.");
//            }
//
//            success = true;
//        }
//
//        return success;

    }

//    public static void cancelDelayedLock(Context context) {
//        if (BuildConfig.DEBUG){
//            Log.d("delayedLockAlarmSetup", "cancelling alarm");
//        }
//        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(context, AlarmDelayedLockOption.class);
//        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, DELAYED_LOCK_ALARM_REQ_CODE, intent, PendingIntent.FLAG_IMMUTABLE);
//
//        if (alarmMgr!= null) {
//            alarmMgr.cancel(alarmIntent);
//            rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
//            iqd.setSettingsTempLongVal(DELAY_BEFORE_LOCK_EXPIRY, 0);
//        }
//
//    }

//    private static void scheduleBackupLockJob(Context context){
//
//        if (BuildConfig.DEBUG) {
//            Log.d("delayedLockAlarmSetup",
//            "try to set up lock retry jobscheduler from alarm creation area as backup in case alarm fails to fire on certain devices. NON CANCELLABLE NOTIF IS MADE.");
//        }
//        ComponentName componentName = new ComponentName(context, postSnoozeLockCheckJob.class);
//        JobInfo jobInfo = new JobInfo.Builder(RELOCK_JOB_ID, componentName)
//                .setMinimumLatency((15 * 60 * 1000))//15 MIN
//                .setOverrideDeadline((16 * 60 * 1000))//16 MIN
//                //.setPeriodic((15 * 60 * 1000))//16 MIN
//                .setPersisted(false)
//                .setBackoffCriteria((15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
//                .build();
//        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(JOB_SCHEDULER_SERVICE);
//        int resultCode = 0;
//        if (jobScheduler != null) {
//            resultCode = jobScheduler.schedule(jobInfo);
//            if (resultCode == JobScheduler.RESULT_SUCCESS) {
//                if (BuildConfig.DEBUG) {
//                    Log.d("delayedLockAlarmSetup", "Job schedule success scheduleBackupLockJob()");
//                }
//                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
//                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 1);
//
//
//                try {
//
//                    if (BuildConfig.DEBUG) {
//                        Log.d("delayedLockAlarmSetup", "set up notif for backup lock via jobschedule after alarm was made");
//                    }
//                    //if ()
//                    String lockScheduleNoficDispTitle = context.getResources().getString(R.string.schedule_lock_title);
//                    //String lockScheduleNoficDispMessage = context.getResources().getString(R.string.scheduled_lock_descr) + " 15 " + context.getString(R.string.minutes);
//                    NotificationHelper notificationHelper = new NotificationHelper(context);
//                    notificationHelper.createNotifChannelOreo("scheduled_events", context.getResources().getString(R.string.scheduled_events),
//                            3, false);
//                    NotificationCompat.Builder builder = notificationHelper.getScheduledItemNotificationBuilder(lockScheduleNoficDispTitle, "",
//                            R.drawable.ic_access_time_black_24dp, NOTIF_ID_CANCEL_SCHEDULED_LOCK, false);
//                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
//                    notificationManager.notify(NOTIF_ID_CANCEL_SCHEDULED_LOCK, builder.build());
//                } catch (Exception e) {
//                    if (BuildConfig.DEBUG) {
//                        Log.e("delayedLockAlarmSetup", "failed to create cancellable notif", e);
//                    }
//                }
//
//            } else {
//                if (BuildConfig.DEBUG) {
//                    Log.d("delayedLockAlarmSetup", "Job schedule fail scheduleBackupLockJob()");
//                }
//                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
//                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
//
//            }
//        } else {
//            if (BuildConfig.DEBUG) {
//                Log.d("delayedLockAlarmSetup", "Job schedule fail: constant PIN scheduleBackupLockJob(). Jobscheduler is null.");
//            }
//        }
//    }


}
