/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.lockTasks;

import android.app.admin.DevicePolicyManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.jobs.postSnoozeLockCheckJob;
import com.merrycachemiss.rl.jobs.taskDelayExecuteJob;
import com.merrycachemiss.rl.receivers.DeviceAdmin;
import com.merrycachemiss.rl.util.NotificationHelper;
import com.merrycachemiss.rl.tasker.getTaskerProjectsTasks;
import com.merrycachemiss.rl.util.getSetCurrentWIFISSID;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.JOB_SCHEDULER_SERVICE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DELAY_BEFORE_LOCK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DELAY_BEFORE_LOCK_EXPIRY;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_IF_CHARGING;
import static com.merrycachemiss.rl.dataStorage.dataConstants.DONT_LOCK_TWICE_WITHIN;
import static com.merrycachemiss.rl.dataStorage.dataConstants.PHONE_LOCKING_ENABLED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.POST_SNOOZE_LOCK_CHECK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.SNOOZE_STOP_TIME;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_DELAY;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.rldbContract.SerialProjection.SERIAL_PROJECTION;

import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_TASK;
import static com.merrycachemiss.rl.util.NotificationHelper.SCHEDULED_EVENTS;
import static com.merrycachemiss.rl.util.NotificationHelper.clearNotification;

public final class lockDecider {
    
    private rldbInsertQueryDelete iqd;
    private DevicePolicyManager devicePolicyManager;
    private boolean has_admin_rights;

    private static final String TAG = "lockDecider";

    public lockDecider(){
    }

    public void lockIfValid(Context context, String btDeviceSerial, String btDeviceName, int btOffSimPowerRemoved, int postSnoozeLockCheck, int fromAlarm, long currentTime) {
        //set up database, admin polling vars/objects
        devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName deviceAdminComponentName = new ComponentName(context, DeviceAdmin.class);

        iqd = new rldbInsertQueryDelete(context.getApplicationContext());


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "check if has admin");
        }
        
        try {
            has_admin_rights = devicePolicyManager.isAdminActive(deviceAdminComponentName);
        } catch (Exception e){
            Log.d(TAG, "failed to check if has admin, devicePolicyManager could be null or maybe we're in a direct boot state and life is a simulation");
        }
        long phone_locking_enabled = iqd.getSettingsTempLongVal(PHONE_LOCKING_ENABLED);

        if (has_admin_rights && phone_locking_enabled == 1) {
            //if (postSnoozeLockCheckJob == 0) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "postSnoozeLockCheck: " + postSnoozeLockCheck + " btOffSimPowerRemoved: " + btOffSimPowerRemoved + " fromAlarm: " + fromAlarm);
            }


            if (BuildConfig.DEBUG) {
                Log.d(TAG, "check serial if monitored");
            }
            boolean disconnectedBTdeviceIsMonitored = false;//this prevents some internal DB crashes when doing lock checks when BT is off or doing post snooze lock check
            if (btOffSimPowerRemoved == 0 && postSnoozeLockCheck == 0 && fromAlarm == 0){
                final String[] selectionArgs = {btDeviceSerial};
                disconnectedBTdeviceIsMonitored = iqd.genericSingleItemExistsQuery(rldbContract.MonitoredBTQuery.CONTENT_URI, SERIAL_PROJECTION, selectionArgs);


                if (disconnectedBTdeviceIsMonitored && iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE) == 1){
                    //new setting for only locking if no other locking wearables are known to be connected
                    boolean anotherMonitoredDeviceConnected = iqd.genericSingleItemExistsQuery(rldbContract.ConnectedMonitoredBTQuery.CONTENT_URI, null, null);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Monitored device disconnected. currently, anotherMonitoredDeviceConnected: "  + anotherMonitoredDeviceConnected);
                    }
                    if (anotherMonitoredDeviceConnected){
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "since another device is still connected and user is using multi-device mode, not locking");
                        }
//                    if (iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE_QUEUE_LATER_LOCK) == 1){
//                        if (BuildConfig.DEBUG) {
//                            Log.d(TAG, "queing up a lock since MULTI_DEVICE_MODE_QUEUE_LATER_LOCK is on");
//                        }
//
//                    }

                        return;//don't do anything more, if user is a multi-watch user and they have another locking device currently connected.
                    }
                }
                //^ multi-watch check. might need to make its own thing though, as you will need the query to prove that it's > 1 for the new sub-option

                //used for constant PIN related stuff for when watch is missing and user wants constant PIN mode because of it
                iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED, 1);
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "lock check triggered from scheduled lock or bt off or sim/power disconnect");
                }
            }

            boolean hasMonitoredBTdevicesStored = iqd.genericSingleItemExistsQuery(rldbContract.HasAnyMonitoredBTQuery.CONTENT_URI, null, null);

            if ((disconnectedBTdeviceIsMonitored || (btOffSimPowerRemoved == 1 && hasMonitoredBTdevicesStored) || btOffSimPowerRemoved >= 2 || postSnoozeLockCheck == 1 || fromAlarm == 1)) {

            /*
            delayed lock option in case of wearable return:
            if DELAY_BEFORE_LOCK = 1 && disconnectedBTdeviceIsMonitored && fromAlarm = 0,
                -set alarm
                -RETURN.

            if none of the above is true, it will proceed as normal
            */

                long lockDelay = ((10 + (5 * iqd.getSettingsTempLongVal(DELAY_BEFORE_LOCK))) * 1000);
                //enable the last commented out thing when the setting is on
                if (disconnectedBTdeviceIsMonitored && lockDelay != 0) {

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "user has the lock delay option, creating a delay to see if the device came back (haunted) during the delay period. " +
                                "If it came back during this period, don't lock. Awaiting for user's set time: " + lockDelay);
                    }
                    //create a unique notification ID for delayed lock, in case there are stacked delays happening and you don't want one to cancel the other.
                    //keeping NOTIF_ID_CANCEL_SCHEDULED_LOCK as part of the number, for searchability and personal context during development, so decrease the possible max
                    //by the proper amount, to not overflow the int
                    int lockDelayNotifIdUnique = (int)(Math.random() * Integer.MAX_VALUE)-NOTIF_ID_CANCEL_SCHEDULED_LOCK;
                    try {
                        //put a delay here, to await for the queueDelayedLock period
                        //also in here, post that notification that the lock delay is happening
                        Log.d(TAG, "try to post notif about locking delay period, which is " + lockDelay + " ms");
                        try {
                            String lockScheduleNoficDispTitle = context.getResources().getString(R.string.schedule_lock_title);
                            NotificationHelper notificationHelper = new NotificationHelper(context);
                            notificationHelper.createNotif(lockScheduleNoficDispTitle, "", "scheduled_events", context.getResources().getString(R.string.scheduled_events),
                                    false, 3, R.drawable.ic_access_time_black_24dp, NOTIF_ID_CANCEL_SCHEDULED_LOCK+ lockDelayNotifIdUnique, false);

                        } catch (Exception e) {
                            Log.e(TAG, "failed to post notif about locking delay period", e);
                        }
                        Thread.sleep(lockDelay);
                        delayedLockCheck delayCheck = new delayedLockCheck();
                        if (delayCheck.lockingDeviceHasReturnedWithinDelayTime(context, lockDelay, btDeviceSerial, currentTime)) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "device " + btDeviceSerial + " returned within locking delay period");
                            }

                            clearNotification(context, TAG, NOTIF_ID_CANCEL_SCHEDULED_LOCK+lockDelayNotifIdUnique);
                            return;//the individual device was found to be reconnected within the user's set delay period.
                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "device " + btDeviceSerial + " DID NOT RETURN within locking delay period");
                            }
                        }

                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to schedule alarm", e);
                        }

                    }

                    clearNotification(context, TAG, NOTIF_ID_CANCEL_SCHEDULED_LOCK+lockDelayNotifIdUnique);

                }

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "proceeding with lock, since alarm option is not used or this was called either from alarm/BT off/SIM removed. Avoided scheduling new alarm.");
                }

                if (fromAlarm == 1) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "try to cancel alarm after receiving fromAlarm==1");
                    }
                    try{
                        //cancelDelayedLock(context);
                        Log.d(TAG, "try to cancel job set by alarm - new one will be made if still snoozing");

                        try {
                            jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                            cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                            cancelClear.cancelJobClearNotif();
                        } catch (Exception e) {
                            Log.d(TAG, "failed to cancel job set by alarm");

                        }
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to cancel alarm after receiving fromAlarm==1", e);
                        }

                    }
                }
                long curTime = System.currentTimeMillis();
                long snoozeStopTimeRetrieved = iqd.getSettingsTempLongVal(SNOOZE_STOP_TIME);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                String curTimeFormatted = sdf.format(new Date(curTime));

                //wifi probably used most often.
                if (currentlyOnWIFISnoozeSSIDCheck(context)) {
                    didntLockSoRescheduleIfNeeded("WIFI", context, false, fromAlarm);
                    return;
                }

                if (iqd.genericSingleItemExistsQuery(rldbContract.ConnectedSafeBTQuery.CONTENT_URI, null, null)) {
                    didntLockSoRescheduleIfNeeded("BT", context, false, fromAlarm);
                    return;
                }
                if (isCharging(context) && iqd.getSettingsTempLongVal(DONT_LOCK_IF_CHARGING) == 1) {
                    didntLockSoRescheduleIfNeeded("charging", context, false, fromAlarm);
                    return;
                }


                if (curTime < snoozeStopTimeRetrieved) {
                    didntLockSoRescheduleIfNeeded("snooze stop " + snoozeStopTimeRetrieved + " - " + sdf.format(new Date(snoozeStopTimeRetrieved)) + " curtime: "
                            + curTimeFormatted, context, true, fromAlarm);
                    return;
                }



                try {

                    devicePolicyManager.lockNow();

                    iqd.setSettingsTempLongVal(DELAY_BEFORE_LOCK_EXPIRY, 0);//reset alarm expiry date

                    long xxxxSecondsIntoFuture = curTime + 1250;//always have 1.25 second double-lock prevention

                    int dontLockTwiceWithinVal = (int) iqd.getSettingsTempLongVal(DONT_LOCK_TWICE_WITHIN);//sPrefs.getSharedPrefsInt("dont_lock_twice_within");
                    switch (dontLockTwiceWithinVal) {
                        case 0:
                            //user chose DISABLED, so break out of checking this
                            break;
                        case 1:
                            Log.d(TAG, "dont_lock_twice_within was set to 1 (5 sec), so setting snooze to this");
                            xxxxSecondsIntoFuture = curTime + 5000;
                            break;
                        case 2:
                            Log.d(TAG, "dont_lock_twice_within was set to 2 (10 sec), so setting snooze to this");
                            xxxxSecondsIntoFuture = curTime + 10000;
                            break;
                        case 3:
                            Log.d(TAG, "dont_lock_twice_within was set to 3 (15 sec), so setting snooze to this");
                            xxxxSecondsIntoFuture = curTime + 15000;
                            break;
                        case 4:
                            Log.d(TAG, "dont_lock_twice_within was set to 4 (20 sec), so setting snooze to this");
                            xxxxSecondsIntoFuture = curTime + 20000;
                            break;
                    }


                    if (btOffSimPowerRemoved == 0 && postSnoozeLockCheck == 0 && fromAlarm == 0) {
                        Log.d(TAG, "**RL LOCKED because**  " + btDeviceName + " disconnected " + " and user's dontLockTwiceWithinVal set to " + dontLockTwiceWithinVal);

                    } else if (fromAlarm == 1){
                        Log.d(TAG, "**RL LOCKED because** from alarm");

                        //add 13 seconds to current time for when BT off
                        //xxxxSecondsIntoFuture = curTime + 13000;
                    } else if (btOffSimPowerRemoved == 1){
                        Log.d(TAG, "**RL LOCKED because** BT turned off");

                        //add 10 seconds to current time for when BT off
                        xxxxSecondsIntoFuture = curTime + 10000;
                    } else if (btOffSimPowerRemoved == 2){
                        Log.d(TAG, "**RL LOCKED because** SIM removed");
                    } else if (btOffSimPowerRemoved == 3){
                        Log.d(TAG, "**RL LOCKED because** power cable removed");

                    } else {
                        Log.d(TAG, "**RL LOCKED because** did post snooze lock check");

                    }

                    //only set snoozeStopTimeRetrieved if it's less than projected xxxxSecondsIntoFuture
                    //if xxxxSecondsIntoFuture was set because of device ACL disconnect, then it will be set here. Default is 0 if user isn't using this option.
                    //if xxxxSecondsIntoFuture was set because of BT off, then it will be set here.
                    if (xxxxSecondsIntoFuture > snoozeStopTimeRetrieved) {
                        iqd.setSettingsTempLongVal(SNOOZE_STOP_TIME, xxxxSecondsIntoFuture);//sPrefs.setSharedPrefsLong("snooze_stop_time", xxxxSecondsIntoFuture);
                        snoozeStopTimeRetrieved = iqd.getSettingsTempLongVal(SNOOZE_STOP_TIME);//snoozeStopTimeRetrieved = sPrefs.getSharedPrefsLong("snooze_stop_time");
                        Log.d(TAG, "curtime is " + curTimeFormatted +
                                " snoozeStopTimeRetrieved is now: " + snoozeStopTimeRetrieved + " " + sdf.format(new Date(snoozeStopTimeRetrieved)));
                    } else{
                        Log.d(TAG, "curtime is " + curTimeFormatted +
                                " snoozeStopTimeRetrieved not overriden, was " + snoozeStopTimeRetrieved + " " + sdf.format(new Date(snoozeStopTimeRetrieved)));
                    }

                    Log.d(TAG, "try to cancel job after locking phone");

                    try {
                        jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                        cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                        cancelClear.cancelJobClearNotif();
                    } catch (Exception e) {
                        Log.d(TAG, "failed to cancel job after a locking phone");

                    }

                    try{

                        if (!Objects.equals(iqd.getSettingsTempStringVal(TASKER_TASK), "")) {
                            executeOrScheduleTaskerTask(context, (int) iqd.getSettingsTempLongVal(TASKER_JOB_DELAY));
                        } else{
                            Log.d(TAG, "no tasker task chosen");
                        }
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to execute Tasker task or schedule task job", e);
                        }

                    }


                } catch (Exception e){


                    Log.e(TAG, "admin couldnt lock: ", e);
                }

            }

        } else {
            String adminAndLockStatus = has_admin_rights + " || " + phone_locking_enabled;
            Log.d(TAG, adminAndLockStatus);
        }
    }

    private void didntLockSoRescheduleIfNeeded(String reason, Context context, boolean becauseSnoozedTimer, int fromAlarm){
        Log.d(TAG, "**RL didn't lock because** - " + reason);
        

        if (iqd.getSettingsTempLongVal(POST_SNOOZE_LOCK_CHECK) == 1 && iqd.getSettingsTempLongVal(RELOCK_JOB_SCHEDULED) == 0
                && !becauseSnoozedTimer){
            Log.d(TAG, "relocking scheduled");
            try {
                schedulePostSnoozeLockCheck(context, fromAlarm);
                Log.d(TAG, "relock is scheduled");
            } catch (Exception e){
                Log.d(TAG, "failed to schedule relock");
                
            }
        }
    }

    private Boolean currentlyOnWIFISnoozeSSIDCheck(Context context){
        getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(context, TAG);
        return wifiSSIDGetSet.currentlyOnWIFISnoozeSSID(iqd, wifiSSIDGetSet.getCurrentWIFISSID());

    }

    private boolean isCharging(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.getApplicationContext().registerReceiver(null, ifilter);
        // Are we charging / charged?
        if (batteryStatus != null) {
            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
            boolean wirelessCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS;
            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean charging = status == BatteryManager.BATTERY_STATUS_CHARGING;
            if (BuildConfig.DEBUG) {
                boolean battery_full = status == BatteryManager.BATTERY_STATUS_FULL;
                Log.d(TAG, "usbCharge: " + usbCharge + " acCharge: " + acCharge + " charging: " + charging + " battery_full: " + battery_full);
            }
            return  charging || usbCharge || acCharge || wirelessCharge;// || ((usbCharge || acCharge) && battery_full);
        }
        return false;
    }

    private void schedulePostSnoozeLockCheck(Context context, int fromAlarm){
        if (fromAlarm == 1) {
            Log.d(TAG, "sleeping for 1.5 second since this came from an alarm in which another job was cancelled.");
            try {
                Thread.sleep(1500);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to sleep for 1.5 second before setting lock retry job");
                }
            }
        }
        Log.d(TAG, "try to set up lock retry jobscheduler from lockDecider schedulePostSnoozeLockCheck()");
        ComponentName componentName = new ComponentName(context, postSnoozeLockCheckJob.class);
        JobInfo jobInfo = new JobInfo.Builder(RELOCK_JOB_ID, componentName)
                .setMinimumLatency((15 * 60 * 1000))//15 MIN
                .setOverrideDeadline((16 * 60 * 1000))//16 MIN
                //.setPeriodic((15 * 60 * 1000))//16 MIN
                .setPersisted(false)
                .setBackoffCriteria((15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
                .build();
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = 0;
        if (jobScheduler != null) {
            resultCode = jobScheduler.schedule(jobInfo);
            if (resultCode == JobScheduler.RESULT_SUCCESS) {
                Log.d(TAG, "Job schedule success schedulePostSnoozeLockCheck()");
                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 1);


                String lockScheduleNoficDispTitle = context.getResources().getString(R.string.schedule_lock_title);
                String lockScheduleNoficDispMessage = context.getResources().getString(R.string.scheduled_lock_descr);
                String channelNameLocalized = context.getResources().getString(R.string.scheduled_events);
                NotificationHelper notificationHelper = new NotificationHelper(context);
                notificationHelper.createNotif(lockScheduleNoficDispTitle, lockScheduleNoficDispMessage, SCHEDULED_EVENTS, channelNameLocalized, false,
                        3, R.drawable.ic_access_time_black_24dp, NOTIF_ID_CANCEL_SCHEDULED_LOCK, true);


            } else {
                Log.d(TAG, "Job schedule fail schedulePostSnoozeLockCheck()");
                iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);

            }
        } else {
            Log.d(TAG, "Job schedule fail schedulePostSnoozeLockCheck(). Jobscheduler was null.");
            iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);

        }
    }

    private void executeOrScheduleTaskerTask(Context context, int multiplier){
        getTaskerProjectsTasks tasker = new getTaskerProjectsTasks(context);

        if (multiplier > 0 && Objects.equals(tasker.testTaskerCapability(), "RL")) {
            //if user chose delay period and tasker is functional, then schedule it.
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "try to set up delayed task jobscheduler from lockDecider executeOrScheduleTaskerTask()");
            }
            ComponentName componentName = new ComponentName(context, taskDelayExecuteJob.class);
            JobInfo jobInfo = new JobInfo.Builder(TASKER_TASK_JOB_ID, componentName)//LIFE BEGAN
                    .setMinimumLatency(((long) multiplier * 15 * 60 * 1000))//15 MIN
                    .setOverrideDeadline(((long) multiplier * 16 * 60 * 1000))//16 MIN
                    .setPersisted(true)
                    .setBackoffCriteria(((long) multiplier * 15 * 60 * 1000), JobInfo.BACKOFF_POLICY_LINEAR)
                    .build();
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
            int resultCode = 0;
            if (jobScheduler != null) {
                resultCode = jobScheduler.schedule(jobInfo);
                if (resultCode == JobScheduler.RESULT_SUCCESS) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Job schedule success executeOrScheduleTaskerTask()");
                    }
                    iqd.setSettingsTempLongVal(TASKER_JOB_SCHEDULED, 1);


                    String lockScheduleNoficDispTitle = context.getResources().getString(R.string.schedule_tasker_title);
                    String lockScheduleNoficDispMessage = context.getResources().getString(R.string.scheduled_tasker_descr).replace("_TIME_", String.valueOf(multiplier * 15));
                    String channelNameLocalized = context.getResources().getString(R.string.scheduled_events);

                    NotificationHelper notificationHelper = new NotificationHelper(context);
                    notificationHelper.createNotif(lockScheduleNoficDispTitle, lockScheduleNoficDispMessage, SCHEDULED_EVENTS, channelNameLocalized, false,
                            3, R.drawable.ic_access_time_black_24dp, NOTIF_ID_CANCEL_SCHEDULED_TASK, true);



                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Job schedule fail executeOrScheduleTaskerTask()");
                    }
                    iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                }

            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "user isn't using delayed tasker task executions (time multiplier = 0), so executing it NOW");
                }
                //tasker exec code here

                try {

                    tasker.executeTask(iqd.getSettingsTempStringVal(TASKER_TASK), false);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to exec task after lock", e);
                    }

                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Job schedule fail executeOrScheduleTaskerTask(). Jobscheduler was null.");
            }
            iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
        }

    }


}
