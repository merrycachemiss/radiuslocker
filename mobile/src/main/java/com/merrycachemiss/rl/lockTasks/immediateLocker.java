/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.lockTasks;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.jobs.jobCancelClearNotif;
import com.merrycachemiss.rl.receivers.DeviceAdmin;
import com.merrycachemiss.rl.tasker.getTaskerProjectsTasks;
import com.merrycachemiss.rl.util.permissionHelper;

import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_ID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.TASKER_TASK;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_CANCEL_SCHEDULED_LOCK;

public final class immediateLocker {

    private final String TAG = "immediateLocker";
    private final String callingClass;
    private final Context context;
    final boolean doTaskerTask;
    final boolean fromConstantPINmode;
    private String status;//0 = unknown fail, 1 == success, 2 == fail because no admin. Mainly used for Wear app toasts to user
    private final DevicePolicyManager devicePolicyManager;
    private final ComponentName deviceAdminComponentName;

    public immediateLocker(Context context, boolean doTaskerTask, boolean fromConstantPINmode, String callingClassOrTAG){
        this.context = context;
        this.doTaskerTask = doTaskerTask;
        this.fromConstantPINmode = fromConstantPINmode;
        this.callingClass = callingClassOrTAG;
        devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdminComponentName = new ComponentName(context, DeviceAdmin.class);
    }

    public void lockNOW(){
        boolean has_admin_rights = false;
        try {
            if (devicePolicyManager != null) {
                has_admin_rights = devicePolicyManager.isAdminActive(deviceAdminComponentName);
            }
        } catch (Exception e){
            Log.d(TAG, "failed to check if has admin, devicePolicyManager likely null. from " + callingClass);
            status = "2";
            return;
        }

        if (has_admin_rights){
            try {
                devicePolicyManager.lockNow();
                if (!fromConstantPINmode) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "try to cancel job after locking phone, since not from constant PIN mode. from " + callingClass);
                    }
                    try {
                        jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                        cancelClear.setJobAndNotifParams(RELOCK_JOB_ID, NOTIF_ID_CANCEL_SCHEDULED_LOCK, RELOCK_JOB_SCHEDULED);
                        cancelClear.cancelJobClearNotif();
                    } catch (Exception e) {
                        Log.d(TAG, "failed to cancel job after a locking phone. from " + callingClass);
                    }

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "try to cancel alarm after lock. from " + callingClass);
                    }
//                    try {
//                        cancelDelayedLock(context);
//                    } catch (Exception e) {
//                        if (BuildConfig.DEBUG) {
//                            Log.d(TAG, "failed to cancel alarm after immediate lock. from " + callingClass, e);
//                        }
//                    }

                    if (!doTaskerTask) {
                        status = "1";
                        return;
                    }
                } else {//fromConstantPINmode
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "Even if CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID is running, the constant PIN heartbeat job or something else separately found that it's time to lock. Cancelling that other job with job id CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID.");
                    }
                    try {
                        jobCancelClearNotif cancelClear = new jobCancelClearNotif(context, TAG);
                        cancelClear.setJobAndNotifParams(CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID, 0, 0);
                        cancelClear.cancelJobClearNotif();
                    } catch (Exception e){
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to cancel job with CONSTANT_PIN_POST_PAUSE_LOCK_JOB_ID", e);
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "lock triggered via constant PIN mode. from " + callingClass);
                    }
                }

            } catch (Exception e){
                Log.d(TAG, "admin couldnt lock. from " + callingClass, e);
            }


            if (doTaskerTask && !fromConstantPINmode){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "execute tasker task NOW. from " + callingClass);
                }
                getTaskerProjectsTasks tasker = new getTaskerProjectsTasks(context);
                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context.getApplicationContext());
                try {
                    tasker.executeTask(iqd.getSettingsTempStringVal(TASKER_TASK), false);
                    tasker.unregisterRec();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to exec task after immediate lock. from " + callingClass, e);
                    }
                }

                status = "1";
                return;
            }


        } else {
            Log.d(TAG, "admin rights not granted. from " + callingClass);
            permissionHelper perm = new permissionHelper(context);
            perm.surfaceNotificationForMissingPerms();
            status = "2";
            return;
        }

        status = "0";
    }


    public String getLockStatus(){
        return status;
    }

}
