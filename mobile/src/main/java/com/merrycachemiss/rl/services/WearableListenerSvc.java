/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.services;

import static com.merrycachemiss.rl.dataStorage.dataConstants.USE_WEAR_APIS;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.util.connDisconnTasks;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public final class WearableListenerSvc extends WearableListenerService {

    final String TAG = "WearableListenerSvc";
    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/lock_phone")) {
            String messageReceived = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "message " + messageReceived + " received from " + messageEvent.getSourceNodeId() + " to path " +
                        messageEvent.getPath());
            }
            switch (messageReceived) {
                case "1":
                    executeLock(messageEvent, false);
                    break;
                case "2":
                    executeLock(messageEvent, true);
                    break;
                case "3":
                    startConstantPIN(messageEvent.getSourceNodeId());
                    break;
            }
        }
    }


    private void executeLock(final MessageEvent messageEvent, boolean doTaskerTask){
        immediateLocker lock = new immediateLocker(this, doTaskerTask, false, "WearableListenerSvc");
        lock.lockNOW();
        String lockResult = lock.getLockStatus();//0 = unknown fail, 1 == success, 2 == fail because no admin
        returnResultToWatch(messageEvent.getSourceNodeId(), lockResult);

    }


    private void startConstantPIN(final String sourceNodeId){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "startConstantPIN() exec'd");
        }
        constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
        final String startConstantPINAndGetResult = constHelp.startConstantPINByChoice(this, true, sourceNodeId, false);
        if (Objects.equals(startConstantPINAndGetResult, "3") || Objects.equals(startConstantPINAndGetResult, "2")){
            returnResultToWatch(sourceNodeId, startConstantPINAndGetResult);
        }
    }


    /*
    * The reason for tracking connection of wear nodes is because both of the following can fail to track when a wear device disconnects in certain scenarios:
        <action android:name="android.bluetooth.device.action.ACL_CONNECTED" />
        <action android:name="android.bluetooth.device.action.ACL_DISCONNECTED" />
    * The wear device will show the strikethrough-cloud icon, indicating no connection, because of bugs in play services or whatever, even though the watch is right next to the phone.
    * Sometimes the wear device will remain like this for hours or days, but much-less common than nearly a decade ago.
    * But it's effectively disconnected, because in my testing, when the device truly leaves BT range, it doesn't fire the above BT disconnect event.
    * So tracking the nodes here acts as a backup way of locking down when wear is being dumb af.
    *
    * */


    public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {
        super.onCapabilityChanged(capabilityInfo);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onCapabilityChanged()");
        }

//        //remove this for production. just need to monitor this for testing IRL
//        try {
//            Toast.makeText(getApplicationContext(),
//                    "node change detected",
//                    Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//            if (BuildConfig.DEBUG) {
//                Log.e(TAG, "failed to toast", e);
//            }
//        }

        if (!capabilityInfo.getNodes().isEmpty()) {

            Runnable run = () -> {
                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(this);
                if (iqd.getSettingsTempLongVal(USE_WEAR_APIS) == 1) {

                    List<Node> connectedWearNodes = null;
                    try {
                        connectedWearNodes = Tasks.await(Wearable.getNodeClient(this).getConnectedNodes());//put countdownlatch instead
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.e(TAG, "failed to get list of Nodes", e);
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "onCapabilityChanged - Device status:");
                        Log.d(TAG, "connectedWearNodes: " + connectedWearNodes);
                    }
                    Set<Node> detectedWearDevices = capabilityInfo.getNodes();

                    ArrayList<String> monitoredDevices;
                    monitoredDevices = iqd.getAllMonitoredDevices();
                    if (monitoredDevices == null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "monitoredDevices is null, probably none stored");
                        }
                        return;
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "monitoredDevices before chopdown: " + monitoredDevices);
                    }
                    ListIterator<String> monitoredDevicesIterator = monitoredDevices.listIterator();
                    while (monitoredDevicesIterator.hasNext()) {
                        //chop down monitored devices to wear OS ones only
                        String discoveredNodeID = "";
                        String serial = monitoredDevicesIterator.next();
                        try {
                            discoveredNodeID = Tasks.await(Wearable.getNodeClient(this).getNodeId(serial), 7, TimeUnit.SECONDS);
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Tasks.await method failed getting the nodeID from a serial (" + serial + ") failed, is likely not a wear device.", e);
                            }
                        }
                        if (Objects.equals(discoveredNodeID, "")) {
                            monitoredDevicesIterator.remove();
                            //monitoredDevicesIterator
                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "monitoredDevicesIterator.next()'s serial " + serial + " has discoveredNodeID: " + discoveredNodeID);
                            }
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "monitoredDevices after chopdown: " + monitoredDevices);
                    }

                    boolean lock = false;
                    String lockingSerial = "";
                    connDisconnTasks connDisconn = new connDisconnTasks(this, TAG);
                    for (Node device : detectedWearDevices) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "name - " + device.getDisplayName());
                            Log.d(TAG, "id - " + device.getId());
                            Log.d(TAG, "nearby - " + device.isNearby());
                        }
                        for (String serial : monitoredDevices) {
                            String serialNode = "";
                            try {
                                serialNode = Tasks.await(Wearable.getNodeClient(this).getNodeId(serial), 9, TimeUnit.SECONDS);
                            } catch (Exception e) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "failed to get the node from the serial... even though it was proven to exist for this serial: " + serial);
                                }
                            }
                            if (device.isNearby() && serialNode.equals(device.getId())) {
                                //set as connected, in case BT was being a fool
                                connDisconn.connTasks(serial);

                            } else if (!device.isNearby() && serialNode.equals(device.getId())) {
                                final String[] connectedSelectionArgs = {serial, "1"};
                                boolean isConnectedBTDevice = iqd.isConnectedBTDeviceQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);
                                if (isConnectedBTDevice) {
                                    //monitored wear device is tagged as not being nearby, yet is tagged as STILL being connected, according to the OS' buggy af BT stack.
                                    lock = true;
                                    lockingSerial = serial;
                                    connDisconn.disconnTasksWearNode(serial);
                                }
                            }
                        }
                    }

                    Intent btDeviceConnDisconnIntent = new Intent("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
                    this.sendBroadcast(btDeviceConnDisconnIntent, "com.merrycachemiss.rl");
                    if (lock) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "after processing what wear devices are monitored and disconnected, concluded that we must send a lock signal to the main service");
                        }
                        Intent lockCheckForService = new Intent("com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE");
                        lockCheckForService.putExtra("btDeviceSerial", lockingSerial);
                        this.sendBroadcast(lockCheckForService, "com.merrycachemiss.rl");
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "user is not using wear APIs to detect disconnection");
                    }
                }
            };

            Thread wearDisconnectedLockCheckThread = new Thread(run);
            wearDisconnectedLockCheckThread.start();

        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onCapabilityChanged - No Devices. Checking if any monitored/locking devices have a nodeID, if so: lock, as none are connected.");
            }


            Runnable run = () -> {

                ArrayList<String> connectedMonitoredDevices;
                rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(this);
                if (iqd.getSettingsTempLongVal(USE_WEAR_APIS) == 1) {
                    try {
                        connectedMonitoredDevices = iqd.getAllConnectedMonitoredDevices();
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "onPeerConnected() failed to build up list of monitored devices for whatever greasy reason", e);
                        }
                        return;
                    }

                    if (connectedMonitoredDevices == null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "connectedMonitoredDevices is null, none are stored and/or connected");
                        }
                        return;
                    }

                    boolean lock = false;
                    String discoveredNodeID = "";
                    String lockingSerial = "";
                    for (String serial : connectedMonitoredDevices) {
                        try {
                            discoveredNodeID = Tasks.await(Wearable.getNodeClient(this).getNodeId(serial), 9, TimeUnit.SECONDS);
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Tasks.await method failed getting the nodeID from a serial (" + serial + ") failed, is likely not a wear device.", e);
                            }
                        }
                        if (!Objects.equals(discoveredNodeID, "")) {
                            //^one of the monitored/locking devices is a wear device

                            //this monitored wear device is thought to be connected, because bluetooth event didn't fire android.bluetooth.device.action.ACL_DISCONNECTED
                            //though it's not connected, according to wear APIs nothing is connected when the logic reaches this step here.

                            //we are going to trust the wear APIs' version of the truth, as the disconnected cloud symbol on the watch will have appeared,
                            //Sometimes ACL_DISCONNECTED won't fire for it, even though it's effectively disconnected.

                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "found a connected and monitored device that has a node id, yet bluetooth (and not wear node API) erroneously thinks it's connected: " +
                                        serial + " node: " + discoveredNodeID);
                            }

                            lock = true;
                            //set to disconnected in our internal DB
                            iqd.BTConnStatusInsert(serial, 0);
                            lockingSerial = serial;//just need to pass any of the locking serials, doesn't matter if overwritten by another in the list.

                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Connected and monitored device has no node id, it's not a set-up wear device: " + serial);
                            }
                        }

                        discoveredNodeID = "";//reset for next check
                    }

                    Intent btDeviceConnDisconnIntent = new Intent("com.merrycachemiss.rl.BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
                    this.sendBroadcast(btDeviceConnDisconnIntent, "com.merrycachemiss.rl");

                    //finally, lock. send the lock check to the foreground service, hopefully the user put in the battery exceptions and it's running.
                    //will attempt to launch the service
                    if (lock) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "after processing what wear devices are monitored and disconnected, concluded that we must send a lock signal to the main service");
                        }
                        Intent lockCheckForService = new Intent("com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE");
                        lockCheckForService.putExtra("btDeviceSerial", lockingSerial);
                        this.sendBroadcast(lockCheckForService, "com.merrycachemiss.rl");
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "user is not using wear APIs to detect disconnection");
                    }
                }
            };

            Thread wearDisconnectedLockCheckThread = new Thread(run);
            wearDisconnectedLockCheckThread.start();



        }
    }


    private void returnResultToWatch(final String sourceNodeId, final String result){

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "returnResultToWatch() back to watch");
        }
        try {
            Runnable sendMessage = () -> {
                try {
                    Task<Integer> sendMessageTask =
                            Wearable.getMessageClient(getApplicationContext()).sendMessage(sourceNodeId,
                                    "/lock_phone_result", result.getBytes());
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "sendMessage() back to watch about success status: " + e);
                    }
                }
            };
            new Thread(sendMessage).start();
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to create new thread for sending message back to watch " + e);
            }
        }


    }

}
