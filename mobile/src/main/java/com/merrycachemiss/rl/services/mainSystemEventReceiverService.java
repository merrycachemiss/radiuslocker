/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl.services;

import static android.Manifest.permission.BLUETOOTH_CONNECT;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.os.SystemClock;
import android.util.Log;

import com.merrycachemiss.rl.BuildConfig;
import com.merrycachemiss.rl.R;

import com.merrycachemiss.rl.dataStorage.rldbContract;
import com.merrycachemiss.rl.dataStorage.rldbInsertQueryDelete;
import com.merrycachemiss.rl.util.constantPinHelper;
import com.merrycachemiss.rl.lockTasks.immediateLocker;
import com.merrycachemiss.rl.lockTasks.lockDecider;
import com.merrycachemiss.rl.util.NotificationHelper;
import com.merrycachemiss.rl.util.connDisconnTasks;
import com.merrycachemiss.rl.util.getSetCurrentWIFISSID;
import com.merrycachemiss.rl.util.permissionHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.merrycachemiss.rl.dataStorage.dataConstants.BT_REFRESHER_QUEUED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.CONSTANT_REQUIRE_PIN_PAUSE_TIME_END;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LATEST_CONNECTED_WIFI_SSID;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_CABLE_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.MULTI_DEVICE_MODE;
import static com.merrycachemiss.rl.dataStorage.dataConstants.RELOCK_JOB_SCHEDULED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.LOCK_IF_SIM_REMOVED;
import static com.merrycachemiss.rl.dataStorage.dataConstants.START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE;
import static com.merrycachemiss.rl.util.NotificationHelper.NOTIF_ID_BT_MON_SERVICE;

public final class mainSystemEventReceiverService extends Service {

    private static final String TAG = "mainSysEventRecService";
    private final Intent deviceConnectedDisconnectedIntent = new Intent("com.merrycachemiss.rl");
    private long wifiConnCheckSuppressTimer = 0;
    private long serviceLaunchTime = 0;
    public mainSystemEventReceiverService() {
    }

    //The iqd obj must be initiated from scratch with any receiver, even though the service is
    //constantly running, else you end up with the dead object exception.
    //I believe that this is because the db is accessed through the separate Process that holds
    //the content provider.
    //When the content provider process is killed off, then the dead object exception happens.

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onCreate");
        }

    }

    private static mainSystemEventReceiverService instance = null;

    public static boolean isInstanceCreated() {
        //determine if service is started -- var overridden by below methods
        return instance != null;
    }
    private void createNotification(){
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "createNotification");
        }
        NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
        notificationHelper.createNotifChannelOreo("background_services", getApplicationContext().getResources().getString(R.string.services_and_tasks),
                3, false);
        NotificationCompat.Builder builder = notificationHelper.btMonRefreshLockerNotifBuilder(getApplicationContext().getResources().getString(R.string.monitoring_bt_state), R.drawable.ic_lock,
                null, true);
        startForeground(NOTIF_ID_BT_MON_SERVICE, builder.build());

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //2022-11-03 So because we've run into the problem where some of the services can't start in the background, we have to load everything from
        //those services into here. Funny that it didn't show up on a Pixel or emulators, just crash reports.
        //This is foolish, because this service will become more bloated and have to run and/or execute all items that used to run/exec only when needed.
        //example: now it has to monitor for screen_off event at all times, formerly this monitoring used to be in constant PIN mode service only when that service was running.
        //          this means that every time the user turns off the screen, actions have to be executed.
        //constant PIN to do: still do the constantPin charging jobs, but instead of starting constant PIN service,
        //just lock when constant PIN mode parameters are valid
        //in the job, set it to lock immediately, as the mode has just become active, just like before (but used to happen in the service).
        //check what some of the cooldowns and heartbeats do, though


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onStartCommand");
        }
        try {
            createNotification();
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to create notification in onStartCommand");
            }
        }



        if (BuildConfig.DEBUG) {
            Log.d(TAG, "service onStartCommand called.");
        }


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "registering receivers");
        }
        serviceLaunchTime = SystemClock.elapsedRealtime();
        @SuppressLint("UnspecifiedRegisterReceiverFlag") Runnable r = () -> {

            IntentFilter eventFilter = new IntentFilter();

            eventFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

            eventFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

            eventFilter.addAction("android.intent.action.SIM_STATE_CHANGED");

            eventFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);

            eventFilter.addAction(Intent.ACTION_POWER_CONNECTED);

            eventFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

            eventFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");

            eventFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");

            eventFilter.addAction(Intent.ACTION_SCREEN_OFF);

            //since the service has the foreground location var attached to it (android:foregroundServiceType="location"),
            //other sources (ie postsnoozelock) should do a lock check (snooze or not, etc) through this service, so that it helps ensure that the permission grant is used
            eventFilter.addAction("com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE");


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "registering SHUTDOWN receivers for P");
                }
                eventFilter.addAction(Intent.ACTION_SHUTDOWN);
                eventFilter.addAction("android.intent.action.QUICKBOOT_POWEROFF");
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "not on P+, so no need to register SHUTDOWN receivers");
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ContextCompat.registerReceiver(this, events, eventFilter, ContextCompat.RECEIVER_EXPORTED);
                //so this is supposed to be receiver_not_exported, I believe, which would allow system events to be received.
                //but the bluetooth events aren't received - just the power and wifi change ones.
                //am I wrong about this, or did those morons leave out bluetooth connect and disconnect as "system events"?

            } else {
                registerReceiver(events, eventFilter);
            }

        };

        Thread btDisconnectMonitorThread = new Thread(r);
        btDisconnectMonitorThread.start();


        if (BuildConfig.DEBUG) {
            Log.d(TAG, "monitoring service started");
        }
        instance = this;

        return Service.START_STICKY;
    }



    private final BroadcastReceiver events = new BroadcastReceiver() {
        @SuppressLint("MissingPermission")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "received " + action);
                }
                if (Intent.ACTION_POWER_CONNECTED.equals(action)) {
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "ACTION_POWER_CONNECTED detected");
                    }

                    constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
                    constHelp.activateConstantPINServiceIfNeeded(context, iqd);
                } else if ("com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE".equals(action)) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "com.merrycachemiss.rl.LOCK_CHECK_FOR_SERVICE received");
                    }
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        String btDeviceSerial = extras.getString("btDeviceSerial", "RLnull");
                        String btDeviceName = extras.getString("btDeviceName", "RLnull");
                        int postSnoozeLockCheck = extras.getInt("postSnoozeLockCheck", 0);
                        int btOffSimPowerRemoved = extras.getInt("btOffSimPowerRemoved", 0);
                        int fromAlarm = extras.getInt("fromAlarm", 0);


                        lockCheck(context, btDeviceSerial, btDeviceName, btOffSimPowerRemoved, postSnoozeLockCheck, fromAlarm);
                    }
                } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                    //a BT device was paired or unpaired. For security reasons, engage locking procedures, if it was a locking device, even if device was not connected during pairing.
                    deviceConnectedDisconnectedIntent.putExtra("message", "BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");

                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    permissionHelper perm = new permissionHelper(context);
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);


                    if (!perm.isMissingRequiredPerm(BLUETOOTH_CONNECT, Build.VERSION_CODES.S, iqd, BT_REFRESHER_QUEUED, 1)) {
                        //if a BT device was paired (assumed connected) or unpaired, update the connection states table
                        if (Objects.requireNonNull(device).getBondState() == BluetoothDevice.BOND_BONDED) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "New device paired, adding to DB as connected");
                            }
                            iqd.BTConnStatusInsert(device.getAddress(), 1);
                            context.sendBroadcast(deviceConnectedDisconnectedIntent, "com.merrycachemiss.rl");
                            final String[] connectedSelectionArgs = {device.getAddress(), "1"};
                            boolean isConnectedBTDevice = iqd.isConnectedBTDeviceQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);

                            Log.d(TAG, "BT device connected state, according to db " + device.getName() + " " + device.getAddress() + " status: " + isConnectedBTDevice);
                        } else if (device.getBondState() == BluetoothDevice.BOND_NONE) {


                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Device unpaired, removing from all DB but first checking if they were used for locking");
                            }

                            //if BOND_NONE, BT device is no longer paired/bonded
                            //remove from connected device table altogether
                            String devAddress = device.getAddress();
                            context.sendBroadcast(deviceConnectedDisconnectedIntent, "com.merrycachemiss.rl");

                            lockDecider locker = new lockDecider();
                            try {
                                locker.lockIfValid(getApplicationContext(), devAddress, device.getName(), 0, 0, 0, SystemClock.elapsedRealtime());
                            } catch (Exception e) {
                                if (BuildConfig.DEBUG) {
                                    Log.e(TAG, "Couldn't try to lock device unpaired.", e);
                                }
                            }
                            iqd.BTConnStatusDelete(devAddress);
                            final String[] connectedSelectionArgs = {device.getAddress(), "1"};

                            boolean isConnectedBTDevice = iqd.isConnectedBTDeviceQuery(rldbContract.ConnectedBTQuery.CONTENT_URI, connectedSelectionArgs);
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "BT device connected state, according to db " + device.getName() + " " + device.getAddress() + " status: " + isConnectedBTDevice);
                            }
                            locker = null;
                        }

                    } else {
                        //we are missing BT permissions, but the OS at least told us that the BT bond state changed for a BT device (meaning: a device was paired or unpaired).
                        //We don't know if it was a locking device that was unpaired, but we should still lock anyway, for security purposes.
                        //Similar happens when a user upgrades to 12+, or upgrades the app from a version that didn't target 12+ on a 12+ device, and the permission is not granted.
                        //--But only locks if screen is off in those cases.
                        perm.surfaceNotificationForMissingPerms();
                        iqd.updateAllBTConnStatusToDisconn();
                        //If they use a BT device for snoozing (including multi-watch lock prevention), we will make it so the user will
                        //refresh BT when they go into the main activity, since we temporarily lost the state of their devices
                        //while the permission was missing:
                        if (iqd.getSettingsTempLongVal(MULTI_DEVICE_MODE) == 1 || iqd.genericSingleItemExistsQuery(rldbContract.ConnectedSafeBTQuery.CONTENT_URI, null, null)) {
                            iqd.setSettingsTempLongVal(BT_REFRESHER_QUEUED, 1);
                        }
                        lockDecider locker = new lockDecider();
                        try {
                            locker.lockIfValid(getApplicationContext(), "RLnull", "RLnull", 1, 0, 0, SystemClock.elapsedRealtime());
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.e(TAG, "Couldn't try to lock device unpaired and BT permission is missing.", e);
                            }
                        }

                    }


                    //release this stuff immediately, in case such a thing is needed in a long-running service.
                    //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                    //Though, my linter is complaining weirdly about this.
                    device = null;
                    perm = null;
                    iqd = null;
                } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    // BT radio state detection
                    deviceConnectedDisconnectedIntent.putExtra("message", "BT_DEVICE_WAS_CONNECTED_OR_DISCONNECTED");
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "BT turned off. Updating everything to disconnected, and locking if applicable.");
                        }

                        iqd.updateAllBTConnStatusToDisconn();
                        lockDecider locker = new lockDecider();
                        try {
                            locker.lockIfValid(getApplicationContext(), "RLnull", "RLnull", 1, 0, 0, SystemClock.elapsedRealtime());
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Couldn't try to lock when power cable removed. Maybe in preboot mode.");
                            }
                        }

                        //i dont think this was used anymore - double check and remove sometime
                        deviceConnectedDisconnectedIntent.putExtra("BTONOFFmessage", "BT_OFF");
                        context.sendBroadcast(deviceConnectedDisconnectedIntent, "com.merrycachemiss.rl");

                        //used for constant PIN related stuff for when watch is missing and user wants constant PIN mode because of it
                        iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_LOCKING_WATCH_IS_DISCONNECTED, 1);

                        iqd = null;
                        locker = null;
                    }

                    //release this stuff immediately, in case such a thing is needed in a long-running service.
                    //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                    //Though, my linter is complaining weirdly about this.
                    bluetoothAdapter = null;
                } else if ("android.intent.action.SIM_STATE_CHANGED".equals(action)) {
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);

                    if (SystemClock.elapsedRealtime() - 3750 > serviceLaunchTime) {
                        if (Objects.equals(intent.getStringExtra("ss"), "ABSENT") && iqd.getSettingsTempLongVal(LOCK_IF_SIM_REMOVED) == 1) {

                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "SIM removed. Locking if used the option.");
                            }

                            lockDecider locker = new lockDecider();
                            try {
                                locker.lockIfValid(getApplicationContext(), "RLnull", "RLnull", 2, 0, 0, SystemClock.elapsedRealtime());
                            } catch (Exception e) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "Couldn't try to lock when power cable removed. Maybe in preboot mode.");
                                }
                            }

                            locker = null;

                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Service started or SIM just removed, but user isn't using lock setting. SIM status: " + intent.getStringExtra("ss"));
                            }
                        }

                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Prevented lock bug where it thinks SIM was removed upon service start (don't lock if within 2s of service begin).");
                        }
                    }

                    //release this stuff immediately, in case such a thing is needed in a long-running service.
                    //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                    //Though, my linter is complaining weirdly about this.
                    iqd = null;
                } else if ("android.bluetooth.device.action.ACL_CONNECTED".equals(action)) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "ACTION_ACL_CONNECTED");
                    }

                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if (device != null) {
                        connDisconnTasks conn = new connDisconnTasks(context, TAG);
                        conn.connTasks(device.getAddress());

                        //release this stuff immediately, in case such a thing is needed in a long-running service.
                        //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                        //Though, my linter is complaining weirdly about this.
                        device = null;
                        conn = null;
                    }

                } else if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {

                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "started disconnect tasks");
                    }

                    if (device != null) {
                        connDisconnTasks disconn = new connDisconnTasks(context, TAG);
                        disconn.disconnTasks(device);

                        //release this stuff immediately, in case such a thing is needed in a long-running service.
                        //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                        //Though, my linter is complaining weirdly about this.
                        device = null;
                        disconn = null;
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {

                    //android.net.conn.CONNECTIVITY_CHANGE seems to be fired rapidly upon disconnecting from one network. Lots of repeat receive events for no reason.
                    //wifiConnCheckSuppressTimer aims to have it CTFD and perform this operation much less.
                    //Also, I heard somewhere that CMFT must be stopped.

                    if (SystemClock.elapsedRealtime() >= wifiConnCheckSuppressTimer) {
                        wifiConnCheckSuppressTimer = SystemClock.elapsedRealtime()+6000;
                        try {
                            //try to run this on a delay, in case user is on a mesh network and transitioning between networks.
                            //hopefully their connection settles when this is ready to check for a lock.
                            Runnable wifiOPS = () -> {
                                try {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(TAG, "sleep for 5750 before wifi connection state change operations");
                                    }
                                    Thread.sleep(5750);
                                } catch (Exception e) {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(TAG, "failed to sleep for 5750 before wifi connection state change operations");
                                    }
                                }
                                performWIFIops(context);
                            };
                            new Thread(wifiOPS).start();
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "failed run wifi connection state change operations on separate thread in background");
                            }
                            performWIFIops(context);
                        }
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "suppressed performWIFIops() because there's still " + ((SystemClock.elapsedRealtime()-wifiConnCheckSuppressTimer)*-1) + " ms left");
                        }
                    }

                } else if (Intent.ACTION_POWER_DISCONNECTED.equals(action)) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "power unplugged. resetting cancelled alarm temp var to 0");
                    }
                    //this is so constant PIN will return after user plugs in. this is in case they were plugged in elsewhere and decided to plug in again when -actually- going to sleep
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
                    iqd.setSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP, 0);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "cancelled constant PIN alarm time is now set to 0, as shown here: " + iqd.getSettingsTempLongVal(CONSTANTLY_REQUIRE_PIN_ALARM_TIME_TEMP));
                    }

                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "since user just unplugged, going to also remove constant PIN service and heartbeat if necessary, and reset heartbeat cooldown period to 0.");
                    }
                    iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_ACTIVE_HEARTBEAT_COOLDOWN_TIME_END, 0);
//                    constantPinHelper constHelp = new constantPinHelper(context.getApplicationContext());
//                    constHelp.stopConstantPinIfNecessary(context.getApplicationContext(), iqd, false);

                    if (iqd.getSettingsTempLongVal(LOCK_IF_CABLE_REMOVED) == 1) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Cable removed. Locking if used the option.");
                        }

                        lockDecider locker = new lockDecider();
                        try {
                            locker.lockIfValid(getApplicationContext(), "RLnull", "RLnull", 3, 0, 0, SystemClock.elapsedRealtime());
                        } catch (Exception e) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Couldn't try to lock when power cable removed. Maybe in preboot mode.");
                            }
                        }

                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Power cable just removed, but user isn't using lock setting.");
                        }
                    }

                    //release this stuff immediately, in case such a thing is needed in a long-running service.
                    //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
                    //Though, my linter is complaining weirdly about this.
                    iqd = null;

                } else if (Intent.ACTION_SHUTDOWN.equals(action)) {
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "shutdown detected: " + Intent.ACTION_SHUTDOWN);
                    }
                    try {
                        iqd.updateAllBTConnStatusToDisconn();
                        iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to write to DB on shutdown");
                        }
                    }

                } else if ("android.intent.action.QUICKBOOT_POWEROFF".equals(action)) {
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "shutdown detected: android.intent.action.QUICKBOOT_POWEROFF");
                    }
                    try {
                        iqd.updateAllBTConnStatusToDisconn();
                        iqd.setSettingsTempLongVal(RELOCK_JOB_SCHEDULED, 0);

                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to write to DB on shutdown");
                        }
                    }

                } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                    //lifted from constant PIN mode service, since services can't be started from the background.
                    rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);


                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "screen off detected");
                    }

                    //lock if needed, if not then stop service an possibly stop heartbeat
                    constantPinHelper constHelp = new constantPinHelper(context);
                    if (constHelp.bedtimeModeAndWithin8hrLockingIsValid(iqd) || constHelp.watchMissingAndConstantPinEnabledOrIsManuallyEnabled(iqd)) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "locking screen if within 8hr before user's alarm, or manually enaged this mode, or disconnected wearable is still gone and user enabled that setting");
                        }
                        long pauseTimeEnd = iqd.getSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END);
                        if (SystemClock.elapsedRealtime() > pauseTimeEnd) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "locking screen since pause period is expired or was null");
                            }
                            immediateLocker lock = new immediateLocker(context, false, true, TAG);
                            lock.lockNOW();
                            //add a small delay before locking again, in case user keeps turning screen on and causing it to lock again and again.
                            iqd.setSettingsTempLongVal(CONSTANT_REQUIRE_PIN_PAUSE_TIME_END, SystemClock.elapsedRealtime()+2000);
                            constHelp.createConstantPINNotificationIfNotExists();//there are some states where this didn't create a notif. ensure it's created.
                        } else {
                            if (BuildConfig.DEBUG) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.US);
                                String pauseTimeEndFormatted = sdf.format(new Date(pauseTimeEnd));
                                Log.d(TAG, "didn't lock because it's paused for this additional period of time " + (pauseTimeEnd-SystemClock.elapsedRealtime()));
                            }
                        }

                    } else {

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "not within 8hr window yet/anymore, so didn't lock screen. Stopping service and/or heartbeat if necessary");
                        }

                        constHelp.stopConstantPinIfNecessary(context, iqd, false);

                    }

                }
            }
        }
    };

    private void lockCheck(Context context, String btDeviceSerial, String btDeviceName, int btOffSimPowerRemoved, int postSnoozeLockCheck, int fromAlarm){
        lockDecider locker = new lockDecider();
        try {
            locker.lockIfValid(context, btDeviceSerial, btDeviceName, btOffSimPowerRemoved, postSnoozeLockCheck, fromAlarm, SystemClock.elapsedRealtime());
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Couldn't try to lock when power cable removed. Maybe in preboot mode.");
            }
        }
        locker = null;
    }


    private void performWIFIops(Context context) {

        //reminder that this is duplicated in onBootOnShutdownBroadcastRec and should be moved into a util class.
        rldbInsertQueryDelete iqd = new rldbInsertQueryDelete(context);
        getSetCurrentWIFISSID wifiSSIDGetSet = new getSetCurrentWIFISSID(context, TAG);
        String ssid = wifiSSIDGetSet.getCurrentWIFISSID();
        if (iqd.getSettingsTempLongVal(START_CONSTANT_PIN_IF_LEAVE_WIFI_SNOOZE) == 1) {
            //disconnected from wifi, check if the last stored connection was in wifi snooze list
            if ((ssid.isEmpty() || ssid.trim().isEmpty() || !wifiSSIDGetSet.currentlyOnWIFISnoozeSSID(iqd, ssid)) && wifiSSIDGetSet.justLeftAnyWIFISnoozeSSID(iqd)) {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "starting constant PIN mode because left wifi snooze network SSID: " + iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID));
                }
                constantPinHelper constHelp = new constantPinHelper(getApplicationContext());
                try {
                    constHelp.startConstantPINByChoice(getApplicationContext(), false, null, false);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "failed to launch service for constant PIN from app shortcut", e);
                    }
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "left wifi network, but it wasn't used for snoozing. SSID: " + iqd.getSettingsTempStringVal(LATEST_CONNECTED_WIFI_SSID));
                }
            }
        }
        //set current ssid info, but only after comparison of before vs after network change.
        wifiSSIDGetSet.setCurrentWIFISSID(iqd, ssid);

        //release this stuff immediately, in case such a thing is needed in a long-running service.
        //They have to reinit upon receiving broadcasts anyway, else risking dead object exception-related stuff.
        //Though, my linter is complaining weirdly about this.
        wifiSSIDGetSet = null;
        iqd = null;

    }


    @Override
    public void onDestroy() {
        instance = null;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "service STOPPED/DESTROYED");
        }

        try {
            unregisterReceiver(events);
        }catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Couldn't unregister events receiver for unknown reasons");
            }
        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "service STOPPED/DESTROYED end, all things unregistered where possible");
        }
        super.onDestroy();

    }






    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
