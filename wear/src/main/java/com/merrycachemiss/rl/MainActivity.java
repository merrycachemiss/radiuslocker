/*
 *    RadiusLocker
 *    Copyright (C) 2024 merry cache miss technologies, Inc.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    Additional permission under GNU GPL version 3 section 7
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Material Components For Android (com.google.android.material:material)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android AppCompat Library (androidx.appcompat:appcompat)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android ConstraintLayout (androidx.constraintlayout:constraintlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Legacy Support V4 (androidx.legacy:legacy-support-v4)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Biometric (androidx.biometric:biometric)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Play Services Wearable (com.google.android.gms:play-services-wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Android Software Development Kit License, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Coordinator Layout (androidx.coordinatorlayout:coordinatorlayout)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional permission to convey
 *    the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable Support Library (com.google.android.support:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support UI (androidx.wear:wear)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Compat (androidx.core:core)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core UI (androidx.legacy:legacy-support-core-ui)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Support Library Core Utilities (androidx.legacy:legacy-support-core-utils)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Android Wear Support Input (androidx.wear:wear-input)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 *
 *    If you modify this Program, or any covered work, by linking or combining
 *    it with Wearable (com.google.android.wearable:wearable)
 *    (or a modified version of that library), containing parts covered by the terms of
 *    Apache 2.0, the licensors of this Program grant you additional
 *    permission to convey the resulting work.
 */

package com.merrycachemiss.rl;

import static android.view.KeyEvent.KEYCODE_STEM_1;
import static android.view.KeyEvent.KEYCODE_STEM_2;
import static android.view.KeyEvent.KEYCODE_STEM_3;

import static com.merrycachemiss.rl.prefKeys.SEND_LOCK_ON_APP_LAUNCH;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.wear.input.WearableButtons;
import androidx.wear.widget.CircularProgressLayout;
import androidx.wear.widget.ConfirmationOverlay;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.wearable.intent.RemoteIntent;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MainActivity extends WearableActivity implements View.OnClickListener, MessageClient.OnMessageReceivedListener, CapabilityClient.OnCapabilityChangedListener,
        CircularProgressLayout.OnTimerFinishedListener {

    /*debugging wear device:
adb forward tcp:4444 localabstract:/adb-hub
adb connect 127.0.0.1:4444

removing/installing versions of wear app onto debugged wear device:
cd C:\Users\mcm\Desktop\release\
adb -s 127.0.0.1:4444 uninstall com.merrycachemiss.rl
adb -s 127.0.0.1:4444 install RadiusLockerWEAR_
     */
    private final String TAG = "MainActivity wear";
    private TextView connectivityStatusText;
    private static final String WATCH_SERVER = "watch_server";
    private static String phoneNodeID;
    private static boolean phoneAppReachable = false;
    private CountDownLatch waitToFindWatchLatch;
    private CountDownLatch sendMessageLatch;
    private Button phoneOnboardingButton;
    private TextView phoneOnboardingText;
    private Button lockButton;
    private Button lockButtonTasker;
    private Button startConstantPINButton;
    private TextView circularProgressCancelText;
    private TextView buttonOneText;
    private TextView buttonTwoText;
    private TextView buttonThreeText;
    private ImageView buttonOneImage;
    private ImageView buttonTwoImage;
    private ImageView buttonThreeImage;
    private CheckBox noDelayCheckbox;
    private CircularProgressLayout circularProgress;
    private ScrollView mainScrollView;
    private Vibrator vibrator;
    private boolean alsoTasker = false;
    private boolean startConstantPIN = false;
    private static boolean isPhoneConnected = false;
    private static boolean phoneMessageFinalResultPending = false;
    private dataHandler prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Enables always on mode, app turns black when device idle
        setAmbientEnabled();

        phoneOnboardingButton = findViewById(R.id.phoneOnboardingButton);
        phoneOnboardingText = findViewById(R.id.phoneOnboardingText);
        lockButton = findViewById(R.id.lockButton);
        lockButtonTasker = findViewById(R.id.lockButtonTasker);
        startConstantPINButton = findViewById(R.id.startConstantPINButton);
        connectivityStatusText = findViewById(R.id.connectivityStatusText);
        mainScrollView = findViewById(R.id.mainScrollView);
        circularProgress = findViewById(R.id.circularProgress);
        circularProgressCancelText = findViewById(R.id.circularProgressCancelText);
        buttonOneText = findViewById(R.id.buttonOneText);
        buttonTwoText = findViewById(R.id.buttonTwoText);
        buttonThreeText = findViewById(R.id.buttonThreeText);
        buttonOneImage = findViewById(R.id.buttonOneImage);
        buttonTwoImage = findViewById(R.id.buttonTwoImage);
        buttonThreeImage = findViewById(R.id.buttonThreeImage);
        noDelayCheckbox = findViewById(R.id.noDelayCheckbox);
        circularProgress.setOnTimerFinishedListener(this);
        circularProgress.setOnClickListener(this);
        phoneOnboardingButton.setOnClickListener(this);
        lockButton.setOnClickListener(this);
        lockButtonTasker.setOnClickListener(this);
        startConstantPINButton.setOnClickListener(this);
        noDelayCheckbox.setOnClickListener(this);
        Wearable.getMessageClient(this).addListener(this);
        Wearable.getCapabilityClient(this).addListener(this, WATCH_SERVER);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        prefs = new dataHandler(this);
        lockButton.setOnLongClickListener(v -> {
            if (v.getId() == R.id.lockButton) {
                alsoTasker = false;
                startConstantPIN = false;
                sendMessage(alsoTasker, startConstantPIN);
            }
            return true;
        });

        if (prefs.getSharedPrefsBool(SEND_LOCK_ON_APP_LAUNCH)){
            noDelayCheckbox.setChecked(true);
        }


    }


    private void buttonAssetShowHide(boolean show, int buttonCode, ImageView buttonImage, TextView buttonText){
        if (show) {
            try {
                if (WearableButtons.getButtonInfo(this, buttonCode) != null) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, buttonCode + " found");
                    }
                    buttonImage.setImageDrawable(WearableButtons.getButtonIcon(this, buttonCode));
                    buttonImage.setVisibility(View.VISIBLE);
                    buttonText.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to replace drawable for " + buttonCode);
                }
            }
        } else {
            buttonImage.setVisibility(View.GONE);
            buttonText.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onResume()");
        }
        setConnectionStatusText(isPhoneConnected());

        //lock phone when app is (re)opened
        if (prefs.getSharedPrefsBool(SEND_LOCK_ON_APP_LAUNCH)){
            lockPhoneCountdown(true);
        }

    }

    @Override
    public boolean onKeyDown(int code, KeyEvent event){
        if (event.getRepeatCount() == 0) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, KEYCODE_STEM_1 + " pressed");
            }
            if (code == KEYCODE_STEM_1) {
                alsoTasker = false;
                startConstantPIN = false;
                sendMessage(alsoTasker, startConstantPIN);
                return true;
            } else if (code == KEYCODE_STEM_2) {
                alsoTasker = false;
                startConstantPIN = true;
                sendMessage(alsoTasker, startConstantPIN);
                return true;
            } else if (code == KEYCODE_STEM_3) {
                alsoTasker = true;
                startConstantPIN = false;
                sendMessage(alsoTasker, startConstantPIN);
                return true;
            }
        }
        return super.onKeyDown(code, event);
    }

    private void lockPhoneCountdown(boolean fromAppOpen){

        circularProgress.setVisibility(View.VISIBLE);
        circularProgressCancelText.setVisibility(View.VISIBLE);
        disableMainButtons();
        alsoTasker = false;
        startConstantPIN = false;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mainScrollView.fullScroll(ScrollView.FOCUS_UP);
        if (fromAppOpen) {
            //have a shorter delay if locking from app open. The delay is good in case locking is not desired.
            circularProgress.setTotalTime(1250);
        } else {
            circularProgress.setTotalTime(1750);
        }
        circularProgress.startTimer();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.lockButton) {
            lockPhoneCountdown(false);
        } else if (v.getId() == R.id.lockButtonTasker) {
            circularProgress.setVisibility(View.VISIBLE);
            circularProgressCancelText.setVisibility(View.VISIBLE);
            disableMainButtons();
            alsoTasker = true;
            startConstantPIN = false;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mainScrollView.fullScroll(ScrollView.FOCUS_UP);
            circularProgress.setTotalTime(3500);
            circularProgress.startTimer();
        } else if (v.getId() == R.id.startConstantPINButton) {
            circularProgress.setVisibility(View.VISIBLE);
            circularProgressCancelText.setVisibility(View.VISIBLE);
            disableMainButtons();
            alsoTasker = false;
            startConstantPIN = true;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mainScrollView.fullScroll(ScrollView.FOCUS_UP);
            circularProgress.setTotalTime(1500);
            circularProgress.startTimer();
        } else if (v.getId() == R.id.circularProgress) {
            circularProgress.stopTimer();
            circularProgress.setVisibility(View.GONE);
            circularProgressCancelText.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mainScrollView.fullScroll(ScrollView.FOCUS_UP);
            enableMainButtons();
        } else if (v.getId() == R.id.phoneOnboardingButton) {
            installPhoneApp();
        } else if (v.getId() == R.id.noDelayCheckbox) {
            if (noDelayCheckbox.isChecked()){
                prefs.setSharedPrefsBool(SEND_LOCK_ON_APP_LAUNCH, true);
            } else {
                prefs.setSharedPrefsBool(SEND_LOCK_ON_APP_LAUNCH, false);
            }
        }
    }

    @Override
    public void onTimerFinished(CircularProgressLayout layout) {
        // User didn't cancel, perform the action
        sendMessage(alsoTasker, startConstantPIN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        circularProgress.setVisibility(View.GONE);
        circularProgressCancelText.setVisibility(View.GONE);
        enableMainButtons();
        alsoTasker = false;
        startConstantPIN = false;
    }

    private void sendMessage(final boolean alsoTasker, final boolean startConstantPIN){

        //wait 15 seconds to check on phone reachability, if not currently known as reachable.
        //if not reachable, can't send the signal, so cancel sending.
        Runnable sendMessage = () -> {
            if (phoneNodeID == null || Objects.equals(phoneNodeID, "") || !phoneAppReachable) {
                waitToFindWatchLatch = new CountDownLatch(1);
                getPhoneNodeID(true);

                try{
                    Thread.sleep(500);
                } catch (Exception e){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to sleep for trying to wait a little to get phone node id");
                    }
                }
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "starting wait on the waitToFindWatchLatch countdownlatch for 15 seconds");
                    }
                    waitToFindWatchLatch.await(15, TimeUnit.SECONDS);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "waitToFindWatchLatch latch counted down or timed out");
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to wait for waitToFindWatchLatch countdownlatch");
                    }
                    waitToFindWatchLatch.countDown();
                }
                if (!phoneAppReachable){
                    failedLockMessageReshowButtons(false);
                    return;
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "sendMessage() to /lock_phone");
            }
            //phone is reachable, as the previous countdownlatch didn't return out of this method.
            //attempt signal send, and start another countdownlatch to display failure message if
            //latch isn't released by the return signal before timeout.
            try {
                phoneMessageFinalResultPending = true;
                if (!startConstantPIN) {
                    if (!alsoTasker) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "sendMessage() to /lock_phone without Tasker flag");
                        }
                        Task<Integer> sendMessageTask =
                                Wearable.getMessageClient(MainActivity.this).sendMessage(phoneNodeID, "/lock_phone", "1".getBytes(StandardCharsets.UTF_8));


                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "sendMessage() to /lock_phone with Tasker flag");
                        }
                        Task<Integer> sendMessageTask =
                                Wearable.getMessageClient(MainActivity.this).sendMessage(phoneNodeID, "/lock_phone", "2".getBytes(StandardCharsets.UTF_8));
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "sendMessage() to /lock_phone with start constant PIN flag");
                    }
                    Task<Integer> sendMessageTask =
                            Wearable.getMessageClient(MainActivity.this).sendMessage(phoneNodeID, "/lock_phone", "3".getBytes(StandardCharsets.UTF_8));
                }

                sendMessageLatch = new CountDownLatch(1);
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "starting wait on the sendMessageLatch countdownlatch for 15 seconds");
                    }

                    sendMessageLatch.await(15, TimeUnit.SECONDS);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "sendMessageLatch latch counted down or timed out");
                    }
                    if (!phoneMessageFinalResultPending) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "received lock confirmation in under 15 seconds. Good! sendMessageLatch.getCount(): " + sendMessageLatch.getCount());
                        }
                    } else {
                        failedLockMessageReshowButtons(true);

                        isPhoneConnected();
                    }
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to wait for sendMessageLatch countdownlatch");
                    }
                    sendMessageLatch.countDown();
                }

            } catch (Exception e) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                circularProgress.setVisibility(View.GONE);
                circularProgressCancelText.setVisibility(View.GONE);
                enableMainButtons();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "sendMessage(): " + e);
                }
            }
        };
        new Thread(sendMessage).start();

    }

    @Override
    public void onMessageReceived(@NonNull MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/lock_phone_result")) {
            String messageReceived = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            phoneMessageFinalResultPending = false;
            //lastLock = System.currentTimeMillis();

            try {
                sendMessageLatch.countDown();
            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to countdown sendMessageLatch, since it's prob null");
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "message " + messageReceived + " received from " + messageEvent.getSourceNodeId() + " to path " +
                        messageEvent.getPath());
            }
            try{
                switch (messageReceived) {
                    case "1":
                        try {
                            new ConfirmationOverlay()
                                    .setType(ConfirmationOverlay.SUCCESS_ANIMATION)
                                    .setMessage("Phone lock CONFIRMED")
                                    .showOn(MainActivity.this);
                        } catch (Exception e) {
                            try {
                                runOnUiThread(() -> {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(TAG, "Failed to show phone lock confirmation overlay");
                                    }
                                    Toast.makeText(getApplicationContext(), "Phone lock CONFIRMED", Toast.LENGTH_LONG).show();

                                });
                            } catch (Exception e2) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "failed to make toast about phone confirmation lock");
                                }
                            }
                        }
                        vibrate(true);

                        break;
                    case "2":
                        Toast.makeText(this, "FAILURE.\nRadiusLocker on Phone doesn't have Device Admin access. Fix on phone.", Toast.LENGTH_LONG).show();
                        vibrate(false);
                        break;
//                    case "3":
//                        Toast.makeText(this, "FAILURE.\nThis feature requires an upgrade to Premium on the phone", Toast.LENGTH_LONG).show();
//                        break;
                    case "4":
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "constant PIN service queued to start. will get final return message confirming its successful launch.");
                        }
                        break;
                    case "5":
                        vibrate(true);
                        Toast.makeText(this, "Constant PIN mode started\nPhone locked, too.", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(this, "Unknown failure on phone. Maybe the RL phone app needs updating,\n if you've just tried a new feature here", Toast.LENGTH_SHORT).show();
                        vibrate(false);
                        break;
                }

            } catch (Exception e){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "failed to make toast about locking phone success");
                }
            }
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            circularProgress.setVisibility(View.GONE);
            circularProgressCancelText.setVisibility(View.GONE);
            enableMainButtons();
        }
    }

    private void vibrate (boolean success){
        if (vibrator.hasVibrator()) {
            if (success){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(300);
                }
            } else {
                long[] failpattern = {0, 250, 500, 350};
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//didn't work?
//                    VibrationEffect.createWaveform(failpattern, VibrationEffect.DEFAULT_AMPLITUDE);
//                } else {
                    vibrator.vibrate(failpattern, -1);
               //}
            }
        }
    }
    public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onCapabilityChanged(): " + capabilityInfo);
        }
        if (capabilityInfo.getNodes().size() > 0) {
            getPhoneNodeID(false);
        } else {
            isPhoneConnected();//setConnectionStatusText(false);
        }

    }

    private void failedLockMessageReshowButtons(final boolean fromSendMessageCountdown){

        try {
            runOnUiThread(() -> {
                long sendMessageCount = 1;
                try {
                    sendMessageCount = sendMessageLatch.getCount();
                } catch (Exception e){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Failed to perform sendMessageLatch.getCount(), because latch object is null");
                    }
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Failed to reach phone for lock. sendMessageLatch.getCount(): " + sendMessageCount);
                }

                if (sendMessageCount > 0 || !fromSendMessageCountdown) {
                    Toast.makeText(getApplicationContext(), "Failed to communicate with Phone app", Toast.LENGTH_SHORT).show();
                }
                circularProgress.setVisibility(View.GONE);
                circularProgressCancelText.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                enableMainButtons();
//                    try {
//                        if (fromSendMessageCountdown) {
//                            sendMessageLatch.countDown();
//                        } else {
//                            waitToFindWatchLatch.countDown();
//                        }
//                    } catch (Exception e){
//                        if (BuildConfig.DEBUG) {
//                            Log.e(TAG, "tried to countdown remaining latches if anything, but failed.", e);
//                        }
//                    }
            });
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "failed to make toast about... failing to find phone");
            }
        }
        vibrate(false);

    }



    private void setConnectionStatusText(final boolean connected){
        runOnUiThread(() -> {
            if (connected) {
                if (phoneAppReachable) {
                    connectivityStatusText.setText(getApplicationContext().getResources().getString(R.string.connected));
                    connectivityStatusText.setTextColor(Color.GREEN);
                    phoneOnboardingButton.setVisibility(View.GONE);
                    lockButton.setVisibility(View.VISIBLE);
                    lockButtonTasker.setVisibility(View.VISIBLE);
                    enableMainButtons();
                    buttonAssetShowHide(true, KEYCODE_STEM_1, buttonOneImage, buttonOneText);
                    buttonAssetShowHide(true, KEYCODE_STEM_2, buttonTwoImage, buttonTwoText);
                    buttonAssetShowHide(true, KEYCODE_STEM_3, buttonThreeImage, buttonThreeText);
                    showPhoneAppOnboarding(false);
                }

            } else {
                connectivityStatusText.setText(getApplicationContext().getResources().getString(R.string.disconnected));
                connectivityStatusText.setTextColor(Color.RED);
                buttonAssetShowHide(false, KEYCODE_STEM_1, buttonOneImage, buttonOneText);
                buttonAssetShowHide(false, KEYCODE_STEM_2, buttonTwoImage, buttonTwoText);
                buttonAssetShowHide(false, KEYCODE_STEM_3, buttonThreeImage, buttonThreeText);
                phoneAppReachable = false;
                phoneNodeID = null;
            }
        });
    }

    private void enableMainButtons(){
        lockButton.setEnabled(true);
        lockButtonTasker.setEnabled(true);
        startConstantPINButton.setEnabled(true);
        noDelayCheckbox.setEnabled(true);
    }

    private void disableMainButtons(){
        lockButton.setEnabled(false);
        lockButtonTasker.setEnabled(false);
        startConstantPINButton.setEnabled(false);
        noDelayCheckbox.setEnabled(false);
    }



    private boolean isPhoneConnected() {

        Task<List<Node>> nodesTask = Wearable.getNodeClient(this)
                .getConnectedNodes();
        nodesTask.addOnSuccessListener(nodes -> {
            if (nodes.size() > 0){
                isPhoneConnected = true;
                getPhoneNodeID(false);
            } else {
                isPhoneConnected = false;

                setConnectionStatusText(false);
            }
        });

        nodesTask.addOnFailureListener(e -> {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "isPhoneConnected called, **exception from ADDONFAILURELISTENER**: ", e);
            }
            isPhoneConnected = false;
        });

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "isPhoneConnected() = " + isPhoneConnected);
        }
        return isPhoneConnected;

    }



    private void getPhoneNodeID(boolean countDownWait){
        phoneAppReachable = false;
        Task<CapabilityInfo> capabilityInfoTask = Wearable.getCapabilityClient(this)
                .getCapability(WATCH_SERVER, CapabilityClient.FILTER_REACHABLE);

        capabilityInfoTask.addOnCompleteListener(task -> {

            if (task.isSuccessful()) {

                CapabilityInfo capabilityInfo = task.getResult();
                if (capabilityInfo != null) {
                    String nodeOutput = String.valueOf(capabilityInfo.getNodes());

                    Set<Node> connectedNodes = capabilityInfo.getNodes();

                    phoneNodeID = pickBestNodeId(connectedNodes);



                    if (phoneNodeID != null) {
                        phoneAppReachable = true;
                        setConnectionStatusText(true);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "best node candidate from getPhoneNodeID(): PHONE (watch_server) nodes: " + nodeOutput + " nodeID: " + phoneNodeID);
                        }
                    } else{
                        phoneAppReachable = false;
                        setConnectionStatusText(false);

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "getPhoneNodeID(): no reachable node found (app not installed)");
                        }

                        if (isPhoneConnected){
                            showPhoneAppOnboarding(true);
                        }
                    }

                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Node capability request failed to return any results.");
                }
                try {
                    runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Failed to find RadiusLocker app on Phone, or is Disconnected", Toast.LENGTH_SHORT).show());
                } catch (Exception e){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "failed to make toast about... failing to find phone");
                    }
                }
                setConnectionStatusText(false);
                phoneAppReachable = false;
            }

        });

        if (countDownWait) {
            waitToFindWatchLatch.countDown();
        }
    }

    private void showPhoneAppOnboarding(boolean show){
        if (show) {
            connectivityStatusText.setVisibility(View.GONE);
            phoneOnboardingButton.setVisibility(View.VISIBLE);
            phoneOnboardingButton.setEnabled(true);
            phoneOnboardingText.setVisibility(View.VISIBLE);
            lockButton.setVisibility(View.GONE);
            lockButtonTasker.setVisibility(View.GONE);
            startConstantPINButton.setVisibility(View.GONE);
            noDelayCheckbox.setVisibility(View.GONE);

            buttonAssetShowHide(false, KEYCODE_STEM_1, buttonOneImage, buttonOneText);
            buttonAssetShowHide(false, KEYCODE_STEM_2, buttonTwoImage, buttonTwoText);
            buttonAssetShowHide(false, KEYCODE_STEM_3, buttonThreeImage, buttonThreeText);
            disableMainButtons();
        } else{
            connectivityStatusText.setVisibility(View.VISIBLE);
            phoneOnboardingButton.setVisibility(View.GONE);
            phoneOnboardingButton.setEnabled(false);
            phoneOnboardingText.setVisibility(View.GONE);
            lockButton.setVisibility(View.VISIBLE);
            lockButtonTasker.setVisibility(View.VISIBLE);
            startConstantPINButton.setVisibility(View.VISIBLE);
            noDelayCheckbox.setVisibility(View.VISIBLE);

            buttonAssetShowHide(true, KEYCODE_STEM_1, buttonOneImage, buttonOneText);
            buttonAssetShowHide(true, KEYCODE_STEM_2, buttonTwoImage, buttonTwoText);
            buttonAssetShowHide(true, KEYCODE_STEM_3, buttonThreeImage, buttonThreeText);
            enableMainButtons();
        }

    }
    private void installPhoneApp(){
        Intent intentAndroid = new Intent(Intent.ACTION_VIEW).addCategory(Intent.CATEGORY_BROWSABLE).setData(Uri.parse("market://details?id=com.merrycachemiss.rl"));
        RemoteIntent.startRemoteActivity(getApplicationContext(), intentAndroid, installPhoneAppResultReceiver);
    }

    // Result from sending RemoteIntent to phone to open app in play/app store.
    private final ResultReceiver installPhoneAppResultReceiver = new ResultReceiver(new Handler()) {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == RemoteIntent.RESULT_OK) {
                try{
                    new ConfirmationOverlay()
                            .setType(ConfirmationOverlay.OPEN_ON_PHONE_ANIMATION)
                            .setMessage("Open on phone")
                            .showOn(MainActivity.this);

                } catch (Exception e){
                    try {
                        runOnUiThread(() -> {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Failed to show phone open overlay");
                            }
                            Toast.makeText(getApplicationContext(), "Open on phone", Toast.LENGTH_LONG).show();

                        });
                    } catch (Exception e2){
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to make toast about phone open");
                        }
                    }
                }
            } else if (resultCode == RemoteIntent.RESULT_FAILED) {
                try{
                    new ConfirmationOverlay()
                            .setType(ConfirmationOverlay.FAILURE_ANIMATION)
                            .setMessage("Failed to launch Play Store on Phone. Find RadiusLocker manually.")
                            .showOn(MainActivity.this);
                } catch (Exception e){
                    try {
                        runOnUiThread(() -> {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "Failed to show phone open error message overlay");
                            }
                            Toast.makeText(getApplicationContext(), "Failed to launch Play Store on Phone. Find RadiusLocker manually.", Toast.LENGTH_LONG).show();

                        });
                    } catch (Exception e2){
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "failed to make toast about phone open failure");
                        }
                    }
                }
            } else {
                if (BuildConfig.DEBUG) {
                    throw new IllegalStateException("failed to get result of phone installation launch" + resultCode);
                }
            }
        }
    };
    private String pickBestNodeId(Set<Node> nodes) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getting best nearby node");
        }
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "name: " + node.getDisplayName() + " id: " + node.getId() + " nearby: " + node.isNearby());
            }
            if (node.isNearby()) {
                return node.getId();
            }
        }
        //if nothing found, return null by default
        return null;
    }

    @Override
    protected void onDestroy() {
        try {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to clear screen on flag in onDestroy.", e);
            }

        }
        try {
            Wearable.getMessageClient(this).removeListener(this);
        } catch (Exception e){
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "failed to removed listener in onDestroy.", e);
            }

        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "removed listener and destroyed activity.");
        }
        super.onDestroy();
    }

}
