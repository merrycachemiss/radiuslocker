# RadiusLocker
RadiusLocker is a free app that automatically locks your phone when your smartwatch falls out of connection range, and then temporarily disables biometric unlocking until you've next entered your PIN.

The goal is to further lock down your phone if it gets taken away from you, or if you've left it behind somewhere, thus adding an extra layer of security on local data. Think of it as automation for the Lockdown feature, which temporarily disables biometric unlocking features, but also available for devices running older Android versions behind P. 

There is also a Wear OS companion app, which can be used to lock your phone manually while still in range, and both apps can be used just for this if you are not interested in any extra/automated functionality. The Wear OS companion app requires Google Play Services, as there's seemingly no other way to facilitate communication between a phone and Wear OS watch.


Full explanation of functionality, and every feature is free:

https://www.radiuslocker.co

https://play.google.com/store/apps/details?id=com.merrycachemiss.rl


This project will also be pushed to https://radicle.xyz/, a decentralized repo service. It's available at:
https://app.radicle.xyz/seeds/maple.radicle.garden/radiuslocker

Clone it with git: git clone https://maple.radicle.garden/hnrkgewat8qynqw77i93kh99573ktps8g1oyy.git

or

Clone it with the Radicle CLI using this command on Linux\* and MacOS: rad clone rad:git:hnrkgewat8qynqw77i93kh99573ktps8g1oyy --seed maple.radicle.garden

*If you plan to use Radicle CLI under WSL, do this:
https://docs.radicle.xyz/troubleshooting#install-radicle-on-windows-with-wsl



The source is released under GPLv3, with the exceptions available (via https://www.gnu.org/licenses/gpl-faq.html#GPLIncompatibleLibs) to make it compatible with the linked libraries that are potentially GPL-incompatible. The exceptions are explicitly listed, making for busty source file headers.


# TL;DR:
RL eventually needs a proper architecture, because I made this as I learned, but now I'm mainly working on other things and this app is mainly in maintenance mode rather than adding new features. I'm aware the code very un-succulent right now, but I'm not taking new contributions right now either due to limited time as I expand my day job's hours while also continuing my other new side projects.

# Pre-amble for those who haven't seen the code, read and understand before asking questions or complaining:

A lot of this was written with knowledge obtained right after the "God class" era, and it shows. Also keep in mind that I have less hours in this than a junior dev's first year, and I hadn't done anything significant in Java or general OOP in almost a decade, so I'd lost a lot of the knowledge. There are areas where I had implemented static methods for development convenience and speed, but now that I know a lot more (again), there's stuff that I'm aware probably break OOP "rules".

But know that I'm cringing with you, and hope to eventually overhaul much of the cruft and replace some shortcuts I'd taken in the interest of available time.

Since this is a weekend project, I just didn't have the time to keep up with the extremely annoying shifting opinions on the better architecture while I was still learning the basics, and simultaneously diving into quite esoteric parts of the APIs for implementing unique ideas. Sometimes I'd waste half of a year's weekends trying to implement something shiny and new from Google, but it didn't work reliably enough on different device types or OS levels. Google then introduced Jetpack and became more opinionated, too, but so much of their stuff continued to be in an unsustainable flux for many years, with surprisingly terrible documentation for them being the ones running the show. As mentioned, I've been burned by some bugs by their newly released libraries, even though their creators were enthusiastically telling us to start using them immediately at keynotes or on podcasts. So I just ignored most of their new (seemingly fad) initiatives for years, continuing to focus on adding new features while admittedly building on a rickety foundation myself.

# Other implementation notes and gotchas:

The wifi activity is quite rough, and the constant PIN mode might be hard to understand at first glance. My app is not the only one I've seen do this, but in the wifi activity when I kick the user to enable the "background location" stuff in App Info, the onboarding rationale still shows up when the activity resumes, so the user has to hit "OK" again to make it go away before it realizes that the additional permission level was granted. The three listviews could possibly be combined into a single file and a single layout, but I never got back to doing that. Also never got back into implementing onSavedInstanceState stuff either, so it reloads some database data in onResume -- and this is just one example of brute-forcing things in the interest of development time. But since this is mostly a background-running app, I didn't bother with a few such niceties, as there is minimal benefit compared to apps that users are opening on a regular basis. The Wear sort of maintains the god class concept, because it's just one simple activity with a few buttons.

Another brute-force example is in the JobScheduler. I had read that JobScheduler is unaware of its currently scheduled tasks, so I made it cancel its task right before scheduling the same new one. I never went to see if it was still true, as WorkManager had come along and I had planned to migrate to it, but it seemed that WorkManager didn't support multi-process. They eventually introduced stuff related to supporting multi-process in WM, but at the time (still?) documentation on that was thin and I had better stuff to do. But I do want to revisit replacing JS with WM, as the API is probably in less flux by now too. I'll just have to see if they ruined it by introducing too many battery-saving limitations. This app has many time-sensitive operations that the user expects to be executed at the right moment, so I can't go into changing the scheduler without extensive research and testing. Hence retaining JobScheduler.

As mentioned, the app is multi-process, backed a content provider in its own process handling the db. The DB needs to be changed to a singleton, I wasn't aware of the concept when I made it, partly because the content provider strategy removes all data problems of multiple instances of the DB object existing (obviously this introduces the efficiency problems). Almost every service is in its own process, because I've found that the app used 1/10 the RAM on idle after the main application is closed - this is relevant because there is an ongoing foreground service. More RAM will indeed be used if a few processes get spun up simultaneously, but since the service is usually the only one running most of the time, it is a better decision. Makes sense to me, all extra resources are cleanly disposed of 100%, with no lingering parts left behind. This was proven by the profiler when I ran it long ago. Another huge advantage is that if one process crashes, the others are unaffected and continue to run without interference. This saved me when the biometric API was crashy in the UI when it was new, but the background processes related to locking continued to run without issues. You get it by now. Only downside is sprefs can't be accessed from multiple processes reliably, but who cares - we have the content provider-backed database for the most important stuff. Sprefs is only used for extremely simple stuff, such as tracking acceptance of the EULA.

There were two APIs that I had implemented but they didn't work well, so if anyone thought to contribute and make a fork related to these, I'll save you some time: The Activity Recognition Transition API was buggy as hell, and the Sleep API doesn't alert you when the user is likely to have entered sleep.

* The Activity Recognition Transition API was going to be used to implement a new Snooze state for when the user is driving or biking, but I was running a development version of the app on several devices I kept with me which vibrated when driving was detected. On a few devices, they'd just be sitting on a table for an hour and madly vibrate out of nowhere, with my tester textview in the activity reporting that the phone is in a moving car.

* For the Sleep one, I wanted to implement a secondary trigger for constant PIN mode that didn't necessarily rely on the user's alarm time like it does now. But halfway through development, I'd found that the Sleep API only sends out a signal after sleeping ends, thus giving the developer a summary of the user's Sleep activity, rather than reporting "user is likely asleep RIGHT NOW, because the phone is idle and the room is dark".

Besides, I have just removed all Google Play Services components of the app where possible, allowing more devices to run every existing feature without restriction or risk of a crash. Communication with the Wear OS Companion app might still use parts of it, considering that the API is called "com.google.android.gms:play-services-wearable". I believe WorkManager uses Play Services, but it's not required. I'll see that part when I get there.

There are areas that I had trouble with that technically work, but have some problems. One of them is the Quick Settings tiles -- I followed some guides, yet couldn't get some things to work. I wanted the Constant PIN tile to be highlighted when the mode was on (its service would be running in its own process), but it didn't react. Also, I put some of the stuff in a try/catch because there was a crash that happened when the user removes the tile on certain devices. At least, I think that's when it happens. The other part is now with the Wear companion app. With Wear OS 3, it seems that there are a lot of ANRs, I think it's something to do with the code that attempts to keep the screen on when the app is doing its lock countdowns. I have a guess that it might happen when the user doesn't "close" the app (swipe away on the watch), but instead presses the home button and the app is slightly running in the background. They are those weird "native" crashes, though, so maybe it's not entirely my fault. I've since seen new code that is used to keep the screen on in certain scenarios, but I didn't do enough research yet to confirm that it's a good enough replacement for keeping the functionality, yet fixing the ANR.

I didn't care to learn Kotlin, compose, or fragments. For fragments specifically, I'd seen a lot of chatter about people hating them when I was first starting out. Maybe I'd just encountered a biased group. At this time I don't like Kotlin, but like anyone, that could change if I learn it more -- the scenarios I'd used it in just seemed to create a lot of extra fluff. Part of that is because I was combining it with Java code, though. I'm aware that it's supposed to be better if you put the work into it.